/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { components } from './dyff-api'
import { Score } from './views'

export type Report = components['schemas']['Report']
export type Model = components['schemas']['Model']
export type ForeignModel = components['schemas']['ForeignModel']
export type Dataset = components['schemas']['Dataset']
export type Evaluation = components['schemas']['Evaluation'] & {
  datasetName: string
}
export type InferenceService = components['schemas']['InferenceService']
export type InferenceSession = components['schemas']['InferenceSession']
export type DatasetCreateRequest = components['schemas']['DatasetCreateRequest']
export type Measurement = components['schemas']['Measurement']
export type Method = components['schemas']['Method']
export type Module = components['schemas']['Module']
export type SafetyCase = components['schemas']['SafetyCase']
export type ArtifactURL = components['schemas']['ArtifactURL']
export type UseCase = components['schemas']['UseCase']

export type Documentation = components['schemas']['Documentation']
export type DocumentationEditRequest = components['schemas']['DocumentationEditRequest']
export type Annotations = components['schemas']['Annotation'][] | undefined

export type InferenceServiceWithModel = InferenceService & {
  model?: Model
}
export type SafetyCaseWithDocs = SafetyCase & {
  docs?: Documentation
}
export type MethodWithDocs = Method & {
  docs?: Documentation
}
export type InferenceServiceWithDocs = InferenceService & {
  docs?: Documentation
}
export type InferenceServiceWithModelDocs = InferenceServiceWithDocs & {
  model?: ModelWithDocs
}
export type DatasetWithDocs = Dataset & {
  docs?: Documentation
}
export type ModelWithDocs = Model & {
  docs?: Documentation
}
export type ForeignModelWithDocs = ForeignModel & {
  docs?: Documentation
}
export type ModuleWithDocs = Module & {
  docs?: Documentation
}
export type ModelAnalysis = ModelWithDocs & {
  safetyCases?: SafetyCaseWithDocs[]
}
export type ModelWithDocsSvcs = ModelWithDocs & {
  services?: InferenceService[]
}
export type SafetyCaseWithModel = SafetyCase & {
  service?: InferenceServiceWithDocs
  model?: ModelWithDocs
}

export type UseCaseWithMethods = UseCase & {
  methods: MethodWithDocs[]
}

export type MethodWithSafetyCases = MethodWithDocs & {
  safetyCases: SafetyCase[]
}

export type UseCaseWithMethodsWithSafetyCases = UseCase & {
  methods: MethodWithSafetyCases[]
}

export type Entity =
  | Report
  | Model
  | Dataset
  | Evaluation
  | InferenceService
  | InferenceSession
  | Measurement
  | Method
  | SafetyCase
  | Module
  | UseCase

export type EntityWithDocs<T extends Entity> = T & {
  docs?: Documentation
}

export type EntityWithLinks<T extends Entity> = T & {
  links: {
    data?: ArtifactURL[]
    error?: string
  }
}

export type EntityWithLogLink<T extends Entity> = T & {
  link: {
    data?: ArtifactURL
    error?: string
  }
}

export type ArtifactKind = 'notebook' | 'logs' | 'scores' | 'measurement' | 'unknown'

export type ParsedArtifact = {
  data: string
  path: string
  kind: ArtifactKind
}

export type EntityWithArtifacts<T extends Entity> = T & {
  links: {
    data?: ArtifactURL[]
    error?: string
  }
  artifacts: {
    data?: ParsedArtifact[]
    error?: string
  }
}

export type EntityWithLogs<T extends Entity> = T & {
  link: {
    data?: ArtifactURL
    error?: string
  }
  logs: {
    data?: string[]
    error?: string
  }
}

export type SafetyCategoryKey =
  | 'code-completion'
  // | 'employment-decisions'
  // | 'factuality'
  | 'health-products'
  | 'human-representation'
  | 'privacy'

export type SafetyCategory = {
  key: SafetyCategoryKey
  title: string
  description: string
  icon: (size: number) => JSX.Element
}

export type TopicMethodRecord = Record<SafetyCategoryKey, Method | undefined>

export type CombinedScore = Score & {
  service?: InferenceServiceWithDocs | undefined
  model?: ModelWithDocs | undefined
  method?: Method | undefined
  safetyCase: SafetyCase
}
