/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

export type TokenType = string
export type SetTokenType = (newToken: string, redirect?: string) => void
export type SessionType = 'user' | 'guest'
export type DyffApiGrant = {
  resources: string[]
  functions: string[]
  accounts: string[]
  entities: string[]
}

export type DyffApiJWT = {
  jti: string
  sub: string
  nbf: number
  exp: number
  iat: number
  grants: DyffApiGrant[]
} | null

export type AuthContextType = {
  token: TokenType
  decodedToken: DyffApiJWT
  tokenExpired: boolean // Parseable and expired
  tokenValid: boolean // Parseable, regardless of expiration
  sessionType: SessionType
  releaseToken: () => void
  setToken: SetTokenType
  loading: boolean
  isSignedIn: boolean
  tokenIssuedAt: Date | null
  tokenExpiresAt: Date | null
  showOperatorControls: boolean
  setShowOperatorControls: (newValue: boolean) => void
}

export type PageDefinition = {
  [page: string]: {
    route: string // Route string E.g. /tests
    key: string // Lowerkase key for nav naming
    name: string // Pretty STring
  }
}
