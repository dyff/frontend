/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Entity, EntityWithArtifacts, EntityWithDocs, EntityWithLogs } from './api'

// export type EntityHookParams = Record<string, string | string[]>
export type EntityHookParams = {
  id?: string | string[]
  labels?: {
    [key: string]: any
  }
  method?: string
  account?: string
  status?: string
  reason?: string
  name?: string
  [key: string]: any
}
export type EntityHookSetParams = (key: string, value: string) => void
export type EntityHookRefresh = (newParams?: EntityHookParams) => void
export type EntityHookOptions = {
  /** Initial search parameters for the query GET */
  initParams?: EntityHookParams
  /** Whether to hit the /public version of dyff-api */
  usePublic?: boolean
  /** If using public, also attempt to access authorized endpoit with 'internal' access label */
  useInternal?: boolean
  /** Whether to fetch documents */
  fetchDocs?: boolean
  /** Whether to fetch downlinks */
  fetchArtifacts?: boolean
  /** Whether to fetch logs */
  fetchLogs?: boolean
  /** Automatically trigger fetch on intial hook load */
  doInitalFetch?: boolean
}
export type EntityHookTriggerOptions = { token: string; params: EntityHookParams }

export type EntityHookResponse<T extends Entity> = {
  data: T[] | null
  docs: EntityWithDocs<T>[] | null
  artifacts: EntityWithArtifacts<T>[] | null
  logs: EntityWithLogs<T>[] | null
  error: EntityHookError
  loading: boolean
  refresh: EntityHookRefresh
  lastUpdated: Date | null
  updateCount: number
  params: EntityHookParams
  setParams: EntityHookSetParams
  updateLabels: UpdateLabelsFn
  updateDocs: UpdateDocsFn
}

export type EntityHookError = {
  /** An informative message of the error (manually provided) */
  message: string
  /** The HTTP status code of the error (if relevant) */
  status: number | string
  /** A message provided by the original error */
  statusText: string
  /** Which part of the fetch process that threw the error */
  part: EntityFetchErrorPart
} | null

export type EntityFetchErrorPart = 'primary' | 'docs' | 'auth' | 'artifacts' | 'logs' | 'unknown'

export type StagedLabels = {
  [key: string]: string | null | undefined
}

export type UpdateLabelsFn = (
  id: string,
  newLabels: StagedLabels,
  cb: {
    onSuccess: () => void
    onDone: () => void
  }
) => void
export type UpdateDocsFn = (
  id: string,
  newDocs: Documentation,
  cb: {
    onSuccess?: () => void
    onDone?: () => void
  }
) => void
