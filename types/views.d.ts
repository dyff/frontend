/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { components, operations } from './dyff-views'

export type MethodSingleView = components['schemas']['MethodSingleView']
export type MethodListView = components['schemas']['MethodListView']
export type SystemSingleView = components['schemas']['SystemSingleView']
export type SystemListView = components['schemas']['SystemListView']
export type HomeView = components['schemas']['HomeView']

export type HTTPValidationError = components['schemas']['HTTPValidationError']
export type Score = components['schemas']['Score']
export type ScoreSpec = components['schemas']['ScoreSpec']

export type View = MethodSingleView | MethodListView | SystemSingleView | SystemListView
