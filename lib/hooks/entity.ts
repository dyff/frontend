/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  ArtifactURL,
  DocumentationEditRequest,
  Entity,
  EntityWithArtifacts,
  EntityWithDocs,
  EntityWithLinks,
  EntityWithLogLink,
  EntityWithLogs,
  ParsedArtifact
} from '@/types/api'

import { useAuth } from '@/components/auth/AuthProvider'
import {
  EntityFetchErrorPart,
  EntityHookError,
  EntityHookOptions,
  EntityHookParams,
  EntityHookResponse,
  StagedLabels
} from '@/types/entity'
import { useSetState } from '@mantine/hooks'
import { notifications, showNotification } from '@mantine/notifications'
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { useRouter } from 'next/navigation'
import { useEffect, useState } from 'react'
import { PAGE_DEFS } from '../constants/constants'
import { DYFF_ACCESS_STR } from '../constants/labels'
import {
  getInferredArtifactKind,
  parseFetchError,
  parseResStatus,
  removeDuplicates
} from '../helpers'

/**
 * Entity data hook for individual API entity
 * @param entityRoute String of API route for the entity
 * @param options Options for entity hook
 * @returns
 */
export function useEntityData<T extends Entity>(
  entityRoute: string,
  options: EntityHookOptions
): EntityHookResponse<T> {
  // Set defaults
  const {
    fetchDocs = false,
    fetchArtifacts = false,
    fetchLogs = false,
    doInitalFetch = true,
    usePublic = false,
    useInternal = false,
    initParams = {
      id: '',
      account: '',
      status: '',
      reason: '',
      name: '',
      labels: {}
    }
  } = options
  const router = useRouter()
  const { token, tokenValid, loading: tokenLoading } = useAuth()

  const [data, setData] = useState<T[] | null>(null)
  const [docs, setDocs] = useState<EntityWithDocs<T>[] | null>(null)
  const [artifacts, setArtifacts] = useState<EntityWithArtifacts<T>[] | null>(null)
  const [logs, setLogs] = useState<EntityWithLogs<T>[] | null>(null)
  const [error, setError] = useState<EntityHookError>(null)

  const [loading, setLoading] = useState<boolean>(false)
  const [updateLoading, setUpdateLoading] = useState<boolean>(false)
  const [lastUpdated, setLastUpdated] = useState<Date | null>(null)
  const [updateCount, setUpdateCount] = useState(0)

  const [params, _setParams] = useSetState<EntityHookParams>(initParams)

  /**
   * Wrapper for _setParams
   * @param key Parameter key
   * @param value Parameter value
   */
  const setParams = (key: string, value: string) => {
    _setParams({ [key]: value })
  }

  /**
   * Refresh data with state
   * @param newParams New parameters to assign to params state
   */
  const refresh = async (newParams?: EntityHookParams) => {
    try {
      setError(null)
      setLoading(true)
      if (newParams) {
        _setParams({ ...newParams })
      }
      const theseParams = newParams ? newParams : initParams
      // Append to new params labels if existent
      const internalParams = {
        ...(theseParams || {}),
        labels: {
          ...theseParams.labels,
          [DYFF_ACCESS_STR]: 'internal'
        }
      }

      const basePath = buildRoutePath()
      const entityHeaders = buildHeaders()
      const internalPath = buildRoutePath(false)

      // Define at base for use in other docs / artifact blocks
      let primaryData: T[] | undefined
      let primaryDocs: EntityWithDocs<T>[] | undefined
      let primaryArtifacts: EntityWithArtifacts<T>[] | undefined
      let primaryLogs: EntityWithLogs<T>[] | undefined
      let internalData: T[] | undefined

      // Fetch primary data
      try {
        const primaryPath = basePath
        const primaryRes: AxiosResponse = await axios.get(primaryPath, {
          headers: entityHeaders,
          params: buildURLParams(theseParams)
        })

        if (!primaryRes.data) {
          throw new EntityFetchError('No data found in response!', 500, 'Server error', 'primary')
        }

        // Check for error response from API
        if (primaryRes.data.detail) {
          throw new EntityFetchError(
            'Bad API Response: ' + primaryRes.data.detail,
            404,
            'Server error',
            'primary'
          )
        }
        primaryData = primaryRes.data
        if (primaryData) setData(primaryData)
      } catch (e) {
        forwardError(e, 'primary')
      }

      // Fetch internal data if enabled, discard if same as fetched in primary
      try {
        if (usePublic && useInternal) {
          const internalRes: AxiosResponse = await axios.get(internalPath, {
            headers: buildHeaders(false),
            params: buildURLParams(internalParams)
          })

          if (!internalRes.data) {
            throw new EntityFetchError('No data found in response!', 500, 'Server error', 'primary')
          }

          // Check for error response from API
          if (internalRes.data.detail) {
            throw new EntityFetchError(
              'Bad API Response: ' + internalRes.data.detail,
              404,
              'Server error',
              'primary'
            )
          }
          internalData = internalRes.data
          if (internalData) {
            const combData = removeDuplicates([...(primaryData || []), ...internalData])
            setData(combData)
            // Exclude duplicates within primaryData for subsequent fetches
            internalData = internalData.filter((d) =>
              primaryData ? !primaryData.includes(d) : true
            )
          }
        }
      } catch (e) {
        // console.log('Expected unauthenticated error fetching internal data')
      }

      // Fetch docs if enabled
      try {
        if (primaryData && !!fetchDocs) {
          primaryDocs = await Promise.all(
            primaryData.map(async (d): Promise<EntityWithDocs<T>> => {
              const docsPath = `${basePath}/${d.id}/documentation`
              const res = await axios.get(docsPath, {
                headers: entityHeaders
              })
              if (!res.data) {
                throw new EntityFetchError(
                  'No data found in individual docs response!',
                  500,
                  'Data missing',
                  'docs'
                )
              }
              return {
                ...d,
                docs: res.data
              }
            })
          )
          setDocs(primaryDocs)
        }
      } catch (e) {
        forwardError(e, 'docs')
      }
      // Fetch internal docs if enabled
      try {
        if (internalData && usePublic && useInternal && fetchDocs) {
          const newInternalDocsData: EntityWithDocs<T>[] = await Promise.all(
            internalData.map(async (d): Promise<EntityWithDocs<T>> => {
              const docsPath = `${internalPath}/${d.id}/documentation`
              const res = await axios.get(docsPath, {
                headers: buildHeaders(false)
              })
              if (!res.data) {
                throw new EntityFetchError(
                  'No data found in individual docs response!',
                  500,
                  'Data missing',
                  'docs'
                )
              }
              return {
                ...d,
                docs: res.data
              }
            })
          )

          const combData = removeDuplicates([...(primaryDocs || []), ...newInternalDocsData])
          setDocs(combData)
        }
      } catch (e) {
        // console.log('Expected unauthenticated error fetching internal docs')
      }

      // Fetch artifacts if enabled
      // Dont throw errors, pivot on error attribute of artifacts property
      if (primaryData && fetchArtifacts) {
        try {
          // Fetch arr of Combined entity with links property
          const entityLinkArr: EntityWithLinks<T>[] = await Promise.all(
            primaryData.map(async (d) => {
              try {
                const linksRes = await axios.get(`${basePath}/${d.id}/downlinks`, {
                  headers: entityHeaders
                })
                const links = linksRes.data as ArtifactURL[]
                return {
                  ...d,
                  links: {
                    data: links
                  }
                }
              } catch (e) {
                const msg = parseFetchError(e)
                return {
                  ...d,
                  links: { error: `Failed to fetch downlinks: ${msg}` }
                }
              }
            })
          )

          const artifactSuccess = (d: EntityWithLinks<T>, data: ParsedArtifact[]) => {
            return {
              ...d,
              artifacts: {
                data: data
              }
            } /*  */
          }

          const artifactError = (d: EntityWithLinks<T>, error: string) => {
            return {
              ...d,
              artifacts: {
                error: error
              }
            }
          }

          // Fetch arr of Combined entity with artifacts property (includes links)
          primaryArtifacts = await Promise.all(
            // Loop through each entity's downlinks
            entityLinkArr.map(async (d) => {
              // Return error if error in links
              if (d.links.error) {
                return artifactError(d, d.links.error)
              }
              if (!d.links.data) {
                return artifactError(d, 'No downlinks found')
              }

              // Loop through each artifact URL
              try {
                // If any fail, throw to catch
                const artifactArr: ParsedArtifact[] = await Promise.all(
                  d.links.data.map(async (link) => {
                    // TODO - Fails in dev env due to CORS (access from localhost:3000)
                    const artifactStr = await axios
                      .get(link.signedURL.url, {
                        headers: link.signedURL.headers
                      })
                      .then((res) => res.data)

                    // Artifact kind is not populated on server. Infer kind based on path
                    const inferredKind = getInferredArtifactKind(link.artifact.path)

                    return {
                      data: artifactStr,
                      kind: inferredKind,
                      path: link.artifact.path
                    }
                  })
                )
                return artifactSuccess(d, artifactArr)
              } catch (e) {
                const msg = parseFetchError(e)
                return artifactError(d, `Failed to fetch artifacts: ${msg}`)
              }
            })
          )

          setArtifacts(primaryArtifacts)
        } catch (e) {
          forwardError(e, 'artifacts')
        }
      }

      // Fetch internal artifacts if enabled
      if (internalData && usePublic && useInternal && fetchArtifacts) {
        try {
          // Fetch arr of Combined entity with links property
          const entityLinkArr: EntityWithLinks<T>[] = await Promise.all(
            internalData.map(async (d) => {
              try {
                const linksRes = await axios.get(`${internalPath}/${d.id}/downlinks`, {
                  headers: buildHeaders(false)
                })
                const links = linksRes.data
                return {
                  ...d,
                  links: {
                    data: links
                  }
                }
              } catch (e) {
                const msg = parseFetchError(e)
                return {
                  ...d,
                  links: { error: `Failed to fetch downlinks: ${msg}` }
                }
              }
            })
          )

          const artifactSuccess = (d: EntityWithLinks<T>, data: ParsedArtifact[]) => {
            return {
              ...d,
              artifacts: {
                data: data
              }
            }
          }

          const artifactError = (d: EntityWithLinks<T>, error: string) => {
            return {
              ...d,
              artifacts: {
                error: error
              }
            }
          }

          // Fetch arr of Combined entity with artifacts property (includes links)
          const internalArtifacts: EntityWithArtifacts<T>[] = await Promise.all(
            // Loop through each entity's downlinks
            entityLinkArr.map(async (d) => {
              // Return error if error in links
              if (d.links.error) {
                return artifactError(d, d.links.error)
              }
              if (!d.links.data) {
                return artifactError(d, 'No downlinks found')
              }

              // Loop through each artifact URL
              try {
                // If any fail, throw to catch
                const artifactArr: ParsedArtifact[] = await Promise.all(
                  d.links.data.map(async (link) => {
                    // TODO - Fails in dev env due to CORS (access from localhost:3000)
                    const artifactStr = await axios
                      .get(link.signedURL.url, {
                        headers: link.signedURL.headers
                      })
                      .then((res) => res.data)

                    // Artifact kind is not populated on server. Infer kind based on path
                    const inferredKind = getInferredArtifactKind(link.artifact.path)

                    return {
                      data: artifactStr,
                      kind: inferredKind,
                      path: link.artifact.path
                    }
                  })
                )
                return artifactSuccess(d, artifactArr)
              } catch (e) {
                const msg = parseFetchError(e)
                return artifactError(d, `Failed to fetch artifacts: ${msg}`)
              }
            })
          )
          const combData = removeDuplicates([...(primaryArtifacts || []), ...internalArtifacts])
          setArtifacts(combData)
        } catch (e) {
          // console.log('Expected unauthenticated error fetching internal artifacts')
        }
      }

      // Fetch logs if enabled
      if (primaryData && fetchLogs) {
        try {
          // Fetch arr of Combined entity with links property
          const entityLogDownlinks: EntityWithLogLink<T>[] = await Promise.all(
            primaryData.map(async (d) => {
              try {
                const linksRes = await axios.get(`${basePath}/${d.id}/logs`, {
                  headers: entityHeaders
                })
                const links = linksRes.data
                return {
                  ...d,
                  link: {
                    data: links
                  }
                }
              } catch (e) {
                const msg = parseFetchError(e)
                return {
                  ...d,
                  link: { error: `Failed to fetch downlinks: ${msg}` }
                }
              }
            })
          )

          const artifactSuccess = (d: EntityWithLogLink<T>, data: any): EntityWithLogs<T> => {
            return {
              ...d,
              logs: {
                data: data
              }
            }
          }

          const artifactError = (d: EntityWithLogLink<T>, error: string): EntityWithLogs<T> => {
            return {
              ...d,
              logs: {
                error: error
              }
            }
          }

          // Fetch arr of Combined entity with artifacts property (includes links)
          primaryLogs = await Promise.all(
            // Loop through each entity's log downlinks
            entityLogDownlinks.map(async (d) => {
              // Return error if error in links
              if (d.link.error) {
                return artifactError(d, d.link.error)
              }
              if (!d.link.data) {
                return artifactError(d, 'No downlinks found')
              }

              // Loop through each artifact URL
              try {
                // If any fail, throw to catch
                const logArr: string[] = await axios
                  .get(d.link.data.signedURL.url, {
                    headers: d.link.data.signedURL.headers
                  })
                  .then((res) => {
                    const logStr = res.data
                    return !!logStr && typeof logStr === 'string' ? logStr.split('\n') : []
                  })

                return artifactSuccess(d, logArr)
              } catch (e) {
                const msg = parseFetchError(e)

                return artifactError(d, `Failed to fetch artifacts: ${msg}`)
              }
            })
          )

          setLogs(primaryLogs)
        } catch (e) {
          forwardError(e, 'logs')
        }
      }

      // If successful
      setLastUpdated(new Date())
      setUpdateCount((prev) => prev + 1)
    } catch (e) {
      if (e instanceof EntityFetchError) {
        const err = e as EntityFetchError
        setError({
          message: err.message,
          part: err.part,
          status: err.status,
          statusText: err.statusText
        })
        if (err.status === 401) {
          router.push(PAGE_DEFS.AUTH_ERROR.route)
        }
      } else {
        const err = e as Error
        const msg = `Unknown error occured in entity hook: ${err.message}`
        setError({
          message: msg,
          part: 'unknown',
          status: 400,
          statusText: msg
        })
        console.error('Unknown error occured in entity hook!')
      }
    } finally {
      setLoading(false)
    }
  }

  /** Update labels for parameter ID's */
  const updateLabels = (
    id: string,
    newLabels: StagedLabels,
    cb: {
      onSuccess?: () => void
      onDone?: () => void
    }
  ): void => {
    if (usePublic) {
      showNotification({ message: 'Cannot update documentation with public API!' })
      return
    }
    const basePath = buildRoutePath()
    const query = JSON.stringify({
      labels: newLabels
    })
    const config = {
      headers: buildHeaders()
    }

    setUpdateLoading(true)
    axios
      .patch(`${basePath}/${id}/labels`, query, config)
      .then((res) => {
        if (res.status === 200) {
          notifications.show({ message: 'Labels updated successfully!' })
          // No backend await for DB changes. Wait 1 second
          setTimeout(() => {
            cb.onSuccess && cb.onSuccess()
            cb.onDone && cb.onDone()
            setUpdateLoading(false)
            refresh()
          }, 1000)
        } else {
          cb.onDone && cb.onDone()
          setUpdateLoading(false)
          notifications.show({ message: `Labels failed to update: ${parseResStatus(res.status)}` })
        }
      })
      .catch((e) => {
        if (e instanceof AxiosError) {
          const res = e.response
          if (res && res.data && res.data.detail) {
            notifications.show({
              message: `Labels failed to update: ${res.data.detail}`
            })
          }
        } else {
          if (e.response) {
            const res = e.response
            notifications.show({
              message: `Labels failed to update: ${parseResStatus(res.status)}`
            })
          } else {
            const err = e as Error
            notifications.show({ message: `Labels failed to update: ${err.message}` })
          }
        }
        cb.onDone && cb.onDone()
        setUpdateLoading(false)
      })
  }

  const updateDocs = (
    id: string,
    newDocs: DocumentationEditRequest,
    cb: {
      onSuccess?: () => void
      onDone?: () => void
    }
  ): void => {
    if (usePublic) {
      showNotification({ message: 'Cannot update documentation with public API!' })
      return
    }
    const basePath = buildRoutePath()
    const query = JSON.stringify(newDocs)
    const config = {
      headers: buildHeaders()
    }

    setUpdateLoading(true)
    axios
      .patch(`${basePath}/${id}/documentation`, query, config)
      .then((res) => {
        if (res.status === 200) {
          notifications.show({ message: 'Documentation updated successfully!' })
          // No backend await for DB changes. Wait 1 second
          setTimeout(() => {
            setUpdateLoading(false)
            cb.onSuccess && cb.onSuccess()
            cb.onDone && cb.onDone()
            refresh()
          }, 1000)
        } else {
          setUpdateLoading(false)
          cb.onDone && cb.onDone()
          notifications.show({
            message: `Documentation failed to update: ${parseResStatus(res.status)}`
          })
        }
      })
      .catch((err) => {
        if (err.response) {
          const res = err.response
          notifications.show({
            message: `Documentation failed to update: ${parseResStatus(res.status)}`
          })
        } else {
          const e = err as Error
          notifications.show({ message: `Documentation failed to update: ${e.message}` })
        }
        cb.onDone && cb.onDone()
        setUpdateLoading(false)
      })
  }

  /**
   * Builds the api route path
   * @param usePublicOverride Override public variable
   * @returns Path string
   */
  const buildRoutePath = (usePublicOverride = usePublic): string => {
    return `/api${!usePublicOverride ? '' : '/public'}/${entityRoute}`
  }

  /**
   * Build axios request headers
   */
  const buildHeaders = (usePublicOverride = usePublic): AxiosRequestConfig['headers'] => {
    // Build headers
    const headers: AxiosRequestConfig['headers'] = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8'
    }
    if (!usePublicOverride) {
      // If fetching private data but token not loaded, throw error
      // If using internal
      if (!token || token === '' || !tokenValid) {
        throw new EntityFetchError('Token invalid when fetching data!', 401, 'Unauthorized', 'auth')
      }
      headers['Authorization'] = `Bearer ${token}`
    }
    return headers
  }

  /**
   * Build URL Parameter object from params object
   * Excludes objects that have blank values to prevent param1=&param2=
   */
  const buildURLParams = (newParams?: EntityHookParams) => {
    const theseParams = !!newParams ? newParams : params
    const searchParams = new URLSearchParams()
    const queryObject: EntityHookParams = {}
    // Exclude empty values from query object
    Object.entries(theseParams).forEach(([key, value]) => {
      if (value && value !== '') {
        queryObject[key] = value
      }
    })
    if (Object.keys(queryObject).length) {
      searchParams.append('query', JSON.stringify(queryObject))
    }
    return searchParams
  }

  /**
   * Handle error type detection and throw appropriate error
   * @param e
   */
  const forwardError = (e: any, errorPart: EntityFetchErrorPart) => {
    if (e instanceof AxiosError) {
      const detail = e.response?.data.detail ?? e.message
      const status = e.response?.status ?? e.status ?? 404
      const statusText = e.response?.statusText ?? 'Not found'
      const msg = `${status} ${statusText} | ${detail}`
      throw new EntityFetchError(msg, status, statusText, 'primary')
    } else if (e instanceof EntityFetchError) {
      throw e
    } else {
      const err = e as Error
      throw new EntityFetchError('Unknown error fetching primary data', 404, err.message, errorPart)
    }
  }

  // Initial fetch if enabled
  useEffect(() => {
    if (!tokenLoading && doInitalFetch) {
      refresh(initParams)
    }
  }, [tokenLoading])

  // Before token is loaded, set loading state if initialFetch
  useEffect(() => {
    setLoading(doInitalFetch)
  }, [])

  return {
    data,
    docs,
    artifacts,
    logs,
    error,
    loading,
    refresh,
    params,
    setParams,
    lastUpdated,
    updateCount,
    updateLabels,
    updateDocs
  }
}

export class EntityFetchError extends Error {
  public status: string | number
  public statusText: string
  public part: EntityFetchErrorPart

  constructor(
    message: string,
    status: string | number,
    statusText: string,
    part: EntityFetchErrorPart
  ) {
    super(message)
    this.name = 'EntityFetchError'
    this.status = status
    this.statusText = statusText
    this.part = part

    Object.setPrototypeOf(this, EntityFetchError.prototype)
    if (Error.captureStackTrace) {
      Error.captureStackTrace(this, EntityFetchError)
    }
  }
}
