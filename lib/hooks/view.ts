/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import { EntityHookError } from '@/types/entity'
import { View } from '@/types/views'
import axios, { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios'
import { useEffect, useState } from 'react'
import { EntityFetchError } from './entity'

interface UseViewConfig<T extends View> {
  route: string
  onError?: (msg: string) => void
  onSuccess?: (data: T) => void
}

export function useView<T extends View>(
  config: UseViewConfig<T>
): {
  data: T | undefined
  loading: boolean
  error: EntityHookError
  refresh: () => void
} {
  const { route, onError, onSuccess } = config
  const { token, tokenValid, loading: tokenLoading } = useAuth()
  const [data, setData] = useState<T>()
  const [loading, setLoading] = useState<boolean>(true)
  const [error, setError] = useState<EntityHookError>(null)

  const refresh = async () => {
    const headers = buildHeaders()
    try {
      setError(null)
      setLoading(true)
      const res: AxiosResponse<T> = await axios.get(route, {
        headers: headers
      })

      if (!res.data) {
        throw new EntityFetchError('No data found in response!', 500, 'Server error', 'primary')
      }

      const data = res.data
      setData(data)
      onSuccess && onSuccess(data)
    } catch (e) {
      if (e instanceof EntityFetchError) {
        const err = e as EntityFetchError
        setError({
          message: err.message,
          part: err.part,
          status: err.status,
          statusText: err.statusText
        })
        onError && onError(err.message)
      } else if (e instanceof AxiosError) {
        if (e.response) {
          const detail = e.response.data.detail ?? 'No detail'
          const msg = `${e.response.status} ${e.response.statusText} | ${detail}`
          setError({
            message: msg,
            part: 'primary',
            status: e.response.status,
            statusText: e.response.statusText
          })
          onError && onError(msg)
        } else {
          setError({
            message: e.message,
            part: 'primary',
            status: 404,
            statusText: 'Bad Request'
          })
          onError && onError(e.message)
        }
      } else {
        const err = e as Error
        const msg = `Unknown error occured in view hook: ${err.message}`
        setError({
          message: msg,
          part: 'unknown',
          status: 400,
          statusText: msg
        })
        onError && onError(msg)
      }
    } finally {
      setLoading(false)
    }
  }

  const buildHeaders = (): AxiosRequestConfig['headers'] => {
    // Build headers
    const headers: AxiosRequestConfig['headers'] = {
      Accept: 'application/json',
      'Content-Type': 'application/json; charset=utf-8'
    }
    if (token && token !== '' && tokenValid) {
      headers['Authorization'] = `Bearer ${token}`
    }
    return headers
  }

  // Initial fetch
  useEffect(() => {
    if (!tokenLoading) {
      refresh()
    }
  }, [tokenLoading])

  return {
    data,
    loading,
    error,
    refresh
  }
}
