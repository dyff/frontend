/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

/**
 * Return random status out of ready, deleted, terminated
 * @returns Random status string
 */
function getRandomStatus(): string {
  const statuses = ['created', 'admitted', 'ready', 'complete', 'error', 'failed', 'terminated']
  const randIdx = Math.floor(Math.random() * statuses.length)
  return statuses[randIdx]
}

function getRandomJobStatus(): string {
  const statuses = ['created', 'admitted', 'ready', 'error']
  const randIdx = Math.floor(Math.random() * statuses.length)
  return statuses[randIdx]
}
