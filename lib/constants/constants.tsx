/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { SessionType } from '@/types/auth'
import {
  IconBox,
  IconBoxModel2,
  IconBriefcase,
  IconDatabase,
  IconDeviceIpadHorizontal,
  IconFileDescription,
  IconHelp,
  IconHome,
  IconHomeCog,
  IconHomeShield,
  IconLayoutDashboard,
  IconRouteSquare,
  IconRuler3,
  IconTransferIn,
  IconUserCircle,
  IconZoomCheck
} from '@tabler/icons-react'

export const PAGE_PERMS = {
  home: ['guest', 'user'],
  user: ['user'],
  dashboard: ['user'], // Covers all sub routes
  tests: ['guest', 'user'],
  usecases: ['guest', 'user'],
  systems: ['guest', 'user'],
  reports: ['guest', 'user']
} as {
  [page: string]: SessionType[]
}

export const PAGE_DEFS = {
  HOME: { route: '/home', name: 'Home', key: 'home', title: 'Home | Dyff', icon: <IconHome /> },
  DYFF_LANDING: {
    route: 'https://dyff.io/',
    name: 'Learn More',
    key: 'learnmore',
    title: 'Learn More',
    icon: <IconHelp />
  },
  EXPLORE_TESTS: {
    route: '/tests',
    name: 'Tests',
    key: 'tests',
    title: 'Tests | Dyff',
    icon: <IconRouteSquare />
  },
  EXPLORE_SYSTEMS: {
    route: '/systems',
    name: 'Systems',
    key: 'systems',
    title: 'Systems | Dyff',
    icon: <IconBoxModel2 />
  },
  EXPLORE_REPORTS: {
    route: '/reports',
    name: 'Reports',
    key: 'reports',
    title: 'Test Report | Dyff',
    icon: <></>
  },
  OPERATOR_DASHBOARD: {
    route: '/dashboard',
    name: 'Dashboard',
    key: 'dashboard/dashboard',
    title: 'Dashboard | Dyff',
    icon: <IconLayoutDashboard />
  },
  OPERATOR_METHODS: {
    route: '/dashboard/methods',
    name: 'Methods',
    key: 'dashboard/methods',
    title: 'Methods | Dyff',
    icon: <IconTransferIn />
  },
  OPERATOR_DATASETS: {
    route: '/dashboard/datasets',
    name: 'Datasets',
    key: 'dashboard/datasets',
    title: 'Datasets | Dyff',
    icon: <IconDatabase />
  },
  OPERATOR_EVALUATIONS: {
    route: '/dashboard/evaluations',
    name: 'Evaluations',
    key: 'dashboard/evaluations',
    title: 'Evaluations | Dyff',
    icon: <IconZoomCheck />
  },
  OPERATOR_INFERENCE_SERVICES: {
    route: '/dashboard/inferenceservices',
    name: 'Inference Services',
    key: '/dashboard/inferenceservices',
    title: 'Inference Services | Dyff',
    icon: <IconHomeCog />
  },
  OPERATOR_INFERENCE_SESSIONS: {
    route: '/dashboard/inferencesessions',
    name: 'Inference Sessions',
    key: '/dashboard/inferencesessions',
    title: 'Inference Sessions | Dyff',
    icon: <IconDeviceIpadHorizontal />
  },
  OPERATOR_MEASUREMENTS: {
    route: '/dashboard/measurements',
    name: 'Measurements',
    key: 'dashboard/measurements',
    title: 'Measurements | Dyff',
    icon: <IconRuler3 />
  },
  OPERATOR_MODULES: {
    route: '/dashboard/modules',
    name: 'Modules',
    key: 'dashboard/modules',
    title: 'Modules | Dyff',
    icon: <IconBox />
  },
  OPERATOR_MODELS: {
    route: '/dashboard/models',
    name: 'Models',
    key: 'dashboard/models',
    title: 'Models | Dyff',
    icon: <IconBoxModel2 />
  },
  OPERATOR_REPORTS: {
    route: '/dashboard/reports',
    name: 'Reports',
    key: 'dashboard/reports',
    title: 'Reports | Dyff',
    icon: <IconFileDescription />
  },
  OPERATOR_SAFETY_CASES: {
    route: '/dashboard/safetycases',
    name: 'Safetycases',
    key: 'dashboard/safetycases',
    title: 'Safety Cases | Dyff',
    icon: <IconHomeShield />
  },
  OPERATOR_USE_CASES: {
    route: '/dashboard/usecases',
    name: 'Use Cases',
    key: 'dashboard/usecases',
    title: 'Use Cases | Dyff',
    icon: <IconBriefcase />
  },
  USER_PROFILE: {
    route: '/user/profile',
    name: 'User Settings',
    key: 'user',
    title: 'User Settings | Dyff',
    icon: <IconUserCircle />
  },
  AUTH_SIGNOUT: {
    route: '/auth/signout',
    name: 'Sign out',
    key: 'auth/signout',
    title: 'Sign out | Dyff',
    icon: <></>
  },
  AUTH_SIGNIN: {
    route: '/auth/signin',
    name: 'Sign In',
    key: 'auth/signin',
    title: 'Sign In | Dyff',
    icon: <></>
  },
  AUTH_ERROR: {
    route: '/auth/error',
    name: 'Error',
    key: 'auth/error',
    title: 'Error | Dyff',
    icon: <></>
  }
}

export const APP_GRID_CARD_SPAN = { lg: 12, xl: 6 }
export const OPERATOR_APP_SHELL_HEADER_HEIGHT = 40 // Match header and navbar header
export const OPERATOR_APP_SHELL_MAIN_HEIGHT = `calc(100vh - ${OPERATOR_APP_SHELL_HEADER_HEIGHT}px)`
export const EXPLORE_APP_SHELL_HEADER_HEIGHT = 40
export const EXPLORE_APP_SHELL_FOOTER_HEIGHT = 75
export const EXPLORE_APP_SHELL_MAIN_HEIGHT = `calc(100vh - ${EXPLORE_APP_SHELL_HEADER_HEIGHT}px)`

export const GROW_ANIM = {
  in: { opacity: 1, transform: 'scale(1.5)' },
  out: { opacity: 0, transform: 'scale(1)' },
  transitionProperty: 'transform, opacity'
}
export const DEFAULT_SORT = [{ id: 'created', desc: true }]
export const DEFAULT_AUTH_REDIRECT = '/home'

export const DYFF_NOTEBOOK_PATH_IDENTIFIER = 'index.html'
export const DYFF_SCORES_PATH_IDENTIFIER = '.dyff/scores.json'
export const DYFF_LOGS_PATH_IDENTIFIER = '.dyff/logs.txt'
export const DYFF_MEASUREMENTS_PATH_IDENTIFIER = 'parquet'

export const AMMO_BAR_LAYOUT_MIDDLE_AISLE_SPAN = {
  base: 12,
  md: 8,
  xl: 6
}
export const AMMO_BAR_LAYOUT_MIDDLE_AISLE_OFFSET = {
  base: 0,
  md: 2,
  xl: 3
}

export const MIDDLE_LAYOUT_MIN_W = '200px'
export const MIDDLE_LAYOUT_MAX_W = '1200px'
