/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Anchor, Text } from '@mantine/core'

// Footer
export const UL_PRIVACY_POLICY_LINK =
  'https://cdn.bfldr.com/D35SAQ1Q/at/ck6rn9s6785xwvv47mw52t/ULRIPrivacyPolicy.pdf'
export const UL_TERMS_OF_USE_LINK =
  'https://cdn.bfldr.com/D35SAQ1Q/at/m5sz9hkzbwk46sxvk7ggjqpq/ULRITermsOfUse.pdf'
export const UL_COPYRIGHT_TEXT = '© 2025 Underwriters Laboratories Inc.'

export const HOME_SPLASH_TITLE = 'Accelerate AI Risk Assessment'
export const HOME_SPLASH_SUB_TITLE = 'For a better understanding of AI system deployment risks'
export const HOME_SPLASH_BUTTON_TEXT = 'Explore'
export const HOME_SPLASH_TITLE_SIZE = 75
export const HOME_SPLASH_SUB_TITLE_SIZE = 35

export const HOME_USE_CASES_TITLE = 'Review Test Results'
export const HOME_USE_CASES_SUB_TITLE =
  'Measurements of system hazards and vulnerabilities, organized by use case.'
export const HOME_USE_CASES_BUTTON_TEXT = 'Review all test results'
export const HOME_USE_CASES_CARD_MOUSEOVER = 'View Test Results'
export const HOME_USE_CASES_CARD_UNAVAILABLE = 'No use cases available'
export const HOME_USE_CASES_TITLE_SIZE = 50
export const HOME_USE_CASES_SUB_TITLE_SIZE = 30

export const HOME_SYSTEMS_TITLE = 'Browse Systems'
export const HOME_SYSTEMS_SUB_TITLE = 'Use-case-specific test results, organized by AI system.'
export const HOME_SYSTEMS_BUTTON_TEXT = 'Browse all systems'
export const HOME_SYSTEMS_CARD_MOUSEOVER = 'View System'
export const HOME_SYSTEMS_TITLE_SIZE = 50
export const HOME_SYSTEMS_SUB_TITLE_SIZE = 30
export const HOME_SYSTEMS_NO_SYSTEMS_TEXT = 'No systems available'
export const HOME_SYSTEMS_NO_SCORES_TEXT = 'No scores available'
export const MAKE_HOME_SYSTEMS_BUTTON_TEXT = (n?: number) => `Browse ${n ?? 'all'} systems`

export const HOME_SYSTEM_SINGLE_NO_RESULTS_TEXT = 'No test results available'

export const METHODS_TITLE = 'Tests'
export const METHODS_SUB_TITLE =
  'Select a test to compare results for different systems. Tests are organized by the use cases to which they apply.'

export const METHODS_TAB_SCORES_TITLE = 'Results'
export const METHODS_TAB_DOCS_TITLE = 'Documentation'
export const METHODS_CARD_TITLE = 'Test'
export const METHODS_CARD_MOUSEOVER = 'View test result'
export const METHODS_CARD_SCORES_UNAVAILABLE = 'No results available'

export const METHODS_SINGLE_SCORE_MOUSEOVER = 'View test result'
export const METHODS_SINGLE_SCORE_BARCHART_UNKNOWN_NAME = '' // If system / model name or docs is not available, use this

export const SYSTEMS_TITLE = 'Systems'
export const MAKE_SYSTEMS_SUB_TITLE = (link: string) => (
  <Text>
    Select a system to see test results, or{' '}
    <Anchor
      href={link}
      fz="lg"
      fw="bold"
      style={{
        color: 'ul-dark-blue'
      }}
    >
      explore test results
    </Anchor>{' '}
    to search by intended use case instead
  </Text>
)

export const SYSTEMS_LIST_CARD_MOUSEOVER = 'View system' // /systems card mouseover message
export const SYSTEMS_SCORE_CARD_MOUSEOVER = 'View test result' // /systems/id scorecard mouseover message

export const REPORTS_UNAVAILABLE = 'Safety Report Unavailable'
export const USE_CASES_FILTER_UNAVAILABLE = 'No use cases found'
