/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { ScoreSpec } from '@/types/views'

export const SCORE_INDEX_KEY = 'name'
export const DYFF_RANGEMIN_SCORE_LABEL = 'rangemin.ui.dyff.io/'
export const DYFF_RANGEMAX_SCORE_LABEL = 'rangemax.ui.dyff.io/'

export const MAKE_DYFF_RANGEMIN_SCORE_LABEL = (score: ScoreSpec) =>
  `${DYFF_RANGEMIN_SCORE_LABEL}${score[SCORE_INDEX_KEY]}`

export const MAKE_DYFF_RANGEMAX_SCORE_LABEL = (score: ScoreSpec) =>
  `${DYFF_RANGEMAX_SCORE_LABEL}${score[SCORE_INDEX_KEY]}`

export const DEFAULT_SCORE_MIN = 0
export const DEFAULT_SCORE_MAX = 100
export const DYFF_ACCESS_STR = 'dyff.io/access'
export const TOPIC_LABEL_KEY = 'audit'
export const VERSION_LABEL_KEY = 'version'

export const DYFF_USECASE_INDEX_KEY = 'usecases.dyff.io/' // {usecases.dyff.io/<usecase-id>: "1"}
export const DYFF_USECASE_INDEX_VALUE = '1'
