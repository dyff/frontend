/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import TableStatusIcon from '@/components/utility/TableStatusIcon'
import { ArtifactKind, Documentation, Entity, EntityWithDocs, Method } from '@/types/api'
import { Score, ScoreSpec } from '@/types/views'
import {
  Badge,
  Center,
  Group,
  Loader,
  luminance,
  MantineTheme,
  useMantineTheme
} from '@mantine/core'
import { useMediaQuery } from '@mantine/hooks'
import {
  IconAlertCircle,
  IconCircleCheck,
  IconCircleDot,
  IconCircleX,
  IconQuestionMark,
  IconWashDrycleanOff
} from '@tabler/icons-react'
import { AxiosError } from 'axios'
import { MRT_ColumnDef } from 'mantine-react-table'
import { ReadonlyURLSearchParams } from 'next/navigation'
import {
  DEFAULT_AUTH_REDIRECT,
  DYFF_LOGS_PATH_IDENTIFIER,
  DYFF_MEASUREMENTS_PATH_IDENTIFIER,
  DYFF_NOTEBOOK_PATH_IDENTIFIER,
  DYFF_SCORES_PATH_IDENTIFIER
} from './constants/constants'
import { DYFF_USECASE_INDEX_KEY } from './constants/labels'
import {
  DEFAULT_SCORE_MAX,
  DEFAULT_SCORE_MIN,
  MAKE_DYFF_RANGEMAX_SCORE_LABEL,
  MAKE_DYFF_RANGEMIN_SCORE_LABEL
} from './constants/labels'

/*
Statuses
EntityStatus.complete,
EntityStatus.error,
EntityStatus.failed,
EntityStatus.ready,
EntityStatus.terminated,
EntityStatus.deleted,
*/

/**
 * Gets the appropriate icon for a dyff entity status
 * @param status Status string
 * @param colored Whether or not to apply color to the icon. Uses getStatusColor
 * @returns
 */
export function getStatusIcon(s: string | undefined, colored?: boolean): JSX.Element {
  const status = s?.toLowerCase()
  let icon
  switch (status) {
    case 'created':
      icon = <IconCircleDot />
      break
    case 'ready':
    case 'complete':
      icon = <IconCircleCheck />
      break
    case 'error':
    case 'failed':
      icon = <IconAlertCircle />
      break
    case 'deleted':
      icon = <IconWashDrycleanOff />
      break
    case 'terminated':
      icon = <IconCircleX />
      break
    case 'admitted':
      // Loader respects color attribute, not c
      icon = <Loader size={18} color={colored ? 'ul-bright-blue' : 'white'} />
      break
    default:
      icon = <IconQuestionMark />
      break
  }
  // Must wrap in center and pass 'c', as svg doesn't respect custom theme colors
  const props = colored ? { c: getStatusColor(status) } : {}
  return <Center {...props}>{icon}</Center>
}

/**
 * Gets the badge color for a status summary component
 * @param status
 * @returns
 */
export function getStatusColor(s: string | undefined) {
  const status = s?.toLowerCase()
  switch (status) {
    case 'created':
      return 'grey'
    case 'admitted':
      return 'ul-bright-blue'
    case 'ready':
    case 'complete':
      return 'ul-bright-green'
    case 'error':
    case 'failed':
      return 'ul-coral'
    case 'terminated':
      return 'grey'
    case 'deleted':
      return 'orange'
    default:
      return 'black'
  }
}

export function truncateStr(id: string | undefined, len = 10) {
  if (!id) return ''
  return `${id.slice(0, len)}...`
}

export function formatDateStr(dateStr: string | Date | undefined, dateOnly = false) {
  if (!dateStr) return 'N/A'
  try {
    let d
    if (typeof dateStr === 'string') {
      d = new Date(dateStr)
    } else {
      d = dateStr
    }
    return dateOnly ? d.toLocaleDateString() : `${d.toLocaleDateString()} ${d.toLocaleTimeString()}`
  } catch (_) {
    return 'N/A'
  }
}

export function parseResStatus(status: number) {
  switch (status) {
    case 400:
      return 'Invalid client request content. Contact an administrator'
    case 401:
      return 'API Key was not included in request'
    case 403:
      return `You don't have permission to access this resource!`
    case 409:
      return `Invalid resource state`
    case 500:
      return 'API Error: Contact an administrator.'
  }
}

export function parseFetchError(e: unknown) {
  if (e instanceof AxiosError) {
    let msg = 'Unknown error'
    // Check if response and detail (dyff-api error detail)
    if (e.response && e.response.data) {
      msg = e.response.data.detail
        ? JSON.stringify(e.response.data.detail)
        : JSON.stringify(e.response.data)
    } else {
      // No response, show standard error message
      msg = `${e.message}`
    }

    // Rephrase known errors
    if (msg.includes('expected measurement.status == Complete; got Failed')) {
      msg = 'Measurement has status Failed - expected Complete'
    }
    return msg
  } else {
    const err = e as Error
    return err.message
  }
}

export function getUniqueStatuses<T extends Entity>(data: T[] | null) {
  return !data
    ? []
    : data.reduce((unique: string[], d: T) => {
        if (d.status && !unique.includes(d.status)) {
          unique.push(d.status)
        }
        return unique
      }, [])
}

export function makeStatusColumn<T extends Entity>(data: T[] | null): MRT_ColumnDef<T> {
  return {
    id: 'status',
    header: 'Status',
    accessorFn: (d) => d.status,
    Cell: ({ row }) => <TableStatusIcon status={row.original.status} />,
    size: 150,
    filterVariant: 'multi-select',
    mantineFilterMultiSelectProps: {
      data: getUniqueStatuses(data)
    },
    mantineTableBodyCellProps: {
      align: 'center'
    },
    enableSorting: true,
    grow: false
  }
}

export function makeCreatedColumn<T extends Entity>(
  data: T[] | null,
  dateOnly = false
): MRT_ColumnDef<T> {
  return {
    accessorFn: (d) => new Date(d.creationTime!),
    Cell: ({ row }) => formatDateStr(row.original.creationTime, dateOnly),
    id: 'created',
    header: 'Created',
    enableSorting: true,
    filterVariant: 'date-range'
    // mantineTableHeadCellProps: {
    //   align: 'right'
    // },
    // mantineTableBodyCellProps: {
    //   align: 'right'
    // }
  }
}

export function makeLabelsColumn<T extends Entity>(data: T[] | null): MRT_ColumnDef<T> {
  return {
    accessorFn: (d) => (!d.labels ? '' : Object.values(d.labels).join(',')),
    Cell: ({ row }) => {
      const labels = row.original.labels
      if (!labels) return <></>
      return (
        <Group gap="xs">
          {Object.values(labels).map((value) => {
            return <Badge key="value">{value}</Badge>
          })}
        </Group>
      )
    },
    id: 'labels',
    header: 'Labels',
    enableSorting: true
  }
}

export function removeDuplicates<T>(arr: T[]) {
  return Array.from(new Set(arr).values())
}

// Helper to find entity's docs in doc array (for views)
export function findDocs<T extends Entity>(
  entity: T | undefined,
  docs: Documentation[] | undefined
): Documentation | undefined {
  return !entity || !docs ? undefined : docs.find((doc) => doc.entity && doc.entity === entity.id)
}

// Combine entity with docs attribute
export function makeEntityDocs<T extends Entity>(
  entity: T,
  docs: Documentation[] | undefined
): EntityWithDocs<Entity> {
  const theseDocs: Documentation | undefined =
    !entity || !docs ? undefined : docs.find((doc) => doc.entity && doc.entity === entity.id)
  return {
    ...entity,
    docs: theseDocs
  }
}

export function useIsMobile(): Boolean {
  const theme = useMantineTheme()
  const small = theme.breakpoints.sm
  return !!useMediaQuery(`(max-width: ${small})`)
}

export function getInferredArtifactKind(path: string): ArtifactKind {
  return path.includes(DYFF_NOTEBOOK_PATH_IDENTIFIER)
    ? 'notebook'
    : path.includes(DYFF_SCORES_PATH_IDENTIFIER)
      ? 'scores'
      : path.includes(DYFF_LOGS_PATH_IDENTIFIER)
        ? 'logs'
        : path.includes(DYFF_MEASUREMENTS_PATH_IDENTIFIER)
          ? 'measurement'
          : 'unknown'
}

export function getRedirectParameter(params: ReadonlyURLSearchParams) {
  const decodedPath = decodeURIComponent(params.get('redirect') || DEFAULT_AUTH_REDIRECT)
  return encodeURIComponent(decodedPath)
}

export function getPerformanceColor(
  theme: MantineTheme,
  score?: Score
): { fill: string; stroke: string; textColor: string } {
  const defaultColors = {
    fill: theme.colors.gray[5],
    stroke: theme.colors.gray[7],
    textColor: theme.black
  }
  if (!score || score.minimum === undefined || score.maximum === undefined) {
    return defaultColors
  }

  const normalizedScore = (score.quantity - score.minimum) / (score.maximum - score.minimum) // 0 to 1
  const valenceScore = score.valence === 'positive' ? normalizedScore : 1 - normalizedScore // +/- 0 to 1
  const saturation = valenceScore * 100 // Range 0 - 100
  const hue = 250 // Purple
  const fillLightness = 60 // Adjust for starting lightness
  const strokeLightness = fillLightness - 20

  const fillHue = `hsl(${hue}, ${saturation}%, ${fillLightness}%)`
  const strokeHue = `hsl(${hue}, ${saturation}%, ${strokeLightness}%)`

  return {
    fill: fillHue,
    stroke: strokeHue,
    textColor: luminance(fillHue) > 0.5 ? theme.black : theme.white
  }
}

export function getUseCaseLabelKeyById(id: string) {
  return `${DYFF_USECASE_INDEX_KEY}${id}`
}

export function makeScoreLabelMinMax(
  method: Method,
  scoreSpec: ScoreSpec
): { min: number; max: number } | undefined {
  if (method.labels && scoreSpec) {
    const minLabelKey = MAKE_DYFF_RANGEMIN_SCORE_LABEL(scoreSpec)
    const maxLabelKey = MAKE_DYFF_RANGEMAX_SCORE_LABEL(scoreSpec)
    if (
      !!method.labels &&
      Object.keys(method.labels).includes(minLabelKey) &&
      Object.keys(method.labels).includes(maxLabelKey)
    ) {
      try {
        return {
          min: parseInt(method.labels[minLabelKey]),
          max: parseInt(method.labels[maxLabelKey])
        }
      } catch (e) {
        console.warn('Method min/max parameter failed to parse as int')
      }
    }
  }
}

// Calculate plot min/max values for a score graph display
export function makeScoreDynamicMinMax(scores: Score[], useSpecBounds: boolean = false) {
  if (!scores.length) return { min: DEFAULT_SCORE_MIN, max: DEFAULT_SCORE_MAX }
  const refScore = scores[0] // Assumes all scores are same spec - use for spec min/max

  const qtyMin = Math.min(...scores.map((s) => s.quantity))
  const qtyMax = Math.max(...scores.map((s) => s.quantity))
  const qtyRange = Math.abs(qtyMax - qtyMin)

  // Expand outside min/max by the ratio * range
  const RATIO = 0.1
  const BOUND_STEP = qtyRange * RATIO
  const LOWER_BOUND = Math.floor(qtyMin - BOUND_STEP)
  const UPPER_BOUND = Math.ceil(qtyMax + BOUND_STEP)
  const BOUND_RANGE = UPPER_BOUND - LOWER_BOUND

  // If > 10 range, round to nearest ten
  const flooredMin = BOUND_RANGE >= 10 ? Math.floor(LOWER_BOUND / 10) * 10 : LOWER_BOUND
  const ceilMax = BOUND_RANGE >= 10 ? Math.ceil(UPPER_BOUND / 10) * 10 : UPPER_BOUND

  // Default to expanded qty if no min/max set
  const specMin = refScore.minimum ?? flooredMin
  const specMax = refScore.maximum ?? ceilMax

  // Disallow dynamic min/max to leak out of score spec min/max
  const constrainedMin = Math.max(specMin, flooredMin)
  const constrainedMax = Math.min(specMax, ceilMax)

  // Experimental - set # graph ticks
  // const TICKS = 5
  // const range = constrainedMax - constrainedMin
  // const step = range / TICKS
  // const ticks = Array.from(new Array(range / TICKS).fill(0)).map((_, i) => step * i)

  // Return spec min/max with fallback to dynamic data
  if (useSpecBounds) {
    return { min: specMin, max: specMax }
  }

  return { min: constrainedMin, max: constrainedMax }
}

export function ordinalSuffix(num: number) {
  const rem10 = num % 10
  const rem100 = num % 100
  if (rem10 === 1 && rem100 !== 11) return `${num}st`
  if (rem10 === 2 && rem100 !== 12) return `${num}nd`
  if (rem10 === 3 && rem100 !== 13) return `${num}rd`
  return `${num}th`
}
