/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { SessionType } from '@/types/auth'
import { showNotification } from '@mantine/notifications'
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios'
import { AppRouterInstance } from 'next/dist/shared/lib/app-router-context.shared-runtime'
import { PAGE_PERMS } from './constants/constants'

export async function redirectGoogleLogin(router: AppRouterInstance) {
  const authURL = await axios.get('/auth/url').then((res) => res.data.url)
  const googleLoginURL = `${authURL}/login/google`
  router.push(googleLoginURL)
}

export async function verifyAPIKey(
  key: string,
  cb: {
    onSuccess?: () => void
    onFailure?: () => void
  } = {}
): Promise<AxiosResponse> {
  const headers: AxiosRequestConfig['headers'] = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    Authorization: `Bearer ${key}`
  }
  return axios.get('/api/tokens/current', { headers: headers })
}

export function isValidPagePerms(pathname: string, sessionType: SessionType): boolean {
  try {
    // Root always permitted
    if (pathname === '/') {
      return true
    }

    if (!sessionType) {
      return false
    }

    const basePath = pathname.split('/')[1]
    const pageRolePerms = PAGE_PERMS[basePath]

    if (!pageRolePerms) {
      showNotification({ message: `Missing page permissions for basepath ${basePath}` })
      return false
    }

    const validPerms = pageRolePerms.includes(sessionType)
    return validPerms
  } catch (e) {
    showNotification({ message: 'Error evaluating page permissions. Contact an administrator' })
    return false
  }
}
