/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, useIsMobile } from '@/lib/helpers'
import { UseCaseWithMethods } from '@/types/api'
import { Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconCalendar } from '@tabler/icons-react'
import { AnimatePresence, motion } from 'motion/react'
import StackMotion from '../layout/StackMotion'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'
import MethodListCard from './MethodListCard'

const LEFT_FLEX_RATIO = 1
const RIGHT_FLEX_RATIO = 9

interface UseCaseMethodGroupCardProps {
  usecase: UseCaseWithMethods
}

export default function UseCaseMethodGroupCard({ usecase }: UseCaseMethodGroupCardProps) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()

  return (
    <Card
      shadow={undefined}
      withBorder
      style={{ overflow: 'visible' }}
      p={0}
      bg={theme.colors['ul-light-blue'][0]}
      w="100%"
    >
      <Stack gap={0} px="sm">
        {/* Use Case Blob */}
        <Flex wrap="wrap" gap={0} w="100%" p={0} m={0}>
          <Stack
            py="sm"
            gap={0}
            miw={300}
            maw="100%"
            justify="start"
            align={isMobile ? 'stretch' : 'start'}
            flex={`${LEFT_FLEX_RATIO} 1 250px`}
            style={{ borderRadius: theme.radius.md }}
          >
            <Group p={0} m={0} gap="xs">
              <Title order={3}>
                {usecase.documentation && usecase.documentation.title
                  ? usecase.documentation.title
                  : 'Unlabeled Use Case'}
              </Title>
              <OpenInOperatorView
                label="Open Use Case in Operator View"
                href={`${PAGE_DEFS.OPERATOR_USE_CASES.route}/${usecase.id}`}
                position="relative"
              />
            </Group>
            <Title order={6} c="dimmed">
              Use Case
            </Title>

            <Group gap="5px" align="end" mt="2px">
              <IconCalendar size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`Added ${formatDateStr(usecase.creationTime, true)}`}</Text>
            </Group>
          </Stack>
          <Flex p="sm" gap="md" miw={400} maw="100%" flex={`${RIGHT_FLEX_RATIO} 1 350px`}>
            {usecase.documentation && (
              <SafeMDPreview
                source={usecase.documentation.summary}
                containerStyles={{ paddingTop: '3px' }}
              />
            )}
          </Flex>
        </Flex>
        {/* Stack of method cards */}
        <StackMotion gap="sm" mb="sm" layout="size">
          <AnimatePresence>
            {!usecase.methods.length && (
              <motion.div
                layout="position"
                initial={{ opacity: 0, height: 0 }}
                animate={{ opacity: 1, height: 'auto' }}
                exit={{ opacity: 0, height: 0 }}
              >
                <Card w="100%" withBorder bg="rgba(0,0,0,0)" py="xs">
                  <Text c="dimmed" size="sm" mx="auto">
                    No tests available
                  </Text>
                </Card>
              </motion.div>
            )}
            {usecase.methods.map((m, idx) => (
              <motion.div
                key={m.id}
                layout="position"
                initial={{ opacity: 0, height: 0 }}
                animate={{ opacity: 1, height: 'auto' }}
                exit={{ opacity: 0, height: 0 }}
              >
                <MethodListCard key={idx} method={m} />
              </motion.div>
            ))}
          </AnimatePresence>
        </StackMotion>
      </Stack>
    </Card>
  )
}
