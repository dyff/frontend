/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Spoiler, Stack, Text } from '@mantine/core'
import { useEffect, useState } from 'react'

interface ParquetDataDisplayProps {
  parquetStr: string
}

export default function ParquetDataDisplay({ parquetStr }: ParquetDataDisplayProps) {
  const [data, setData] = useState<string[]>([])
  const [error, setError] = useState<string>('')

  // const readParquet = async (): Promise<void> => {
  //   try {
  //     setError('')
  //     const buffer = Buffer.from(parquetStr, 'binary')
  //     const reader = await parquetjs.ParquetReader.openBuffer(buffer)
  //     const cursor = reader.getCursor()
  //     const newData: string[] = []
  //     let record = null
  //     while ((record = await cursor.next())) {
  //       // newData.push(`${record}`)
  //     }
  //     setData(newData)
  //   } catch (e) {
  //     setError(`${e}`)
  //   }
  // }

  // Attempt to parse parquet data
  useEffect(() => {
    // readParquet()
  }, [parquetStr])

  return (
    <Stack>
      {/* <Text>Parsed:</Text>
      {data.map((record) => (
        <Text>{JSON.stringify(record)}</Text>
      ))} */}
      {!!error && <Text c="orange">{error}</Text>}
      <Spoiler maxHeight={0} showLabel="Show raw" hideLabel="Hide">
        <Text>Raw: {parquetStr}</Text>
      </Spoiler>
    </Stack>
  )
}
