/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import classes from '@/styles/Base.module.css'
import { ActionIcon, CopyButton, Group, Stack, Text } from '@mantine/core'
import { useHover } from '@mantine/hooks'
import { showNotification } from '@mantine/notifications'
import { IconExternalLink } from '@tabler/icons-react'

interface InfoChipProps {
  label: string | undefined
  content: string | JSX.Element | undefined
  dimmed?: boolean
  truncate?: boolean
  redirect?: string
  clickToCopy?: boolean
}

export default function InfoChip({
  label,
  content,
  dimmed,
  truncate,
  redirect,
  clickToCopy = false
}: InfoChipProps): JSX.Element {
  const { ref } = useHover()

  if (!content) return <></>
  let contentElem = <></>
  if (typeof content === 'string') {
    // Wrap with copy button if click to copy
    contentElem = clickToCopy ? (
      <CopyButton value={content}>
        {({ copy }) => (
          <Text
            ref={ref}
            className={classes['copy-text']}
            truncate={truncate || clickToCopy ? 'end' : undefined}
            c={dimmed ? 'dimmed' : 'default'}
            onClick={() => {
              copy()
              showNotification({ message: `${content} copied to clipboard` })
            }}
          >
            {content}
          </Text>
        )}
      </CopyButton>
    ) : (
      <Text
        ref={ref}
        truncate={truncate ? 'end' : undefined}
        c={dimmed ? 'dimmed' : 'default'}
        style={{ overflowWrap: 'break-word' }}
      >
        {content}
      </Text>
    )
  } else {
    contentElem = content as JSX.Element
  }
  const headerGroup = !!redirect ? (
    <Group gap="xs">
      <ActionIcon size="xs" variant="outlined" component="a" href={redirect}>
        <IconExternalLink />
      </ActionIcon>
      <Text fw={700}>{label}</Text>
    </Group>
  ) : (
    <Text fw={700}>{label}</Text>
  )

  return (
    <Stack gap={0}>
      {headerGroup}
      {contentElem}
    </Stack>
  )
}
