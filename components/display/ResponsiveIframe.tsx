/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { useEffect, useRef, useState } from 'react'
import { useResizeObserver } from '@mantine/hooks'
import { Box } from '@mantine/core'

export default function ResponsiveIframe({ srcDoc }: { srcDoc: string }) {
  const iframeRef = useRef<HTMLIFrameElement>(null)
  const [iframeBodyRef, setIframeBodyRef] = useState<HTMLElement | null>(null)
  const [iframeHeight, setIframeHeight] = useState(300) // Default height

  // Observe size changes in the iframe's body
  const [ref, rect] = useResizeObserver()

  // Update the height when the observed body size changes
  useEffect(() => {
    if (rect.height) {
      setIframeHeight(rect.height)
    }
  }, [rect.height])

  // Detect when iframe loads to set up the observer
  useEffect(() => {
    const iframe = iframeRef.current
    if (!iframe) return

    const updateIframeBodyRef = () => {
      try {
        const body = iframe.contentDocument?.body
        if (body) {
          setIframeBodyRef(body)
          // @ts-ignore Assigning current works as expected
          ref.current = body // Assign the observer ref
        }
      } catch (error) {
        console.error('Error accessing iframe content.')
      }
    }

    iframe.onload = updateIframeBodyRef
  }, [srcDoc]) // Runs when srcDoc changes

  return (
    <Box w="100%">
      <iframe
        ref={iframeRef}
        srcDoc={srcDoc} // Injecting content dynamically
        style={{ width: '100%', height: `${iframeHeight}px`, border: 'none' }}
      />
    </Box>
  )
}
