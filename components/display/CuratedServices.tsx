/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { getUniqueStatuses, makeCreatedColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceService, InferenceServiceWithModel, Model } from '@/types/api'
import { Box, Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconHomeCog } from '@tabler/icons-react'
import {
  MRT_ColumnDef,
  MRT_GlobalFilterTextInput,
  MRT_ShowHideColumnsButton,
  MRT_ToggleFiltersButton,
  MantineReactTable,
  useMantineReactTable
} from 'mantine-react-table'
import { useMemo } from 'react'
import TableStatusIcon from '../utility/TableStatusIcon'

interface CuratedServicesProps {}

/**
  List of Curated Serices
 */
export default function CuratedServices({}: CuratedServicesProps): JSX.Element {
  const theme = useMantineTheme()
  const {
    data: svcData,
    loading: svcLoading,
    refresh: svcRefresh
  } = useEntityData<InferenceService>('inferenceservices', {})

  const { data: modelData, loading: modelLoading } = useEntityData<Model>('models', {})

  const data: InferenceServiceWithModel[] = useMemo(() => {
    if (svcLoading || modelLoading) return []
    const combSvc: InferenceServiceWithModel[] =
      svcData?.map((svc) => {
        const thisModel: Model | undefined =
          modelData?.find((m) => m.id === svc.model?.id) ?? undefined
        return {
          ...svc,
          model: thisModel
        }
      }) ?? []
    return combSvc
  }, [svcData, modelData, svcLoading, modelLoading])

  const columns = useMemo(
    (): MRT_ColumnDef<InferenceServiceWithModel>[] => [
      {
        id: 'model',
        header: 'Model',
        accessorFn: (svc) => svc.model?.name,
        Cell: ({ row }) => {
          const svc = row.original
          const mdl = row.original.model
          return (
            <Stack gap="5px">
              <Group gap="2px" wrap="nowrap">
                <TableStatusIcon status={mdl?.status ?? svc.status} />
                <Text size="sm" style={{ textWrap: 'wrap' }}>
                  {mdl?.name ?? svc.name}
                </Text>
              </Group>
              {mdl && (
                <Text size="xs" c="dimmed">
                  {mdl.id}
                </Text>
              )}
            </Stack>
          )
        },
        size: 110,
        enableSorting: true
      },
      {
        id: 'service',
        header: 'Service',
        accessorFn: (svc) => svc.name,
        Cell: ({ row }) => {
          const svc = row.original
          const mdl = row.original.model
          const snippedName =
            mdl && svc.name.includes(mdl.name) ? svc.name.replace(mdl.name, '') : svc.name
          return (
            <Stack gap="5px">
              <Group gap="2px" wrap="nowrap">
                <TableStatusIcon status={svc.status} />
                <Text size="sm" style={{ textWrap: 'wrap' }}>
                  {snippedName}
                </Text>
              </Group>
              <Text size="xs" c="dimmed">
                {svc.id}
              </Text>
            </Stack>
          )
        },
        size: 110,
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(svcData)
        },
        mantineTableBodyCellProps: {
          align: 'left'
        },
        enableSorting: true,
        grow: true
      },
      {
        id: 'status',
        header: 'Status',
        accessorKey: 'status',
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(data)
        },
        enableSorting: true,
        visibleInShowHideMenu: false
      }
    ],
    [data]
  )

  const table = useMantineReactTable<InferenceServiceWithModel>({
    columns: columns,
    data: data || [],
    state: {
      isLoading: svcLoading || modelLoading
    },
    mantineTableContainerProps: {
      style: {
        height: '100%',
        border: 0
      }
    },
    mantinePaperProps: {
      style: {
        height: '100%',
        border: 0,
        boxShadow: 'none',
        borderRadius: 0
      }
    },
    mantineTableBodyRowProps: {
      style: {
        // margin: '5px',
        // border: '1px solid black',
        // borderRadius: '5px'
      }
    },
    defaultColumn: { maxSize: 5000, size: 300, enableSorting: false },
    initialState: {
      density: 'xs',
      pagination: { pageSize: 50, pageIndex: 0 },
      showGlobalFilter: true,
      showColumnFilters: false,
      columnOrder: ['mrt-row-expand', 'model', 'service'],
      columnVisibility: {
        status: false
      }
      // sorting: {}
    },
    mantinePaginationProps: {
      rowsPerPageOptions: ['50', '100']
    },
    layoutMode: 'grid',
    paginationDisplayMode: 'pages',
    enableStickyHeader: true,
    enableGlobalFilter: true,
    enableRowVirtualization: true,
    enableTopToolbar: false,
    // enableGrouping: true,
    renderDetailPanel: ({ row }) => {
      // Render expanded detail
      return <Text>Expanded stuff :D</Text>
    },
    mantineExpandButtonProps: {
      style: {}
    }
  })

  return (
    <Stack gap={0} pos="relative" h="100%" flex="1" mih="0">
      <Card
        w="100%"
        p="md"
        style={{ flexShrink: 0, borderRadius: 0 }}
        bg={theme.colors['ul-bright-blue'][0]}
      >
        <Stack>
          <Group>
            <IconHomeCog />
            <Title order={2}>Services</Title>
          </Group>
          <Group>
            <MRT_GlobalFilterTextInput table={table} />
            <Flex ml="auto" gap="sm" justify="start" align="center">
              <MRT_ToggleFiltersButton table={table} />
              <MRT_ShowHideColumnsButton table={table} />
            </Flex>
          </Group>
        </Stack>
      </Card>
      <Stack style={{ flex: 1, minHeight: 0, display: 'flex' }}>
        <MantineReactTable table={table} />
      </Stack>
    </Stack>
  )
}
