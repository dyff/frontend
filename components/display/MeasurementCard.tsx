/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { formatDateStr, getStatusIcon } from '@/lib/helpers'
import { Measurement } from '@/types/api'
import { Card, Divider, Group, Stack, Text, Title } from '@mantine/core'

interface MeasurementCardProps {
  m: Measurement
}

export default function MeasurementCard({ m }: MeasurementCardProps): JSX.Element {
  return (
    <Card withBorder>
      <Group gap={10} align="start">
        <Stack gap={1}>
          <Title order={3}>{m.name}</Title>
          <Text>{m.description}</Text>
          <Group gap={5} align="end">
            {getStatusIcon(m.status, true)}
            <Text>{m.status}</Text>
          </Group>
        </Stack>
        <Divider orientation="vertical" />
        <Stack gap={1}>
          <Group>
            <Text fw={700}>Created</Text>
            <Text>{formatDateStr(m.creationTime)}</Text>
          </Group>
          <Group>
            <Text fw={700}>Method</Text>
            <Text>{m.method.name}</Text>
          </Group>
          <Group>
            <Text fw={700}>Level</Text>
            <Text>{m.level}</Text>
          </Group>
          <Group>
            <Text fw={700}>Account</Text>
            <Text>{m.account}</Text>
          </Group>
        </Stack>
      </Group>
    </Card>
  )
}
