/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { formatDateStr, useIsMobile } from '@/lib/helpers'
import styles from '@/styles/SystemScoreCard.module.css'
import { InferenceServiceWithDocs, ModelWithDocs } from '@/types/api'
import { Box, Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconCalendar, IconFileSearch } from '@tabler/icons-react'
import { useRouter } from 'next/navigation'
import SafeMDPreview from '../utility/SafeMDPreview'

const LEFT_FLEX_RATIO = 1
const RIGHT_FLEX_RATIO = 9

interface UseCaseListCardProps {
  model: ModelWithDocs
  services: InferenceServiceWithDocs[]
}

export default function UseCaseListCard({ model, services }: UseCaseListCardProps) {
  const theme = useMantineTheme()
  const router = useRouter()
  const isMobile = useIsMobile()

  const handleCardClick = () => {
    router.push(`/usecases/${model.id}`)
  }

  return (
    <Box className={styles.cardWrapper} data-label="View Use Case" onClick={handleCardClick}>
      <Card
        shadow={undefined}
        withBorder
        className={`${styles.card}`}
        style={{ overflow: 'visible' }}
        p={0}
      >
        <Flex id="main-container" wrap="wrap" gap="lg" w="100%" p={0} m={0}>
          <Stack
            id="blob-container"
            p="sm"
            gap={0}
            miw={300}
            maw="100%"
            justify="start"
            align={isMobile ? 'stretch' : 'start'}
            flex={`${LEFT_FLEX_RATIO} 1 250px`}
            bg={theme.colors['ul-light-blue'][0]}
            style={{ borderRadius: theme.radius.md }}
          >
            <Title order={3}>
              {model.docs && model.docs.title ? model.docs.title : model.name}
            </Title>
            <Group gap="5px" align="end" mb="0.25rem">
              <IconFileSearch size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`${services.length} evaluation${services.length > 1 ? 's' : ''}`}</Text>
            </Group>
            <Group gap="5px" align="end">
              <IconCalendar size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`Added ${formatDateStr(model.creationTime, true)}`}</Text>
            </Group>
          </Stack>
          <Flex p="sm" gap="md" miw={400} maw="100%" flex={`${RIGHT_FLEX_RATIO} 1 350px`}>
            {model.docs && <SafeMDPreview source={model.docs.summary} />}
          </Flex>
        </Flex>
      </Card>
    </Box>
  )
}
