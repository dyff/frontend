/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { METHODS_SINGLE_SCORE_MOUSEOVER } from '@/lib/constants/copy'
import { getPerformanceColor } from '@/lib/helpers'
import blurStyles from '@/styles/Blur.module.css'
import { CombinedScore } from '@/types/api'
import { Box, Card, Center, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconArrowBigRightLine } from '@tabler/icons-react'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'
import { makeSystemName, makeSystemSummary } from './ScoreBarChart'
import { SystemScoreCircle } from './SystemScoreCircle'

const LEFT_FLEX_RATIO = 8
const RIGHT_FLEX_RATIO = 2

interface SystemScoreCardProps {
  score: CombinedScore
}

// Displays scores of a system - used in /tests/id page
export default function SystemScoreCard({ score }: SystemScoreCardProps) {
  const theme = useMantineTheme()
  const safetyCase = score.safetyCase
  const service = score.service
  const model = score.model

  const { fill, textColor } = getPerformanceColor(theme, score)

  return (
    <Card
      shadow={undefined}
      p="md"
      pr={0}
      withBorder
      className={blurStyles['master']}
      style={{ overflow: 'visible' }}
      component="a"
      href={`${PAGE_DEFS.EXPLORE_REPORTS.route}/${safetyCase.id}`}
      td="none"
    >
      <Flex id="main-container" wrap="wrap" gap="lg" align="stretch" w="100%">
        <Stack id="docs-container" gap={0} miw={300} maw="100%" flex={`${LEFT_FLEX_RATIO} 1 300px`}>
          <Group p={0} m={0} gap="xs">
            <Title order={3} mb={0} className={blurStyles['hover-highlight']}>
              {makeSystemName(score)}
            </Title>
            {!!safetyCase.scope?.inferenceService && (
              <OpenInOperatorView
                position="relative"
                href={`${PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}/${safetyCase.scope?.inferenceService}`}
                label="Open Inf Svc in Operator View"
              />
            )}

            {!!safetyCase.scope?.model && (
              <OpenInOperatorView
                position="relative"
                href={`${PAGE_DEFS.OPERATOR_MODELS.route}/${safetyCase.scope?.model}`}
                label="Open Model in Operator View"
              />
            )}
          </Group>
          <Text c="dimmed" size="xs" mb="xs">
            System
          </Text>
          <Box className={blurStyles['target']}>
            <Box className={blurStyles['hover-label']}>
              <Title order={4} c={theme.colors['ul-dark-blue'][9]}>
                {METHODS_SINGLE_SCORE_MOUSEOVER}
              </Title>
              <IconArrowBigRightLine size="2rem" color={theme.colors['ul-dark-blue'][9]} />
            </Box>
            {!!service?.docs?.summary || !!model?.docs?.summary ? (
              <SafeMDPreview source={makeSystemSummary(score)} />
            ) : (
              <Text c="dimmed">No Description Available</Text>
            )}
          </Box>
        </Stack>
        <Flex
          id="scores-container"
          gap="md"
          justify="flex-end"
          align="center"
          miw={400}
          maw="100%"
          flex={`${RIGHT_FLEX_RATIO} 1 400px`}
        >
          <Card
            withBorder
            py="5px"
            px="10px"
            style={{
              borderTopRightRadius: 0,
              borderBottomRightRadius: 0,
              borderRight: 0
            }}
            bg={fill}
            h="fit-content"
          >
            <Group wrap="nowrap">
              <Center>
                <Text c={textColor}>{score.text}</Text>
              </Center>
              <Flex
                id="circle-container"
                direction="column"
                justify="center"
                align="center"
                w={100}
              >
                <SystemScoreCircle
                  score={score}
                  fill="rgba(0,0,0,0)"
                  stroke={textColor}
                  textColor={textColor}
                  circleDiameter={85}
                />
              </Flex>
            </Group>
          </Card>
        </Flex>
      </Flex>
    </Card>
  )
}
