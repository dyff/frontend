/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { METHODS_CARD_TITLE, SYSTEMS_SCORE_CARD_MOUSEOVER } from '@/lib/constants/copy'
import { formatDateStr, makeScoreDynamicMinMax, ordinalSuffix, useIsMobile } from '@/lib/helpers'
import blurStyles from '@/styles/Blur.module.css'
import { MethodWithSafetyCases } from '@/types/api'
import { Score } from '@/types/views'
import { Box, Card, Center, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconArrowBigRightLine, IconCalendar } from '@tabler/icons-react'
import { useMemo } from 'react'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'
import { SystemScoreCircle } from './SystemScoreCircle'
import { SystemScoreScatter } from './SystemScoreScatter'

const SCORE_LEFT_FLEX_RATIO = 8
const SCORE_RIGHT_FLEX_RATIO = 2
const SCORE_LEFT_MIN_W = '300px'
const SCORE_RIGHT_MIN_W = '400px'
const METHOD_LEFT_FLEX_RATIO = 2
const METHOD_RIGHT_FLEX_RATIO = 8
const METHOD_LEFT_MIN_W = '300px'
const METHOD_RIGHT_MIN_W = '400px'

interface MethodScoreCardProps {
  scores: Score[]
  method: MethodWithSafetyCases
}

// For use in /systems/id
export default function MethodScoreCard({ scores, method }: MethodScoreCardProps) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()

  // Aggregate score data and generate card rows
  const scoreRows = useMemo(() => {
    return method.safetyCases.map((safetyCase, idx) => {
      const theseScores: Score[] =
        scores?.filter(
          (s) =>
            s.metadata.refs.method === method?.id &&
            s.metadata.refs.inferenceService === safetyCase.scope?.inferenceService
        ) ?? []

      // Other system's primary scores
      const otherScores =
        scores?.filter(
          (s) =>
            s.metadata.refs.method === method.id &&
            s.metadata.refs.inferenceService !== safetyCase.scope?.inferenceService &&
            s.quantity !== undefined &&
            s.priority === 'primary'
        ) ?? []

      // dyff-api score spec enforces existence of a primary score
      const primaryScore = theseScores.find((s) => s.priority === 'primary')
      const secondaryScore = theseScores.find((s) => s.priority === 'secondary')

      // Primary -> Secondary -> First score (fallback)
      const thisScore: Score | undefined =
        !!theseScores && !!theseScores.length && !!primaryScore
          ? primaryScore
          : !!secondaryScore
            ? secondaryScore
            : theseScores[0]

      const allScores = thisScore ? [thisScore, ...otherScores] : otherScores
      const validScores = allScores.filter((s) => s.quantity !== undefined)
      const hasValidScores = validScores.length > 0

      const { min: plotMin, max: plotMax } = makeScoreDynamicMinMax(validScores)

      const showPercentile = otherScores.length >= 1

      const numLesserScoreQuantities = otherScores.filter((s) =>
        thisScore.valence === 'positive'
          ? s.quantity <= thisScore.quantity
          : s.quantity > thisScore.quantity
      ).length
      const percentile = Math.round((numLesserScoreQuantities / otherScores.length) * 100)

      return (
        <Card
          key={idx}
          shadow={undefined}
          p={0}
          withBorder
          className={blurStyles['master']}
          style={{ overflow: 'visible' }}
          component="a"
          href={`${PAGE_DEFS.EXPLORE_REPORTS.route}/${safetyCase.id}`}
          td="none"
        >
          <Flex id="main-container" wrap="wrap" gap={0} align="stretch" w="100%" p={0}>
            <Stack
              id="docs-container"
              gap={0}
              miw={SCORE_LEFT_MIN_W}
              maw="100%"
              flex={`${SCORE_LEFT_FLEX_RATIO} 1 ${SCORE_LEFT_MIN_W}`}
              p="sm"
            >
              <Title order={5} mb="xs" className={blurStyles['hover-highlight']}>
                {thisScore.title}
              </Title>
              <Box className={blurStyles['target']}>
                <SafeMDPreview source={thisScore.summary} />
                <Box className={blurStyles['hover-label']}>
                  <Title order={4} c={theme.colors['ul-dark-blue'][9]}>
                    {SYSTEMS_SCORE_CARD_MOUSEOVER}
                  </Title>
                  <IconArrowBigRightLine size="2rem" color={theme.colors['ul-dark-blue'][9]} />
                </Box>
              </Box>
            </Stack>
            <Flex
              id="scores-container"
              gap="xs"
              justify="flex-end"
              align="stretch"
              miw={SCORE_RIGHT_MIN_W}
              maw="100%"
              flex={`${SCORE_RIGHT_FLEX_RATIO} 1 ${SCORE_RIGHT_MIN_W}`}
              p="sm"
            >
              <Flex
                id="scatter-container"
                direction="column"
                justify="center"
                miw={300}
                maw={500}
                flex="1"
                p={0}
              >
                {showPercentile && thisScore && (
                  <Text c="dimmed" size="xs" mb={-8} ta="center">
                    {`${ordinalSuffix(percentile)} percentile out of ${otherScores.length + 1} systems`}
                  </Text>
                )}
                {hasValidScores ? (
                  <Box h={80}>
                    <SystemScoreScatter
                      allScores={allScores}
                      plotMin={plotMin}
                      plotMax={plotMax}
                      mainScoreId={thisScore?.id || ''}
                    />
                  </Box>
                ) : (
                  <Center>
                    <Text>No data available</Text>
                  </Center>
                )}
              </Flex>
              <Flex
                id="circle-container"
                direction="column"
                justify="center"
                align="center"
                w={100}
                style={{ flexShrink: 0 }}
              >
                <SystemScoreCircle score={thisScore} circleDiameter={70} />
              </Flex>
            </Flex>
          </Flex>
        </Card>
      )
    })
  }, [scores])

  return (
    <Card shadow={undefined} withBorder style={{ overflow: 'visible' }} p={0}>
      <Stack gap={0} px="sm">
        {/* Use Case Blob */}
        <Flex wrap="wrap" gap={0} w="100%" p={0} m={0}>
          <Stack
            py="sm"
            gap={0}
            miw={METHOD_LEFT_MIN_W}
            maw="100%"
            justify="start"
            align={isMobile ? 'stretch' : 'start'}
            flex={`${METHOD_LEFT_FLEX_RATIO} 1 ${METHOD_LEFT_MIN_W}`}
            style={{ borderRadius: theme.radius.md }}
          >
            <Box pos="relative">
              <Title order={3}>{method.docs?.title ?? `${method.name} (No doc title)`}</Title>
              <OpenInOperatorView
                href={`${PAGE_DEFS.OPERATOR_METHODS.route}/${method.id}`}
                position="absolute"
                style={{
                  bottom: -30,
                  right: 0
                }}
              />
            </Box>
            <Title order={6} c="dimmed">
              {METHODS_CARD_TITLE}
            </Title>
            <Group gap="5px" align="end" mt="2px">
              <IconCalendar size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`Added ${formatDateStr(method.creationTime, true)}`}</Text>
            </Group>
          </Stack>
          <Flex
            p="sm"
            gap="md"
            miw={METHOD_RIGHT_MIN_W}
            maw="100%"
            flex={`${METHOD_RIGHT_FLEX_RATIO} 1 ${METHOD_RIGHT_MIN_W}`}
          >
            {method.docs ? (
              <SafeMDPreview source={method.docs.summary} containerStyles={{ paddingTop: '3px' }} />
            ) : (
              <Text c="dimmed">No description available</Text>
            )}
          </Flex>
        </Flex>
        {/* Stack of method cards */}
        <Stack gap="sm" mb="sm">
          {scoreRows}
        </Stack>
      </Stack>
    </Card>
  )
}
