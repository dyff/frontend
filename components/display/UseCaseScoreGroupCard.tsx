/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { METHODS_CARD_SCORES_UNAVAILABLE } from '@/lib/constants/copy'
import { formatDateStr, useIsMobile } from '@/lib/helpers'
import { UseCaseWithMethodsWithSafetyCases } from '@/types/api'
import { Score } from '@/types/views'
import { Box, Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconCalendar } from '@tabler/icons-react'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'
import MethodScoreCard from './MethodScoreCard'

const LEFT_FLEX_RATIO = 1
const RIGHT_FLEX_RATIO = 9

interface UseCaseScoreGroupCardProps {
  usecase: UseCaseWithMethodsWithSafetyCases
  scores: Score[]
}

// Aggregate Card used in systems/id page
export default function UseCaseScoreGroupCard({ usecase, scores }: UseCaseScoreGroupCardProps) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()

  return (
    <Card
      w="100%"
      shadow={undefined}
      withBorder
      style={{ overflow: 'visible' }}
      p={0}
      bg={theme.colors['ul-light-blue'][0]}
    >
      <Stack gap={0} px="sm">
        {/* Use Case Blob */}
        <Flex wrap="wrap" gap={0} w="100%" p={0} m={0}>
          <Stack
            py="sm"
            gap={0}
            miw={300}
            maw="100%"
            justify="start"
            align={isMobile ? 'stretch' : 'start'}
            flex={`${LEFT_FLEX_RATIO} 1 250px`}
            style={{ borderRadius: theme.radius.md }}
          >
            <Box pos="relative">
              <Title order={3}>
                {usecase.documentation && usecase.documentation.title
                  ? usecase.documentation.title
                  : 'Unlabeled Use Case'}
              </Title>
              <OpenInOperatorView
                href={`${PAGE_DEFS.OPERATOR_USE_CASES.route}/${usecase.id}`}
                position="absolute"
                style={{
                  bottom: -30,
                  right: 0
                }}
              />
            </Box>
            <Title order={6} c="dimmed">
              Use Case
            </Title>
            <Group gap="5px" align="end" mt="2px">
              <IconCalendar size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`Added ${formatDateStr(usecase.creationTime, true)}`}</Text>
            </Group>
          </Stack>
          <Flex p="sm" gap="md" miw={400} maw="100%" flex={`${RIGHT_FLEX_RATIO} 1 350px`}>
            {usecase.documentation && (
              <SafeMDPreview
                source={usecase.documentation.summary}
                containerStyles={{ paddingTop: '3px' }}
              />
            )}
          </Flex>
        </Flex>
        {/* Stack of method cards */}
        <Stack gap="sm" mb="sm">
          {!usecase.methods.length ? (
            <Card w="100%" withBorder bg="rgba(0,0,0,0)" py="xs">
              <Text c="dimmed" size="sm" mx="auto">
                {METHODS_CARD_SCORES_UNAVAILABLE}
              </Text>
            </Card>
          ) : (
            usecase.methods.map((m, idx) => (
              <MethodScoreCard key={idx} method={m} scores={scores} />
            ))
          )}
        </Stack>
      </Stack>
    </Card>
  )
}
