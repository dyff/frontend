/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Annotations } from '@/types/api'
import { Fieldset, Text } from '@mantine/core'
import InfoChip from './InfoChip'

interface AnnotationsSectionProps {
  annotations: Annotations
}

export default function AnnotationsSection({ annotations }: AnnotationsSectionProps) {
  return (
    <Fieldset legend="Annotations" h="100%">
      {!annotations || !annotations.length ? (
        <Text c="dimmed">No data</Text>
      ) : (
        annotations?.map((d) => <InfoChip key={d.key} label={`${d.key}`} content={d.value} />)
      )}
    </Fieldset>
  )
}
