/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import {
  METHODS_CARD_MOUSEOVER,
  METHODS_CARD_SCORES_UNAVAILABLE,
  METHODS_CARD_TITLE
} from '@/lib/constants/copy'
import { formatDateStr, useIsMobile } from '@/lib/helpers'
import blurStyles from '@/styles/Blur.module.css'
import { MethodWithDocs } from '@/types/api'
import { Box, Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconArrowBigRightLine, IconCalendar } from '@tabler/icons-react'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'

const LEFT_FLEX_RATIO = 7
const RIGHT_FLEX_RATIO = 3
const LEFT_MIN_W = '300px'
const RIGHT_MIN_W = '300px'
const SCORE_PREVIEW_COUNT = 3

interface MethodListCardProps {
  method: MethodWithDocs
}

// Method / Test card - used in /tests page
export default function MethodListCard({ method }: MethodListCardProps) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()

  return (
    <Card
      shadow={undefined}
      withBorder
      className={blurStyles['master']}
      style={{ overflow: 'visible' }}
      p={0}
      component="a"
      href={`${PAGE_DEFS.EXPLORE_TESTS.route}/${method.id}`}
      td="none"
    >
      <Flex wrap="wrap" gap="lg" w="100%" p={0} m={0}>
        <Stack
          p="sm"
          gap={0}
          miw={LEFT_MIN_W}
          maw="100%"
          justify="start"
          align={isMobile ? 'stretch' : 'start'}
          flex={`${LEFT_FLEX_RATIO} 1 ${LEFT_MIN_W}`}
        >
          <Group p={0} m={0} gap="xs">
            <Title order={3} className={blurStyles['hover-highlight']}>
              {method.docs && method.docs.title ? method.docs.title : method.name}
            </Title>
            <OpenInOperatorView
              href={`${PAGE_DEFS.OPERATOR_METHODS.route}/${method.id}`}
              label="Open Method in Operator view"
            />
          </Group>
          <Title order={6} c="dimmed">
            {METHODS_CARD_TITLE}
          </Title>
          <Group gap="5px" align="end" my="2px">
            <IconCalendar size="1rem" color={theme.colors.gray[6]} />
            <Text
              c="dimmed"
              lh="0.8rem"
              size="xs"
            >{`Added ${formatDateStr(method.creationTime, true)}`}</Text>
          </Group>
          {method.docs && method.docs.summary ? (
            <SafeMDPreview source={method.docs.summary} />
          ) : (
            <Text>{method.description}</Text>
          )}
        </Stack>
        <Stack
          px="sm"
          py={5}
          gap={0}
          miw={RIGHT_MIN_W}
          maw="100%"
          flex={`${RIGHT_FLEX_RATIO} 1 ${RIGHT_MIN_W}`}
          bg={theme.colors.gray[0]}
          style={{ borderRadius: 'var(--mantine-radius-md)' }}
          className={blurStyles['target']}
        >
          <Box className={blurStyles['hover-label']}>
            <Title order={4} c={theme.colors['ul-dark-blue'][9]}>
              {METHODS_CARD_MOUSEOVER}
            </Title>
            <IconArrowBigRightLine size="2rem" color={theme.colors['ul-dark-blue'][9]} />
          </Box>
          {!method.scores ? (
            <Text c="dimmed" mx="auto">
              {METHODS_CARD_SCORES_UNAVAILABLE}
            </Text>
          ) : (
            <>
              <Title order={6} mx="auto">
                Scores
              </Title>
              {method.scores
                .map((s, idx) => (
                  <Card key={idx} withBorder w="100%" mt="2px" py="2px" radius="xs">
                    <Group wrap="nowrap">
                      <Text fz="sm">{s.title}</Text>
                    </Group>
                  </Card>
                ))
                .slice(0, SCORE_PREVIEW_COUNT)}
              {method.scores.length > SCORE_PREVIEW_COUNT && (
                <Text c="dimmed" mx="auto" fz="xs" mt="3px" mb={0}>
                  {`+ ${method.scores.length - SCORE_PREVIEW_COUNT} more...`}
                </Text>
              )}
            </>
          )}
        </Stack>
      </Flex>
    </Card>
  )
}
