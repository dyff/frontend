/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Entity } from '@/types/api'
import { Button, Group, Modal } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'
import { useRef, useState } from 'react'

interface NotebookModalProps<T extends Entity> {
  notebookStr: string
}

export default function NotebookModal<T extends Entity>({ notebookStr }: NotebookModalProps<T>) {
  const [opened, { close, open }] = useDisclosure(false)
  const ref = useRef<HTMLIFrameElement>(null)
  const [height, setHeight] = useState('0px')
  const onLoad = () => {
    !!ref && setHeight(ref.current!.contentWindow!.document.body.scrollHeight + 'px')
  }

  return (
    <>
      <Modal
        opened={opened}
        onClose={close}
        size="auto"
        fullScreen
        radius={0}
        transitionProps={{ transition: 'fade', duration: 200 }}
        h="auto"
        styles={{
          body: {
            padding: 0
          },
          header: {
            backgroundColor: 'rgba(0,0,0,0)'
          }
        }}
      >
        <iframe
          ref={ref}
          srcDoc={notebookStr}
          onLoad={onLoad}
          onChange={onLoad}
          width="100%"
          height={height}
          style={{
            border: '0px'
          }}
        />
      </Modal>
      <Group justify="center">
        <Button onClick={open}>{`View Fullscreen`}</Button>
      </Group>
    </>
  )
}
