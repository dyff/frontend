/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import StatusSummary from '@/components/display/StatusSummary'
import { useIsMobile } from '@/lib/helpers'
import classes from '@/styles/Base.module.css'
import { Entity } from '@/types/api'
import { EntityHookError, EntityHookParams, EntityHookRefresh } from '@/types/entity'
import { Card, Flex, Group, Text, Title } from '@mantine/core'
import { MRT_ColumnDef, MRT_SortingState, MantineReactTable } from 'mantine-react-table'
import { useRouter } from 'next/navigation'

interface EntitySummaryProps<T extends Entity> {
  title: string
  page: string
  data: T[] | null
  error: EntityHookError
  loading: boolean
  lastUpdated: Date | null
  refresh: EntityHookRefresh
  params: EntityHookParams
  columns: MRT_ColumnDef<T>[]
  initialSorting?: MRT_SortingState
}

/**
 * Entity summary card. Status is automatically handlded
 * @param title Title at the top of the summary card
 * @param page Name of the page / URI for page redirects
 * @param data Entity[] Array of data
 * @param headers Array of strings for headers. Status is already included
 * @param colMapper Function that returns a list of strings for each row's data
 * @param columns MRT Columns
 * @param initialSorting Initial sorting object array
 */
export default function EntitySummary<T extends Entity>({
  title,
  page,
  data,
  error,
  loading,
  lastUpdated,
  refresh,
  params,
  columns,
  initialSorting = []
}: EntitySummaryProps<T>): JSX.Element {
  const router = useRouter()
  const isMobile = useIsMobile()

  const forcedData = data || ([] as T[])

  return (
    <Card withBorder shadow="sm" h="500px" display="relative">
      <Group align="flex-end" justify="space-between" gap={10} wrap="nowrap">
        <Title
          order={isMobile ? 3 : 1}
          className={classes['pointer-link']}
          onClick={() => {
            router.push(`/dashboard/${page}`)
          }}
          mr="auto"
        >
          {title}
        </Title>
        <StatusSummary data={data} params={params} refresh={refresh} loading={loading} />
      </Group>
      <MantineReactTable
        columns={columns}
        data={forcedData}
        initialState={{
          density: 'xs',
          showGlobalFilter: true,
          sorting: initialSorting
        }}
        mantineTableContainerProps={{
          style: {
            height: '400px'
          }
        }}
        state={{
          isLoading: loading
        }}
        enableStickyHeader={true}
        enableTopToolbar={false}
        enablePagination={false}
        enableTableFooter={false}
        enableRowVirtualization={true}
        defaultColumn={{ maxSize: 5000, size: 300, enableSorting: false }}
        layoutMode="grid-no-grow"
        mantineTableBodyRowProps={({ row }) => ({
          onClick: () => {
            router.push(`/dashboard/${page}/${row.original.id}`)
          },
          style: {
            cursor: 'pointer'
          }
        })}
      />
      <Flex justify="end" h="10px">
        {!!loading ? (
          <Text size="xs" c="dimmed">
            Loading...
          </Text>
        ) : (
          !!lastUpdated && (
            <Text size="xs" c="dimmed">
              Last Updated: {lastUpdated.toLocaleTimeString()}
            </Text>
          )
        )}
      </Flex>
    </Card>
  )
}
