/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { SYSTEMS_LIST_CARD_MOUSEOVER } from '@/lib/constants/copy'
import { formatDateStr, useIsMobile } from '@/lib/helpers'
import blurStyles from '@/styles/Blur.module.css'
import { ModelWithDocsSvcs } from '@/types/api'
import { Box, Card, Flex, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconArrowBigRightLine, IconCalendar } from '@tabler/icons-react'
import OpenInOperatorView from '../nav/OpenInOperatorView'
import SafeMDPreview from '../utility/SafeMDPreview'

const LEFT_FLEX_RATIO = 1
const RIGHT_FLEX_RATIO = 9

interface SystemListCardProps {
  model: ModelWithDocsSvcs
}

// Card used in /systems page
export default function SystemListCard({ model }: SystemListCardProps) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()

  return (
    <Box>
      <Card
        shadow={undefined}
        withBorder
        className={blurStyles['master']}
        style={{ overflow: 'visible' }}
        p={0}
        component="a"
        href={`${PAGE_DEFS.EXPLORE_SYSTEMS.route}/${model.id}`}
        td="none"
      >
        <Flex id="main-container" wrap="wrap" gap={0} w="100%" p={0} m={0}>
          <Stack
            id="blob-container"
            p="xs"
            gap={0}
            miw={300}
            maw="100%"
            justify="start"
            align={isMobile ? 'stretch' : 'start'}
            flex={`${LEFT_FLEX_RATIO} 1 250px`}
            bg={theme.colors['ul-light-blue'][0]}
            style={{ borderRadius: theme.radius.md }}
            pos="relative"
          >
            <Title order={3} className={blurStyles['hover-highlight']}>
              {model.docs && model.docs.title ? model.docs.title : model.name}
            </Title>
            <Group gap="5px" align="end">
              <IconCalendar size="1rem" color={theme.colors.gray[6]} />
              <Text
                c="dimmed"
                lh="0.8rem"
                size="xs"
              >{`Added ${formatDateStr(model.creationTime, true)}`}</Text>
            </Group>
            <OpenInOperatorView
              href={`${PAGE_DEFS.OPERATOR_MODELS.route}/${model.id}`}
              label="Open Model in Operator view"
              position="absolute"
              style={{ top: '5px', right: '5px' }}
            />
          </Stack>
          <Flex
            p="xs"
            gap="md"
            miw={400}
            maw="100%"
            flex={`${RIGHT_FLEX_RATIO} 1 350px`}
            className={blurStyles['target']}
          >
            <Box className={blurStyles['hover-label']}>
              <Title order={4} c={theme.colors['ul-dark-blue'][9]}>
                {SYSTEMS_LIST_CARD_MOUSEOVER}
              </Title>
              <IconArrowBigRightLine size="2rem" color={theme.colors['ul-dark-blue'][9]} />
            </Box>
            {model.docs && model.docs.summary ? (
              <SafeMDPreview source={model.docs.summary} />
            ) : (
              <Text c="dimmed" mt="3px">
                No description available
              </Text>
            )}
          </Flex>
        </Flex>
      </Card>
    </Box>
  )
}
