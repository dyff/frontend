/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Card, Group, Stack, Text } from '@mantine/core'

interface InfoChipProps {
  label: string | undefined
  content: string | JSX.Element | undefined
}

export default function InfoCard({ label, content }: InfoChipProps): JSX.Element {
  return (
    <Stack gap={2} h="100%">
      <Text fw={700}>{label}</Text>
      <Group gap={4}>{content}</Group>
    </Stack>
  )
}
