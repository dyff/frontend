/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { EntityHookError } from '@/types/entity'
import { Center, Group, Text, Transition } from '@mantine/core'
import { IconExclamationCircle } from '@tabler/icons-react'

export default function EntityFetchErrorDisplay({ error }: { error: EntityHookError }) {
  const errText = !error
    ? 'Unknown error!'
    : error.status === 401
      ? `You don't have permission to access this resource!`
      : error.message
  return (
    <Transition transition="fade" mounted={!!error}>
      {(styles) => (
        <Center style={styles} p="sm">
          <Group gap="xs" c="ul-coral" align="start">
            <IconExclamationCircle />
            <Text c="dark">{errText}</Text>
          </Group>
        </Center>
      )}
    </Transition>
  )
}
