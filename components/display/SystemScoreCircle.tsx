/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { getPerformanceColor } from '@/lib/helpers'
import { Score } from '@/types/views'
import { Box, useMantineTheme } from '@mantine/core'
import { useMemo } from 'react'

interface SystemScoreCircleProps {
  score?: Score
  textColor?: string
  fill?: string
  stroke?: string
  circleDiameter?: number
}

export function SystemScoreCircle({
  score,
  textColor,
  stroke,
  fill,
  circleDiameter = 100
}: SystemScoreCircleProps) {
  const theme = useMantineTheme()
  const circleRadius = circleDiameter / 2
  const maxFontSize = circleDiameter / 5
  const minFontSize = circleDiameter / 9
  const scaleFactor = 0.4
  const strokeWidth = 2

  const {
    fill: calcFill,
    stroke: calcStroke,
    textColor: calcTextColor
  } = getPerformanceColor(theme, score)
  const thisFill = !!fill ? fill : calcFill
  const thisStroke = !!stroke ? stroke : calcStroke
  const thisTextColor = !!textColor ? textColor : calcTextColor

  const hasValidScore = !!score && !!score.quantityString

  const fontSize = useMemo(() => {
    if (!hasValidScore || !score.quantityString) return maxFontSize
    const textLength = score.quantityString.length
    const calculatedSize = Math.floor(circleRadius / (textLength * scaleFactor))
    return Math.max(Math.min(calculatedSize, maxFontSize), minFontSize)
  }, [hasValidScore, score])

  return (
    <Box h={circleDiameter} w={circleDiameter}>
      <svg
        width="100%"
        height="100%"
        viewBox={`0 0 ${circleDiameter} ${circleDiameter}`}
        aria-hidden="true"
      >
        <circle
          cx={circleRadius}
          cy={circleRadius}
          r={circleRadius - strokeWidth / 2}
          fill={thisFill}
          stroke={thisStroke}
          strokeWidth={strokeWidth}
        />
        <text
          x={circleRadius}
          y={circleRadius}
          textAnchor="middle"
          fill={thisTextColor}
          fontWeight="bold"
          dominantBaseline="central"
          fontSize={fontSize}
        >
          {hasValidScore ? score.quantityString : 'N/A'}
        </text>
      </svg>
    </Box>
  )
}
