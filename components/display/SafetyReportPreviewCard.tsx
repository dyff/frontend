/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { GROW_ANIM } from '@/lib/constants/constants'
import classes from '@/styles/Base.module.css'
import { MethodWithDocs } from '@/types/api'
import { Card, Group, Stack, Text, Title, Transition, useMantineColorScheme } from '@mantine/core'
import { useHover } from '@mantine/hooks'
import { IconSquareArrowRight } from '@tabler/icons-react'
import clsx from 'clsx'
import { useRouter } from 'next/navigation'
import SafeMDPreview from '../utility/SafeMDPreview'

interface SafetyReportCardPreviewCardProps {
  method: MethodWithDocs
  redirect?: string
  w?: string
}

export default function SafetyReportCardPreviewCard({
  method,
  redirect,
  w = '100%'
}: SafetyReportCardPreviewCardProps) {
  const isDark = useMantineColorScheme().colorScheme === 'dark'
  const { ref, hovered } = useHover()
  const router = useRouter()
  const handleRedirect = () => {
    const redirectPath = !!redirect ? redirect : `/usecases/${method.id}`
    router.push(redirectPath)
  }
  return (
    <Card
      w={w}
      ref={ref}
      withBorder
      key={method.id}
      className={clsx({
        [classes.homeCardDark]: isDark,
        [classes.homeCardLight]: !isDark
      })}
      onClick={() => {
        handleRedirect()
      }}
    >
      <Group wrap="nowrap">
        <Stack gap="xs">
          <Title order={3}>{method.docs?.title ? method.docs?.title : method.name}</Title>
          <SafeMDPreview
            source={method.docs?.summary ? method.docs?.summary : method.description}
          />
        </Stack>
        <Stack ml="auto" w={30}>
          <Transition mounted={hovered} transition="slide-right" timingFunction="ease" keepMounted>
            {(transtionStyle) => (
              <IconSquareArrowRight
                style={{
                  ...transtionStyle
                }}
                size={30}
              />
            )}
          </Transition>
        </Stack>
      </Group>
    </Card>
  )
}
