/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { getPerformanceColor } from '@/lib/helpers'
import { Score } from '@/types/views'
import { Card, Text, useMantineTheme } from '@mantine/core'
import { ResponsiveScatterPlot, ScatterPlotDatum, ScatterPlotNodeProps } from '@nivo/scatterplot'
import { animated } from '@react-spring/web'
import { useMemo } from 'react'

const MAIN_SCORE_RADIUS = 5
const OTHER_SCORE_RADIUS = 3

interface ScoreScatterPlotDatum extends Omit<ScatterPlotDatum, 'x' | 'y'>, Score {
  x: number
  y: number
}

interface ColoredNodeProps<RawDatum extends ScoreScatterPlotDatum>
  extends ScatterPlotNodeProps<RawDatum> {
  mainScoreId: string
  otherScoreColors: { fill: string; stroke: string }
}

const ColoredNode = <RawDatum extends ScoreScatterPlotDatum>({
  node,
  style,
  mainScoreId,
  otherScoreColors,
  blendMode
}: ColoredNodeProps<RawDatum>) => {
  const theme = useMantineTheme()
  const isMainScore = node.data.id === mainScoreId
  const { fill, stroke } = isMainScore ? getPerformanceColor(theme, node.data) : otherScoreColors

  return (
    <animated.circle
      cx={style.x}
      cy={style.y}
      r={isMainScore ? MAIN_SCORE_RADIUS : OTHER_SCORE_RADIUS}
      fill={fill}
      stroke={stroke}
      strokeWidth={1}
      style={{ mixBlendMode: blendMode }}
    />
  )
}

interface SystemScoreScatterProps {
  allScores: Score[]
  plotMin: number
  plotMax: number
  mainScoreId: string
}

export function SystemScoreScatter({
  allScores,
  plotMin,
  plotMax,
  mainScoreId
}: SystemScoreScatterProps) {
  const theme = useMantineTheme()

  const otherScoreColors = {
    fill: theme.colors.gray[4],
    stroke: theme.colors.gray[6]
  }

  const chartData = useMemo(() => {
    const sortedScores = [...allScores].sort((a, b) => a.quantity - b.quantity)
    const yValues: { [key: string]: number } = {}

    const getJitteredY = () => {
      return (Math.random() - 0.5) * 1.8 // Range from -0.9 to 0.9
    }

    // Jitter Y coord
    sortedScores.forEach((score) => {
      if (score.id === mainScoreId) {
        yValues[score.id] = 0 // Main score always y=0
      } else {
        yValues[score.id] = getJitteredY()
      }
    })

    // Ensure the main score is last in the array so it's drawn on top
    const mainScoreIndex = sortedScores.findIndex((s) => s.id === mainScoreId)
    if (mainScoreIndex !== -1) {
      const mainScore = sortedScores.splice(mainScoreIndex, 1)[0]
      sortedScores.push(mainScore)
    }

    return [
      {
        id: 'scores',
        data: sortedScores.map((score) => ({
          x: score.quantity,
          y: yValues[score.id],
          ...score
        }))
      }
    ]
  }, [allScores, mainScoreId])

  return (
    <ResponsiveScatterPlot<ScoreScatterPlotDatum>
      data={chartData}
      margin={{ top: 30, right: 20, bottom: 20, left: 20 }}
      xScale={{ type: 'linear', min: plotMin, max: plotMax }}
      yScale={{ type: 'linear', min: -1, max: 1 }}
      axisTop={{
        tickSize: 0,
        tickPadding: 5,
        tickRotation: 0,
        format: (value) => value.toFixed(0),
        tickValues: [plotMin, plotMax]
      }}
      axisRight={null}
      axisBottom={null}
      axisLeft={null}
      enableGridX={true}
      enableGridY={true}
      gridYValues={[0]}
      gridXValues={[plotMin, plotMax]}
      nodeSize={4}
      useMesh={true}
      nodeComponent={(props) => (
        <ColoredNode<ScoreScatterPlotDatum>
          key={props.node.data.id}
          {...props}
          mainScoreId={mainScoreId}
          otherScoreColors={otherScoreColors}
        />
      )}
      tooltip={({ node }) => (
        <div style={{ position: 'relative', zIndex: 9999 }}>
          <Card padding="xs" shadow="sm" style={{ background: 'white' }}>
            <Text size="sm" fw={500}>
              {node.data.quantityString}
            </Text>
            <Text size="xs" c="dimmed">
              {node.data.id === mainScoreId ? 'This System' : 'Other System'}
            </Text>
          </Card>
        </div>
      )}
      layers={['grid', 'axes', 'nodes', 'markers', 'mesh']}
    />
  )
}
