/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { GROW_ANIM } from '@/lib/constants/constants'
import classes from '@/styles/Base.module.css'
import { Box, Card, Group, Stack, Transition } from '@mantine/core'
import { useHover } from '@mantine/hooks'
import { IconSquareArrowRight } from '@tabler/icons-react'
import { useRouter } from 'next/navigation'

interface RedirectCardProps {
  redirect: string
  w?: string
  children: JSX.Element
}

export default function RedirectCard({ redirect, w = '100%', children }: RedirectCardProps) {
  const { ref, hovered } = useHover()
  const router = useRouter()
  const handleRedirect = () => {
    router.push(redirect)
  }
  return (
    <Card
      w={w}
      ref={ref}
      withBorder
      className={classes['home-card']}
      onClick={() => {
        handleRedirect()
      }}
    >
      <Group wrap="nowrap">
        <Stack gap="xs">{children}</Stack>
        <Stack ml="auto">
          <Box w="20px">
            <Transition mounted={hovered} transition={GROW_ANIM} timingFunction="ease" keepMounted>
              {(transtionStyle) => (
                <IconSquareArrowRight
                  style={{
                    ...transtionStyle,
                    paddingRight: '3px'
                  }}
                  size={30}
                />
              )}
            </Transition>
          </Box>
        </Stack>
      </Group>
    </Card>
  )
}
