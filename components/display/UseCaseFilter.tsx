/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { PAGE_DEFS } from '@/lib/constants/constants'
import { USE_CASES_FILTER_UNAVAILABLE } from '@/lib/constants/copy'
import { UseCase } from '@/types/api'
import { Button, Card, Group, Skeleton, Stack, Text, TextInput, Title } from '@mantine/core'
import { animated, useSpring } from '@react-spring/web'
import { IconChevronsDown, IconChevronsUp, IconSearch } from '@tabler/icons-react'
import { useRouter, useSearchParams } from 'next/navigation'
import { Fragment, useEffect, useMemo, useRef, useState } from 'react'
import OpenInOperatorView from '../nav/OpenInOperatorView'

export const ALL_USECASES_KEY = 'all'
export const MAX_USECASES_DISPLAY = 5
const OVERFLOW_H = 35
const MAX_EXPANDED_H = 100
const SINGLE_H = 20
const PEEK_BTN_H = 20
const PEEK_H = SINGLE_H + PEEK_BTN_H

interface UseCaseFilterProps {
  usecases: UseCase[] | undefined
  selectedUseCase: string
  setSelectedUseCase: (newValue: string) => void
  useCaseSearchText: string
  setUseCaseSearchText: (newValue: string) => void
  loading: boolean
}

export function UseCaseFilter({
  usecases,
  selectedUseCase,
  setSelectedUseCase,
  useCaseSearchText,
  setUseCaseSearchText,
  loading
}: UseCaseFilterProps) {
  const router = useRouter()
  const searchParams = useSearchParams()
  const pillsRef = useRef<HTMLDivElement>(null)

  const [isExpanded, setIsExpanded] = useState<boolean>(false)
  const [isOverflow, setIsOverflow] = useState<boolean>(false)

  const handleUsecaseClick = (newId: string) => {
    // Set to all if clicking on already selected useCase
    if (selectedUseCase === newId) {
      setSelectedUseCase('all')
    } else {
      setSelectedUseCase(newId)
    }
  }

  const toggleExpanded = () => {
    if (isExpanded) {
      pillsRef?.current?.scrollTo({
        top: 0,
        left: 0,
        behavior: 'smooth'
      })
    }
    setIsExpanded(!isExpanded)
  }

  // Update search params on use case change
  useEffect(() => {
    const newParams = new URLSearchParams(Array.from(searchParams.entries())) // Clone existing params
    if (selectedUseCase === 'all' || !selectedUseCase) {
      newParams.delete('usecase') // Remove the param if all
    } else {
      newParams.set('usecase', selectedUseCase)
    }
    router.push(`?${newParams.toString()}`, { scroll: false })
  }, [selectedUseCase])

  const useCasePills = useMemo(() => {
    if (!usecases || !usecases.length) return []
    const pills = usecases
      .sort((u) =>
        !u.documentation?.title || useCaseSearchText === ''
          ? 0
          : u.documentation.title.toLowerCase().includes(useCaseSearchText.toLowerCase())
            ? -1
            : 1
      )
      .map((u, idx) => {
        const title = u.documentation?.title
        const isFiltered =
          !u.documentation?.title || useCaseSearchText === ''
            ? true
            : u.documentation.title.toLowerCase().includes(useCaseSearchText.toLowerCase())
        if (!title || !isFiltered) return <Fragment key={idx} />
        return (
          <Button
            key={idx}
            size="compact-xs"
            variant={selectedUseCase === u.id ? 'filled' : 'outline'}
            radius="xl"
            onClick={(_) => handleUsecaseClick(u.id)}
            opacity={isFiltered ? 1 : 0.2}
          >
            {title}
          </Button>
        )
      })

    if (!pills.length) {
      return (
        <Text c="dimmed" size="xs">
          {USE_CASES_FILTER_UNAVAILABLE}
        </Text>
      )
    }
    return pills
  }, [usecases, selectedUseCase, useCaseSearchText])

  const containerHeight = pillsRef?.current?.clientHeight ?? SINGLE_H
  const constrainedHeight = Math.min(MAX_EXPANDED_H, containerHeight)

  const springProps = useSpring({
    height: isExpanded ? constrainedHeight + PEEK_BTN_H : isOverflow ? PEEK_H : SINGLE_H + 10,
    fadeTop: isExpanded ? constrainedHeight : SINGLE_H,
    opacity: isExpanded ? 0 : 1,
    config: { tension: 200, friction: 20 }
  })

  useEffect(() => {
    if (pillsRef && pillsRef.current) {
      setIsOverflow(pillsRef.current.offsetHeight > OVERFLOW_H && !loading)
    }
  }, [pillsRef, useCasePills])

  return (
    <Card withBorder shadow={undefined} w="100%" p="xs">
      <Stack gap="xs">
        <Group gap="xs" pos="relative" w="100%">
          <Title order={4}>Use Cases</Title>
          <Text c="dimmed">Click on a use case to filter</Text>
          <OpenInOperatorView
            label="Open Use Cases in Operator View"
            href={PAGE_DEFS.OPERATOR_USE_CASES.route}
            style={{ marginLeft: '5px', marginTop: 'auto', marginBottom: 'auto' }}
          />
          <TextInput
            leftSectionPointerEvents="none"
            leftSection={<IconSearch size={20} />}
            placeholder="Search use cases"
            onChange={(e) => setUseCaseSearchText(e.target.value)}
            size="xs"
            ml="auto"
          />
        </Group>
        {/* Pills container */}
        <animated.div
          style={{
            height: springProps.height,
            position: 'relative',
            overflow: 'hidden'
          }}
        >
          {/* Actual pill container */}
          <Group
            gap="xs"
            ref={pillsRef}
            h="auto"
            mah={MAX_EXPANDED_H + PEEK_BTN_H}
            style={{ overflowY: isExpanded ? 'auto' : 'hidden' }}
            justify="start"
            pb={isExpanded ? PEEK_BTN_H : 5}
          >
            {loading ? (
              Array(5)
                .fill(null)
                .map((_, i) => <Skeleton key={i} h={SINGLE_H} w="100px" radius="xl" mb="md" />)
            ) : !usecases || !usecases.length ? (
              <Text ta="center" c="dimmed">
                {USE_CASES_FILTER_UNAVAILABLE}
              </Text>
            ) : (
              useCasePills
            )}
          </Group>
          {isOverflow && !loading && (
            <animated.div
              style={{
                top: springProps.fadeTop,
                height: PEEK_BTN_H,
                width: '100%',
                position: 'absolute',
                backgroundImage: 'linear-gradient(to bottom, rgba(255, 255, 255, 0), 10%, white)'
              }}
            >
              <Button
                h={PEEK_BTN_H}
                radius="md"
                size="compact-xs"
                w="100%"
                variant="subtle"
                onClick={toggleExpanded}
              >
                {isExpanded ? <IconChevronsUp size={15} /> : <IconChevronsDown size={15} />}
              </Button>
            </animated.div>
          )}
        </animated.div>
      </Stack>
    </Card>
  )
}
