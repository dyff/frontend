/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { getStatusColor, getStatusIcon } from '@/lib/helpers'
import classes from '@/styles/Base.module.css'
import { Entity } from '@/types/api'
import { EntityHookParams, EntityHookRefresh } from '@/types/entity'
import { ActionIcon, Badge, Flex, Group, ScrollArea, Text, Tooltip } from '@mantine/core'
import { IconReload, IconX } from '@tabler/icons-react'

type StatusSummaryOptions = {
  [status: string]: number
}

type StatusSummaryProps = {
  data: Entity[] | null
  params: EntityHookParams
  refresh: EntityHookRefresh
  loading: boolean
}

/**
 *
 * @param data Entity data
 * @returns
 */
export default function StatusSummary({
  data,
  params,
  refresh,
  loading
}: StatusSummaryProps): JSX.Element {
  // Init so we enforce order of statuses?
  const options = {
    created: 0,
    admitted: 0,
    ready: 0,
    error: 0
  } as StatusSummaryOptions
  // Accumulate option counts
  data &&
    data.length &&
    data.forEach((entity) => {
      if (!entity.status) return
      options[entity.status] = !options[entity.status] ? 1 : options[entity.status] + 1
    })

  return (
    <>
      <ScrollArea
        scrollbars="x"
        scrollbarSize={6}
        scrollHideDelay={0}
        style={{ borderRadius: '5px' }}
        mb="5px"
      >
        <Group gap={5} justify="flex-end" align="center" wrap="nowrap">
          {Object.entries(options).map(([status, count]) => {
            if (!count) return null
            // If specific status is filtered, only show that status
            if (params && params.status && params.status !== status) return
            const border = params.status === status ? '2px solid black' : '0'
            return (
              <Tooltip label={status} position="top" key={`${status}`}>
                <Badge
                  h="28px"
                  color={getStatusColor(status)}
                  style={{ border: border }}
                  onClick={() => {
                    refresh({ status: status })
                  }}
                  className={classes.pointer}
                >
                  <Flex align="center" gap={5}>
                    {getStatusIcon(status)}
                    <Text>{count}</Text>
                  </Flex>
                </Badge>
              </Tooltip>
            )
          })}
        </Group>
      </ScrollArea>
      <Group gap={5} mb="5px">
        {!!params && params.status !== '' && (
          <Tooltip label="Clear filters">
            <ActionIcon
              loading={loading}
              h="100%"
              color="ul-coral"
              className={classes.pointer}
              onClick={() => {
                refresh({ status: '' })
              }}
            >
              <Flex align="center">
                <IconX />
              </Flex>
            </ActionIcon>
          </Tooltip>
        )}
        <Tooltip label="Refresh" position="top">
          <ActionIcon
            loading={loading}
            h="100%"
            color="ul-bright-blue"
            onClick={() => {
              refresh()
            }}
          >
            <Flex align="center">
              <IconReload />
            </Flex>
          </ActionIcon>
        </Tooltip>
      </Group>
    </>
  )
}
