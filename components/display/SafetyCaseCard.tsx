/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { formatDateStr, getStatusIcon } from '@/lib/helpers'
import { SafetyCase } from '@/types/api'
import { Card, Divider, Group, Stack, Text, Title } from '@mantine/core'

interface SafetyCaseCardProps {
  sc: SafetyCase
}

export default function SafetyCaseCard({ sc }: SafetyCaseCardProps): JSX.Element {
  return (
    <Card withBorder>
      <Group gap={10} align="start">
        <Stack gap={1}>
          <Title order={3}>{sc.name}</Title>
          <Text>{sc.description}</Text>
          <Group gap={5} align="end">
            {getStatusIcon(sc.status, true)}
            <Text>{sc.status}</Text>
          </Group>
        </Stack>
        <Divider orientation="vertical" />
        <Stack gap={1}>
          <Group>
            <Text fw={700}>Created</Text>
            <Text>{formatDateStr(sc.creationTime)}</Text>
          </Group>
          <Group>
            <Text fw={700}>Account</Text>
            <Text>{sc.account}</Text>
          </Group>
        </Stack>
      </Group>
    </Card>
  )
}
