/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { PAGE_DEFS } from '@/lib/constants/constants'
import { HOME_USE_CASES_CARD_MOUSEOVER } from '@/lib/constants/copy'
import { formatDateStr } from '@/lib/helpers'
import styles from '@/styles/SystemScoreCard.module.css'
import { UseCase } from '@/types/api'
import { Box, Card, Group, Stack, Text, Title, useMantineTheme } from '@mantine/core'
import { IconCalendar } from '@tabler/icons-react'

interface HomeUseCaseCardProps {
  usecase: UseCase
}

export default function HomeUseCaseCard({ usecase }: HomeUseCaseCardProps) {
  const theme = useMantineTheme()

  return (
    <Box
      className={styles.cardWrapper}
      data-label={HOME_USE_CASES_CARD_MOUSEOVER}
      w="100%"
      maw="400px"
      component="a"
      href={`${PAGE_DEFS.EXPLORE_TESTS.route}?usecase=${usecase.id}`}
    >
      <Card
        shadow={undefined}
        withBorder
        className={`${styles.card}`}
        style={{ overflow: 'visible' }}
        px="md"
        py="xs"
      >
        <Stack gap="xs">
          <Group wrap="nowrap">
            <Stack gap={0}>
              <Title order={3} style={{ textWrap: 'wrap' }}>
                {usecase.documentation?.title ?? 'Unlabeled Use Case'}
              </Title>
              <Group gap="5px" align="end">
                <IconCalendar size="1rem" color={theme.colors.gray[6]} />
                <Text
                  c="dimmed"
                  lh="0.8rem"
                  size="xs"
                >{`Added ${formatDateStr(usecase.creationTime, true)}`}</Text>
              </Group>
            </Stack>
          </Group>
          <Text>{usecase.documentation?.summary ?? 'Description Unavailable'}</Text>
        </Stack>
      </Card>
    </Box>
  )
}
