/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { DEFAULT_SCORE_MAX, DEFAULT_SCORE_MIN } from '@/lib/constants/labels'
import { METHODS_SINGLE_SCORE_BARCHART_UNKNOWN_NAME } from '@/lib/constants/copy'
import { getPerformanceColor, makeScoreDynamicMinMax, makeScoreLabelMinMax } from '@/lib/helpers'
import { CombinedScore, Method } from '@/types/api'
import { ScoreSpec } from '@/types/views'
import { Box, Card, Group, Stack, Text, useMantineTheme } from '@mantine/core'
import { BarDatum, BarTooltipProps, ResponsiveBar } from '@nivo/bar'
import { useMemo, useState } from 'react'
import PillSegmentedControl from './inputs/PillSegmentedControl'

interface ScoreBarChartProps {
  method: Method | undefined
  scores: CombinedScore[] | undefined
  scoreSpec: ScoreSpec
}

type ChartDatum = BarDatum & {
  id: string
  name: string
  value: number
  serviceId: string
}

type ScaleMode = 'full' | 'auto'

const ONE_BAR_HEIGHT = 25
const BAR_MIN_BAR_COUNT_HEIGHT = 7
const AXIS_LABEL_PX_PER_CHAR = 7

export const makeSystemName = (score?: CombinedScore) =>
  score?.service?.docs?.title ??
  score?.model?.docs?.title ??
  score?.service?.name ??
  score?.model?.name ??
  METHODS_SINGLE_SCORE_BARCHART_UNKNOWN_NAME

export const makeSystemSummary = (score?: CombinedScore) =>
  score?.service?.docs?.summary ?? score?.model?.docs?.summary ?? 'No description available'

const makeValenceHint = (valence?: ScoreSpec['valence']) => {
  if (valence === 'positive') {
    return 'Higher is better'
  } else if (valence === 'negative') {
    return 'Lower is better'
  } else {
    return ``
  }
}

export default function ScoreBarChart({ scores, method, scoreSpec }: ScoreBarChartProps) {
  const theme = useMantineTheme()
  const [scaleMode, setScaleMode] = useState<ScaleMode>('full')

  const chartData: ChartDatum[] = useMemo(() => {
    if (!scores || scores.length === 0) return []
    return scores.map((score: CombinedScore) => ({
      id: score.id,
      serviceId: score.service?.id ?? score.id,
      name: makeSystemName(score),
      value: score.quantity
    }))
  }, [scores])

  const { min, max } = useMemo(() => {
    if (!method || !scores || scores.length === 0 || !scoreSpec) {
      return { min: DEFAULT_SCORE_MIN, max: DEFAULT_SCORE_MAX }
    }

    // Use spec bounds if full scale mode
    if (scaleMode === 'full') {
      return makeScoreDynamicMinMax(scores, true)
    }

    // Returns undefined if no labels found
    const result = makeScoreLabelMinMax(method, scoreSpec)
    if (result) {
      return { min: result.min, max: result.max }
    }

    return makeScoreDynamicMinMax(scores)
  }, [scores, method, scaleMode])

  const memoizedTooltips = useMemo(() => {
    return (
      scores?.map((score) => (
        <Card key={score.id} withBorder shadow="md" style={{ zIndex: 1000 }} p="xs">
          <Text>
            {`${makeSystemName(score)}: `} <b>{score.quantityString}</b>
          </Text>
        </Card>
      )) || []
    )
  }, [scores])

  const customTooltip = ({ index }: BarTooltipProps<ChartDatum>) => {
    return memoizedTooltips[index]
  }

  const { barContainerHeight, barLeftMargin, labelOffset, labelSkip } = useMemo(() => {
    if (!scores || !scores.length) {
      return { barHeight: 200, barLeftMargin: 220 }
    }
    const labelOffsetScalar = 8
    const maxSysNameLen = Math.max(...scores.map((s) => makeSystemName(s).length))
    const maxQuantityLen =
      Math.max(...scores.map((s) => s.quantityString.length)) * labelOffsetScalar
    const labelSkip = maxQuantityLen >= 10 ? maxQuantityLen : 0
    const baseLength = Math.max(BAR_MIN_BAR_COUNT_HEIGHT, scores.length)
    return {
      barContainerHeight: `${ONE_BAR_HEIGHT * baseLength}px`, //
      barLeftMargin: maxSysNameLen * AXIS_LABEL_PX_PER_CHAR, // Margin from left of card to bars
      labelOffset: maxQuantityLen * -1,
      labelSkip: labelSkip
    }
  }, [scores])

  if (!scores || scores.length === 0) {
    return (
      <Card withBorder p="md" style={{ width: '100%' }}>
        <Text>Scores unavailable</Text>
      </Card>
    )
  }

  return (
    <Card withBorder p="md" w="100%" style={{ overflow: 'visible' }}>
      <Stack gap="xs" align="start">
        {!scoreSpec ? (
          <Text>Invalid score selected</Text>
        ) : (
          <Group align="start" wrap="nowrap" w="100%">
            <Group gap="sm">
              <Text size="xl" fw={700}>
                {scoreSpec?.title}
              </Text>
              <Text size="sm" c="dimmed">
                {scoreSpec?.summary}
              </Text>
            </Group>
            <Group ml="auto">
              {!!scoreSpec?.valence && (
                <Text ml="auto" c="dimmed" size="xs" ta="end">
                  {makeValenceHint(scoreSpec.valence)}
                </Text>
              )}
            </Group>
          </Group>
        )}
        {chartData.length === 0 ? (
          <Text>No chart data available</Text>
        ) : (
          <Box style={{ height: barContainerHeight }} w="100%">
            <ResponsiveBar
              data={chartData}
              keys={['value']}
              indexBy="id"
              margin={{ top: 10, right: 50, bottom: 50, left: barLeftMargin }}
              padding={0.3}
              layout="horizontal"
              valueScale={{ type: 'linear', min: min, max: max, clamp: true }}
              indexScale={{ type: 'band', round: true }}
              colors={({ index }) => getPerformanceColor(theme, scores[index]).fill}
              borderColor={({ index }) => getPerformanceColor(theme, scores[index]).stroke}
              tooltip={customTooltip}
              enableGridY={false}
              enableGridX={true}
              axisBottom={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: scoreSpec?.unit ?? '',
                legendPosition: 'middle',
                legendOffset: 32
              }}
              axisLeft={{
                tickSize: 5,
                tickPadding: 5,
                tickRotation: 0,
                legend: '',
                legendPosition: 'middle',
                legendOffset: -40,
                format: (id: string) => makeSystemName(scores.find((s) => s.id === id))
              }}
              axisTop={null}
              axisRight={null}
              label={({ index }) => `${scores[index].quantityString}`}
              labelOffset={labelOffset}
              labelSkipWidth={labelSkip}
              labelSkipHeight={12}
              labelTextColor={({ index }) => getPerformanceColor(theme, scores[index]).textColor}
              labelPosition="end"
              legends={[]}
              role="application"
              ariaLabel="Use Case System Bar Chart"
              barAriaLabel={(e) => e.id + ': ' + e.formattedValue + ' in name: ' + e.indexValue}
            />
          </Box>
        )}
        <PillSegmentedControl
          size="xs"
          value={scaleMode}
          onChange={(value: string) => setScaleMode(value as ScaleMode)}
          data={[
            { label: 'Full', value: 'full' },
            { label: 'Auto', value: 'auto' }
          ]}
          label="Range"
        />
      </Stack>
    </Card>
  )
}
