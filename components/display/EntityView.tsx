/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Entity } from '@/types/api'
import { EntityHookError, EntityHookRefresh } from '@/types/entity'
import { ActionIcon, Box, Flex, Text, Tooltip } from '@mantine/core'
import { IconCirclePlus, IconReload } from '@tabler/icons-react'
import {
  MRT_ColumnDef,
  MRT_ShowHideColumnsButton,
  MRT_SortingState,
  MRT_ToggleFiltersButton,
  MantineReactTable
} from 'mantine-react-table'
import { useRouter } from 'next/navigation'
import EntityFetchErrorDisplay from './EntityFetchErrorDisplay'

interface EntityViewProps<T extends Entity> {
  title: string
  page: string
  data: T[] | null
  loading: boolean
  columns: MRT_ColumnDef<T>[]
  lastUpdated: Date | null
  error: EntityHookError
  refresh: EntityHookRefresh
  tableHeight?: string
  canCreate?: boolean
  initialSorting?: MRT_SortingState
  enableBottomToolbar?: boolean
}

/**
 * Entity summary card. Status is automatically handled
 * @param page Page name for redirects
 * @param data Entity[] Array of data
 * @param loading Loading boolean of data
 * @param columns MRT Columns
 * @param lastUpdated lastUpdated Date
 * @param error Error if present
 * @param refresh Function to re-fetch data
 * @param initialSorting Initial sorting object array
 * @param tableHeight Table height
 * @param canCreate Enable create modal (default false)
 * @param enableBottomToolbar Override visibility of bottom toolbar (default true)
 */
export default function EntityView<T extends Entity>({
  page,
  data,
  loading,
  columns,
  lastUpdated,
  error,
  refresh,
  initialSorting = [],
  tableHeight = 'calc(80vh - 230px)',
  canCreate = false,
  enableBottomToolbar = true
}: EntityViewProps<T>): JSX.Element {
  const router = useRouter()
  const forcedData = data || ([] as T[])

  return (
    <Box pos="relative" p="md" h={tableHeight}>
      <MantineReactTable
        columns={columns}
        data={forcedData}
        mantineTableContainerProps={{
          style: {
            height: tableHeight
          }
        }}
        state={{
          isLoading: loading
        }}
        defaultColumn={{ maxSize: 5000, size: 300, enableSorting: false }}
        initialState={{
          density: 'xs',
          pagination: { pageSize: 50, pageIndex: 0 },
          showGlobalFilter: true,
          showColumnFilters: true,
          sorting: initialSorting
        }}
        mantinePaginationProps={{
          rowsPerPageOptions: ['50', '100']
        }}
        paginationDisplayMode="pages"
        enableStickyFooter
        enableStickyHeader
        enableRowVirtualization
        positionGlobalFilter="left"
        layoutMode="grid"
        mantineTableBodyRowProps={({ row }) => ({
          onClick: () => {
            router.push(`/dashboard/${page}/${row.original.id}`)
          },
          style: {
            cursor: 'pointer'
          }
        })}
        enableBottomToolbar={enableBottomToolbar}
        renderToolbarInternalActions={({ table }) => (
          <Flex gap="sm" justify="start" align="center">
            <MRT_ToggleFiltersButton table={table} />
            <MRT_ShowHideColumnsButton table={table} />
            {!!canCreate && (
              <Tooltip label="New" position="top">
                <ActionIcon
                  loading={loading}
                  color="ul-bright-blue"
                  onClick={() => {
                    router.push(`/dashboard/${page}/new`)
                  }}
                >
                  <IconCirclePlus />
                </ActionIcon>
              </Tooltip>
            )}
            <Tooltip label="Refresh" position="top">
              <ActionIcon
                color="ul-bright-blue"
                loading={loading}
                onClick={() => {
                  refresh()
                }}
              >
                <IconReload />
              </ActionIcon>
            </Tooltip>
          </Flex>
        )}
        renderBottomToolbarCustomActions={({ table }) => (
          <Flex gap="sm" justify="start" align="center" p="xs">
            {!!lastUpdated && (
              <Text size="xs" c="gray">
                Last Updated: {lastUpdated.toLocaleTimeString()}
              </Text>
            )}
            {!loading && !!error && <EntityFetchErrorDisplay error={error} />}
          </Flex>
        )}
      />
    </Box>
  )
}
