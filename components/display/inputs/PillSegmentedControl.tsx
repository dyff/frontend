/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  Center,
  Group,
  MantineSize,
  SegmentedControl,
  SegmentedControlProps,
  Text,
  useMantineTheme
} from '@mantine/core'

interface PillSegmentedControlProps {
  value: SegmentedControlProps['value']
  onChange: SegmentedControlProps['onChange']
  data: SegmentedControlProps['data']
  label: string
  size?: MantineSize
  controlSize?: MantineSize
  radius?: MantineSize
}

export default function PillSegmentedControl({
  value,
  onChange,
  data,
  label,
  size = 'lg',
  controlSize = 'sm',
  radius = 'lg'
}: PillSegmentedControlProps) {
  const theme = useMantineTheme()
  return (
    <Group
      gap={0}
      align="stretch"
      bg={theme.colors.gray[0]}
      style={{
        borderRadius: theme.radius[radius]
      }}
      p="0px"
    >
      <Center
        style={{
          borderTopLeftRadius: theme.radius[radius],
          borderBottomLeftRadius: theme.radius[radius],
          borderTopRightRadius: 0,
          borderBottomRightRadius: 0
        }}
      >
        <Text fz={size} fw={700} px="sm">
          {label}
        </Text>
      </Center>
      <SegmentedControl
        value={value}
        size={controlSize}
        onChange={onChange}
        data={data}
        radius={radius}
        withItemsBorders={false}
        styles={{
          root: {
            backgroundColor: theme.colors.gray[0],
            borderRadius: theme.radius[radius]
          },
          indicator: {
            backgroundColor: theme.colors['ul-light-blue'][0]
          },
          label: {
            fontWeight: 700,
            fontSize: theme.fontSizes[size]
          },
          innerLabel: {
            '&:disabled': {
              color: 'default'
            }
          }
        }}
      />
    </Group>
  )
}
