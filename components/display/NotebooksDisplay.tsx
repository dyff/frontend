/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Entity, EntityWithArtifacts } from '@/types/api'
import { Center, Stack, Text } from '@mantine/core'
import { Fragment, useMemo } from 'react'
import NoDataDisplay from './NoData'
import NotebookModal from './NotebookModal'

interface NotebooksDisplayProps<T extends Entity> {
  data: EntityWithArtifacts<T> | null
}

export default function NotebooksDisplay<T extends Entity>({ data }: NotebooksDisplayProps<T>) {
  const artifacts = data?.artifacts
  const notebookArtifacts = artifacts?.data?.filter((a) => a.kind === 'notebook')

  const notebookStack = useMemo(() => {
    return !data || !artifacts || !notebookArtifacts ? (
      <NoDataDisplay />
    ) : artifacts.error || !artifacts.data ? (
      <Center>
        <Text c="dimmed">{artifacts.error || 'No data'}</Text>
      </Center>
    ) : (
      <Stack gap="md">
        {notebookArtifacts.map((a, idx) => {
          const artifactStr = a.data
          return (
            <Fragment key={idx}>
              <NotebookModal notebookStr={artifactStr} />
              <iframe
                srcDoc={artifactStr}
                style={{
                  width: '100%',
                  height: '80vh',
                  border: '0px'
                }}
              />
            </Fragment>
          )
        })}
      </Stack>
    )
  }, [artifacts])

  return notebookStack
}
