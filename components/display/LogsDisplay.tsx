/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Entity, EntityWithLogs } from '@/types/api'
import { Center, Code, Text } from '@mantine/core'
import NoDataDisplay from './NoData'

interface LogsDisplayProps<T extends Entity> {
  logs: EntityWithLogs<T> | null
}

export default function LogsDisplay<T extends Entity>({ logs }: LogsDisplayProps<T>) {
  return !logs ? (
    <NoDataDisplay />
  ) : logs.logs.error || !logs.logs.data || !logs.logs.data.length ? (
    <Center>
      <Text c="dimmed">{logs.logs.error || 'No data'}</Text>
    </Center>
  ) : (
    <Code block>{logs.logs.data.map((logStr) => `${logStr}\n`)}</Code>
  )
}
