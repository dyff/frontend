/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Center, Text } from '@mantine/core'

export default function NoDataDisplay() {
  return (
    <Center>
      <Text c="dimmed">Data unavailable</Text>
    </Center>
  )
}
