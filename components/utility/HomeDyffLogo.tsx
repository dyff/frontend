/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import classes from '@/styles/HomeDyffLogo.module.css'
import Image from 'next/image'

export default function HomeDyffLogo() {
  return (
    <div className={classes['logo-container']}>
      <Image
        className={classes['homepage-logo']}
        src="/static/logo.svg"
        alt="Dyff Logo"
        width={500}
        height={500}
        priority
      />
    </div>
  )
}
