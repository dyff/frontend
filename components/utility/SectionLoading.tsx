/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Skeleton, Stack } from '@mantine/core'

interface SectionLoadingProps {
  rows?: number
  w?: string
  h?: string
}

function SectionLoading({ rows = 5, w = '100%', h = '15px' }: SectionLoadingProps) {
  return (
    <Stack h="100%" w={w} gap={20} justify="start" align="center">
      {Array(rows)
        .fill(null)
        .map((_, i) => (
          <Skeleton key={i} height={h} radius="md" />
        ))}
    </Stack>
  )
}

export default SectionLoading
