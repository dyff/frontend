/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useMantineColorScheme } from '@mantine/core'
import MarkdownPreview, { MarkdownPreviewProps } from '@uiw/react-markdown-preview'
import { CSSProperties } from 'react'
import rehypeSanitize from 'rehype-sanitize'

type SafeMDPreviewProps = {
  source: string | undefined
  maxLength?: number | undefined
  containerStyles?: CSSProperties | undefined
} & MarkdownPreviewProps

export default function SafeMDPreview({
  source,
  maxLength,
  containerStyles,
  ...props
}: SafeMDPreviewProps) {
  const { colorScheme } = useMantineColorScheme()
  const cappedSource = !!maxLength && !!source ? `${source.substring(0, maxLength)}...` : source
  return (
    <span
      data-color-mode={colorScheme}
      style={{
        width: '100%',
        ...containerStyles
      }}
    >
      <MarkdownPreview
        {...props}
        source={cappedSource}
        rehypePlugins={[[rehypeSanitize]]}
        style={{
          backgroundColor: 'rgba(0,0,0,0)',
          width: '100%'
        }}
      />
    </span>
  )
}
