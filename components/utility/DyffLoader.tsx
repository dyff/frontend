/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Box, Center, Loader } from '@mantine/core'
import Image from 'next/image'
import classes from '@/styles/DyffLoader.module.css'

interface DyffLoaderProps {
  centered?: boolean
}

const SIZE = 100

export default function DyffLoader({ centered }: DyffLoaderProps) {
  const loader = (
    <svg
      width={`${SIZE}`}
      height={`${SIZE}`}
      version="1.1"
      viewBox={`0 0 ${SIZE} ${SIZE}`}
      xmlns="http://www.w3.org/2000/svg"
    >
      <g>
        <path
          className={classes['path1']}
          d="m50 4.15-21.1 12.2v24.4l21.1 12.2 21.1-12.2v-24.4zm0 7.41 14.7 8.5v17l-14.7 8.5-14.7-8.5v-17z"
          fill="#58595B"
        />
        <path
          className={classes['path2']}
          d="m74.9 46.8-21.1 12.2v24.4l21.1 12.2 21.1-12.2v-24.4zm0 7.41 14.7 8.5v17l-14.7 8.5-14.7-8.5v-17z"
          fill="#58595B"
        />
        <path
          className={classes['path3']}
          d="m25.1 46.8-21.1 12.2v24.4l21.1 12.2 21.1-12.2v-24.4zm0 7.41 14.7 8.5v17l-14.7 8.5-14.7-8.5v-17z"
          fill="#0A32FF"
        />
      </g>
    </svg>
  )
  return centered ? <Center>{loader}</Center> : loader
}
