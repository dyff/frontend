/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Grid, Skeleton, Stack } from '@mantine/core'

function EntityDetailLoading() {
  return (
    <Grid>
      <Grid.Col span={12}>
        <Skeleton h={50} />
      </Grid.Col>
      <Grid.Col span={3}>
        <Stack h="100%" w="100%" gap={14} justify="start" align="center">
          {Array(8)
            .fill(null)
            .map((_, i) => (
              <Skeleton key={i} height={40} radius="md" />
            ))}
        </Stack>
      </Grid.Col>
      <Grid.Col span={9}>
        <Grid>
          <Grid.Col span={6}>
            <Stack h="100%" w="100%" gap={20} justify="start" align="center">
              <Skeleton height={200} radius="md" />
              <Skeleton height={200} radius="md" />
            </Stack>
          </Grid.Col>
          <Grid.Col span={6}>
            <Stack h="100%" w="100%" gap={20} justify="start" align="center">
              <Skeleton height={200} radius="md" />
              <Skeleton height={200} radius="md" />
            </Stack>
          </Grid.Col>
        </Grid>
      </Grid.Col>
    </Grid>
  )
}

export default EntityDetailLoading
