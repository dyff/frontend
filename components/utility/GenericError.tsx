/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { PAGE_DEFS } from '@/lib/constants/constants'
import { Button, Center, Stack, Title } from '@mantine/core'
import DyffLogo from './DyffLogo'

export default function GenericError() {
  return (
    <Center h="100vh" w="100vw">
      <Stack>
        <DyffLogo size={200} />
        <Title>You look lost!</Title>
        <Button component="a" href={PAGE_DEFS.HOME.route} variant="outline">
          Go home
        </Button>
      </Stack>
    </Center>
  )
}
