/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useMantineColorScheme } from '@mantine/core'
import MDEditor, { MDEditorProps } from '@uiw/react-md-editor'
import rehypeSanitize from 'rehype-sanitize'

export default function SafeMDEditor({ ...props }: MDEditorProps) {
  const { colorScheme } = useMantineColorScheme()
  return (
    <span data-color-mode={colorScheme}>
      <MDEditor
        previewOptions={{
          rehypePlugins: [[rehypeSanitize]]
        }}
        {...props}
      />
    </span>
  )
}
