/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Center } from '@mantine/core'
import DyffLoader from './DyffLoader'

function PageLoading() {
  return (
    <Center h="100vh">
      <DyffLoader />
    </Center>
  )
}

export default PageLoading
