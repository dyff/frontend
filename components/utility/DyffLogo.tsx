/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { Center } from '@mantine/core'
import Image from 'next/image'

export default function DyffLogo({ size }: { size: number }) {
  return (
    <Center>
      <Image src="/static/logo.svg" alt="Dyff Logo" width={size} height={size} priority />
    </Center>
  )
}
