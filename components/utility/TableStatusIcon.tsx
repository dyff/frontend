/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { getStatusIcon } from '@/lib/helpers'
import { Center, Tooltip } from '@mantine/core'

interface TableStatusIconProps {
  status: string | undefined
}

export default function TableStatusIcon({ status }: TableStatusIconProps): JSX.Element {
  return (
    <Center>
      <Tooltip label={status} position="left">
        {getStatusIcon(status, true)}
      </Tooltip>
    </Center>
  )
}
