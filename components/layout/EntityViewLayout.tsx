/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Entity } from '@/types/api'
import { Grid } from '@mantine/core'

interface EntityViewLayoutProps<T extends Entity> {
  top: JSX.Element
  left: JSX.Element
  right: JSX.Element
}
export default function EntityViewLayout<T extends Entity>({
  top,
  left,
  right
}: EntityViewLayoutProps<T>) {
  const sideBarSpan = 3
  return (
    <Grid>
      <Grid.Col span={12}>{top}</Grid.Col>
      <Grid.Col span={{ lg: 12, xl: sideBarSpan }} style={{ alignSelf: 'start' }}>
        {left}
      </Grid.Col>
      <Grid.Col span={{ lg: 12, xl: 12 - sideBarSpan }}>{right}</Grid.Col>
    </Grid>
  )
}
