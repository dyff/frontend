/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Stack, StackProps } from '@mantine/core'
import { motion, MotionProps } from 'framer-motion'

type StackMotionProps = StackProps & MotionProps

const StackMotion = ({ children, ...props }: StackMotionProps) => {
  return (
    <Stack component={motion.div} {...props} pos="relative">
      {children}
    </Stack>
  )
}

export default StackMotion
