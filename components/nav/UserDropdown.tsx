/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { PAGE_DEFS } from '@/lib/constants/constants'
import exploreClasses from '@/styles/ExploreNavShell.module.css'
import classes from '@/styles/UserDropdown.module.css'
import { Avatar, Divider, Loader, NavLink, Popover, Stack } from '@mantine/core'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { useAuth } from '../auth/AuthProvider'

export default function UserDropdown() {
  const pathname = usePathname()
  const { loading, isSignedIn } = useAuth()

  if (loading) return <Loader size="sm" />

  return (
    <Popover shadow="md" width={200} position="bottom-end" offset={1}>
      {!isSignedIn ? (
        <Link
          href={`${PAGE_DEFS.AUTH_SIGNIN.route}?redirect=${encodeURIComponent(pathname)}`}
          className={exploreClasses.nav}
        >
          Sign In
        </Link>
      ) : (
        <Popover.Target>
          <Avatar size="md" className={classes.avatar} style={{ marginLeft: 5 }} />
        </Popover.Target>
      )}
      <Popover.Dropdown p={0}>
        <Stack gap={0} p={0}>
          <NavLink href="/user/profile" label="Account" />
          <Divider />
          <NavLink
            href={`${PAGE_DEFS.AUTH_SIGNOUT.route}?redirect=${encodeURIComponent(pathname)}`}
            label="Sign out"
          />
        </Stack>
      </Popover.Dropdown>
    </Popover>
  )
}
