/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { PAGE_DEFS } from '@/lib/constants/constants'
import { useIsMobile } from '@/lib/helpers'
import { Divider } from '@mantine/core'
import { usePathname } from 'next/navigation'
import { useAuth } from '../auth/AuthProvider'
import BaseNavShell from './BaseNavShell'
import NavItem from './NavItem'

export default function MainNavShell({ children }: { children: any }) {
  const isMobile = useIsMobile()
  const { isSignedIn } = useAuth()
  const pathname = usePathname()
  return (
    <BaseNavShell
      navContent={
        <>
          <NavItem
            route={PAGE_DEFS.HOME.route}
            pageName={PAGE_DEFS.HOME.key}
            label={PAGE_DEFS.HOME.name}
            icon={PAGE_DEFS.HOME.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_DASHBOARD.route}
            pageName={PAGE_DEFS.OPERATOR_DASHBOARD.key}
            label={PAGE_DEFS.OPERATOR_DASHBOARD.name}
            icon={PAGE_DEFS.OPERATOR_DASHBOARD.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}
            pageName={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.key}
            label={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.name}
            icon={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.route}
            pageName={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.key}
            label={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.name}
            icon={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_METHODS.route}
            pageName={PAGE_DEFS.OPERATOR_METHODS.key}
            label={PAGE_DEFS.OPERATOR_METHODS.name}
            icon={PAGE_DEFS.OPERATOR_METHODS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_USE_CASES.route}
            pageName={PAGE_DEFS.OPERATOR_USE_CASES.key}
            label={PAGE_DEFS.OPERATOR_USE_CASES.name}
            icon={PAGE_DEFS.OPERATOR_USE_CASES.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_SAFETY_CASES.route}
            pageName={PAGE_DEFS.OPERATOR_SAFETY_CASES.key}
            label={PAGE_DEFS.OPERATOR_SAFETY_CASES.name}
            icon={PAGE_DEFS.OPERATOR_SAFETY_CASES.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_MEASUREMENTS.route}
            pageName={PAGE_DEFS.OPERATOR_MEASUREMENTS.key}
            label={PAGE_DEFS.OPERATOR_MEASUREMENTS.name}
            icon={PAGE_DEFS.OPERATOR_MEASUREMENTS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_DATASETS.route}
            pageName={PAGE_DEFS.OPERATOR_DATASETS.key}
            label={PAGE_DEFS.OPERATOR_DATASETS.name}
            icon={PAGE_DEFS.OPERATOR_DATASETS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_MODULES.route}
            pageName={PAGE_DEFS.OPERATOR_MODULES.key}
            label={PAGE_DEFS.OPERATOR_MODULES.name}
            icon={PAGE_DEFS.OPERATOR_MODULES.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_MODELS.route}
            pageName={PAGE_DEFS.OPERATOR_MODELS.key}
            label={PAGE_DEFS.OPERATOR_MODELS.name}
            icon={PAGE_DEFS.OPERATOR_MODELS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_REPORTS.route}
            pageName={PAGE_DEFS.OPERATOR_REPORTS.key}
            label={PAGE_DEFS.OPERATOR_REPORTS.name}
            icon={PAGE_DEFS.OPERATOR_REPORTS.icon}
          />
          <NavItem
            route={PAGE_DEFS.OPERATOR_EVALUATIONS.route}
            pageName={PAGE_DEFS.OPERATOR_EVALUATIONS.key}
            label={PAGE_DEFS.OPERATOR_EVALUATIONS.name}
            icon={PAGE_DEFS.OPERATOR_EVALUATIONS.icon}
          />
          <Divider orientation="horizontal" w="100%" />
          {!!isMobile && (
            <>
              {!isSignedIn ? (
                <NavItem
                  route={`${PAGE_DEFS.AUTH_SIGNIN.route}?redirect=${encodeURIComponent(pathname)}`}
                  pageName={PAGE_DEFS.AUTH_SIGNIN.key}
                  label={PAGE_DEFS.AUTH_SIGNIN.name}
                  icon={PAGE_DEFS.AUTH_SIGNIN.icon}
                />
              ) : (
                <>
                  <NavItem
                    route={PAGE_DEFS.USER_PROFILE.route}
                    pageName={PAGE_DEFS.USER_PROFILE.key}
                    label={PAGE_DEFS.USER_PROFILE.name}
                    icon={PAGE_DEFS.USER_PROFILE.icon}
                  />
                  <NavItem
                    route={`${PAGE_DEFS.AUTH_SIGNOUT.route}?redirect=${encodeURIComponent(pathname)}`}
                    pageName={PAGE_DEFS.AUTH_SIGNOUT.key}
                    label={PAGE_DEFS.AUTH_SIGNOUT.name}
                    icon={PAGE_DEFS.AUTH_SIGNOUT.icon}
                  />
                </>
              )}
            </>
          )}
        </>
      }
    >
      {children}
    </BaseNavShell>
  )
}
