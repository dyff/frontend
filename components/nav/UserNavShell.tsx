/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { PAGE_DEFS } from '@/lib/constants/constants'
import { NavLink } from '@mantine/core'
import { IconHome, IconLayoutDashboard, IconUserCircle } from '@tabler/icons-react'
import { usePathname } from 'next/navigation'
import BaseNavShell from './BaseNavShell'

export default function UserNavShell({ children }: { children: any }) {
  const pathname = usePathname()
  return (
    <BaseNavShell
      navContent={
        <>
          <NavLink
            href={PAGE_DEFS.HOME.route}
            label={PAGE_DEFS.HOME.name}
            leftSection={<IconHome />}
          />
          <NavLink
            href={PAGE_DEFS.OPERATOR_DASHBOARD.route}
            label={PAGE_DEFS.OPERATOR_DASHBOARD.name}
            leftSection={<IconLayoutDashboard />}
            color="ul-bright-blue"
          />
          <NavLink
            label="Profile"
            href="/user/profile"
            active={pathname === '/user/profile'}
            leftSection={<IconUserCircle />}
            color="ul-bright-blue"
          />
          {/* <NavLink
            href="/user/keys"
            label="API Keys"
            active={pathname === '/user/keys'}
            leftSection={<IconSquareKey />}
            color="ul-bright-blue"
          /> */}
        </>
      }
    >
      {children}
    </BaseNavShell>
  )
}
