/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Anchor, Breadcrumbs, Text } from '@mantine/core'
import { usePathname } from 'next/navigation'
import { useAuth } from '../auth/AuthProvider'

export default function PageBreadCrumb() {
  const pathNames = usePathname().slice(1).split('/')
  const { loading } = useAuth()
  if (!pathNames || loading) {
    return <></>
  }
  const items = pathNames.map((path, idx) => {
    const redirectLink = '/' + pathNames.slice(0, idx + 1).join('/')
    return (
      <Anchor key={idx} href={redirectLink} fz="xs" c="ul-bright-blue">
        {path}
      </Anchor>
    )
  })
  return (
    <>
      <Breadcrumbs separator="/" separatorMargin="xs" fz="xs">
        {items}
      </Breadcrumbs>
    </>
  )
}
