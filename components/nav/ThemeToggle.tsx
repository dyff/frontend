/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { ActionIcon, useComputedColorScheme, useMantineColorScheme } from '@mantine/core'
import { IconMoon, IconSun } from '@tabler/icons-react'
import cx from 'clsx'
import styles from '@/styles/Base.module.css'

export default function ThemeToggle() {
  const { setColorScheme } = useMantineColorScheme()
  const computedColorScheme = useComputedColorScheme('light', {
    getInitialValueInEffect: true
  })

  const toggleTheme = () => {
    setColorScheme(computedColorScheme === 'light' ? 'dark' : 'light')
  }

  return (
    <ActionIcon onClick={toggleTheme} variant="default" size="md" aria-label="Toggle color scheme">
      <IconSun className={cx(styles.icon, styles.light)} stroke={1.5} />
      <IconMoon className={cx(styles.icon, styles.dark)} stroke={1.5} />
    </ActionIcon>
  )
}
