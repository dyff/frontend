/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  UL_COPYRIGHT_TEXT,
  UL_PRIVACY_POLICY_LINK,
  UL_TERMS_OF_USE_LINK
} from '@/lib/constants/copy'
import { useIsMobile } from '@/lib/helpers'
import classes from '@/styles/ExploreNavShell.module.css'
import { Box, Flex, Stack, Text } from '@mantine/core'
import Image from 'next/image'
import Link from 'next/link'

export default function Footer() {
  const isMobile = useIsMobile()

  return (
    <Flex
      mt="auto"
      direction={isMobile ? 'column' : 'row'}
      bg="ul-dark-blue"
      p="sm"
      align={isMobile ? 'stretch' : 'center'}
      justify={isMobile ? 'center' : 'space-between'}
      gap={isMobile ? 'md' : 0}
    >
      <Flex justify={isMobile ? 'center' : 'flex-start'} w={isMobile ? '100%' : 'auto'} mr="md">
        <Link href="https://dsri.org">
          <Image
            src="/static/dsri-white.svg"
            height={75}
            width={200}
            alt="UL Research Institutes Logo"
          />
        </Link>
      </Flex>

      <Box
        maw={isMobile ? '100%' : '50%'}
        mx={isMobile ? 0 : 'auto'}
        ta={isMobile ? 'center' : 'left'}
      >
        <Text size="xs" c="white" ta="center">
          Research Disclaimer. Safety is highly circumstantial. Safety presented within this
          research is preliminary and may either be wrong or may not apply to the specific
          circumstances faced by the product's users. Please exercise caution.
        </Text>
      </Box>

      <Stack gap={0} align={isMobile ? 'center' : 'flex-end'}>
        <Link className={classes.bottomLink} href={UL_PRIVACY_POLICY_LINK}>
          <Text>Privacy Policy</Text>
        </Link>
        <Link className={classes.bottomLink} href={UL_TERMS_OF_USE_LINK}>
          <Text>Terms of Use</Text>
        </Link>
        <Text className={classes.bottomText}>{UL_COPYRIGHT_TEXT}</Text>
      </Stack>
    </Flex>
  )
}
