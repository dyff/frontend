/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import DyffLogo from '@/components/utility/DyffLogo'
import { EXPLORE_APP_SHELL_HEADER_HEIGHT, PAGE_DEFS } from '@/lib/constants/constants'
import classes from '@/styles/ExploreNavShell.module.css'
import {
  AppShell,
  Burger,
  Divider,
  Group,
  NavLink,
  Popover,
  rgba,
  Stack,
  Switch,
  Text,
  Title,
  useMantineTheme
} from '@mantine/core'
import { useDisclosure, useHover, useWindowScroll } from '@mantine/hooks'
import {
  IconBox,
  IconBoxModel2,
  IconBriefcase,
  IconDatabase,
  IconDeviceIpadHorizontal,
  IconFileDescription,
  IconHelp,
  IconHomeCog,
  IconHomeShield,
  IconLayoutDashboard,
  IconLogin,
  IconLogout,
  IconRouteSquare,
  IconRuler3,
  IconTransferIn,
  IconUserCircle,
  IconZoomCheck
} from '@tabler/icons-react'
import Link from 'next/link'
import { usePathname } from 'next/navigation'
import { useAuth } from '../auth/AuthProvider'
import PageLoading from '../utility/PageLoading'
import Footer from './Footer'
import NavItem from './NavItem'
import UserDropdown from './UserDropdown'

interface ExploreLayout {
  children: JSX.Element
  hideHeader?: boolean
  padding?: string
}

export default function ExploreNavShell({
  children,
  hideHeader,
  padding = ''
}: ExploreLayout): JSX.Element {
  const pathname = usePathname().split('/').at(1)
  const [opened, { toggle }] = useDisclosure()
  const { isSignedIn, loading, showOperatorControls, setShowOperatorControls } = useAuth()
  const fullPathname = usePathname()
  const [scroll] = useWindowScroll()
  const theme = useMantineTheme()
  const { hovered, ref } = useHover()
  const headerBlurCondition = scroll.y > EXPLORE_APP_SHELL_HEADER_HEIGHT
  const headerBlurBg = rgba(theme.white, headerBlurCondition ? 0.8 : 1) // Reduce opacity when scrolled

  return (
    <AppShell
      header={{ height: EXPLORE_APP_SHELL_HEADER_HEIGHT }}
      navbar={{ width: 300, breakpoint: 'sm', collapsed: { desktop: true, mobile: !opened } }}
      padding={padding}
      h="100vh"
    >
      <AppShell.Header
        hidden={hideHeader}
        style={{
          transition: 'background 0.1s ease-in-out',
          background: hovered ? theme.white : headerBlurBg,
          backdropFilter: headerBlurCondition ? 'blur(10px)' : 'none'
        }}
        ref={ref}
      >
        <Group justify="start" align="center" h="100%" px={10} wrap="nowrap">
          <Link className={classes.title} href={PAGE_DEFS.HOME.route}>
            <Group wrap="nowrap">
              <DyffLogo size={30} />
              <Title order={3}>Dyff</Title>
            </Group>
          </Link>
          <Divider orientation="vertical" my={5} visibleFrom="sm" />
          <Group gap={0} wrap="nowrap" visibleFrom="sm">
            <Link
              href={PAGE_DEFS.EXPLORE_TESTS.route}
              className={classes.nav}
              data-active={pathname === PAGE_DEFS.EXPLORE_TESTS.key}
            >
              {PAGE_DEFS.EXPLORE_TESTS.name}
            </Link>
            <Link
              href={PAGE_DEFS.EXPLORE_SYSTEMS.route}
              className={classes.nav}
              data-active={pathname === PAGE_DEFS.EXPLORE_SYSTEMS.key}
            >
              {PAGE_DEFS.EXPLORE_SYSTEMS.name}
            </Link>
            {isSignedIn && (
              <Popover position="bottom-start" offset={2}>
                <Popover.Target>
                  <Group className={classes.nav} wrap="nowrap" w={100}>
                    <Text>Operator</Text>
                  </Group>
                </Popover.Target>
                <Popover.Dropdown p={0}>
                  <NavLink
                    href={undefined}
                    label="Show Operator Controls"
                    leftSection={
                      <Switch
                        size="xs"
                        checked={showOperatorControls}
                        style={{ pointerEvents: 'none' }}
                      />
                    }
                    onClick={(_) => {
                      setShowOperatorControls(!showOperatorControls)
                    }}
                    variant="light"
                    color="ul-bright-blue"
                    disabled={loading}
                  />
                  <Group gap={0}>
                    <Stack p={0} gap={0} justify="start">
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_DASHBOARD.route}
                        pageName={PAGE_DEFS.OPERATOR_DASHBOARD.key}
                        label={PAGE_DEFS.OPERATOR_DASHBOARD.name}
                        icon={PAGE_DEFS.OPERATOR_DASHBOARD.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}
                        pageName={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.key}
                        label={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.name}
                        icon={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.route}
                        pageName={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.key}
                        label={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.name}
                        icon={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_METHODS.route}
                        pageName={PAGE_DEFS.OPERATOR_METHODS.key}
                        label={PAGE_DEFS.OPERATOR_METHODS.name}
                        icon={PAGE_DEFS.OPERATOR_METHODS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_USE_CASES.route}
                        pageName={PAGE_DEFS.OPERATOR_USE_CASES.key}
                        label={PAGE_DEFS.OPERATOR_USE_CASES.name}
                        icon={PAGE_DEFS.OPERATOR_USE_CASES.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_SAFETY_CASES.route}
                        pageName={PAGE_DEFS.OPERATOR_SAFETY_CASES.key}
                        label={PAGE_DEFS.OPERATOR_SAFETY_CASES.name}
                        icon={PAGE_DEFS.OPERATOR_SAFETY_CASES.icon}
                      />
                    </Stack>
                    <Stack p={0} gap={0} justify="start">
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_MEASUREMENTS.route}
                        pageName={PAGE_DEFS.OPERATOR_MEASUREMENTS.key}
                        label={PAGE_DEFS.OPERATOR_MEASUREMENTS.name}
                        icon={PAGE_DEFS.OPERATOR_MEASUREMENTS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_DATASETS.route}
                        pageName={PAGE_DEFS.OPERATOR_DATASETS.key}
                        label={PAGE_DEFS.OPERATOR_DATASETS.name}
                        icon={PAGE_DEFS.OPERATOR_DATASETS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_MODULES.route}
                        pageName={PAGE_DEFS.OPERATOR_MODULES.key}
                        label={PAGE_DEFS.OPERATOR_MODULES.name}
                        icon={PAGE_DEFS.OPERATOR_MODULES.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_MODELS.route}
                        pageName={PAGE_DEFS.OPERATOR_MODELS.key}
                        label={PAGE_DEFS.OPERATOR_MODELS.name}
                        icon={PAGE_DEFS.OPERATOR_MODELS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_REPORTS.route}
                        pageName={PAGE_DEFS.OPERATOR_REPORTS.key}
                        label={PAGE_DEFS.OPERATOR_REPORTS.name}
                        icon={PAGE_DEFS.OPERATOR_REPORTS.icon}
                      />
                      <NavItem
                        route={PAGE_DEFS.OPERATOR_EVALUATIONS.route}
                        pageName={PAGE_DEFS.OPERATOR_EVALUATIONS.key}
                        label={PAGE_DEFS.OPERATOR_EVALUATIONS.name}
                        icon={PAGE_DEFS.OPERATOR_EVALUATIONS.icon}
                      />
                    </Stack>
                  </Group>
                </Popover.Dropdown>
              </Popover>
            )}
          </Group>
          <Group gap={0} ml="auto" wrap="nowrap" visibleFrom="sm">
            <Link href={PAGE_DEFS.DYFF_LANDING.route} className={classes.nav}>
              {PAGE_DEFS.DYFF_LANDING.name}
            </Link>
            <UserDropdown />
            {/* <ThemeToggle /> */}
          </Group>
          <Burger ml="auto" opened={opened} onClick={toggle} hiddenFrom="sm" size="sm" />
        </Group>
      </AppShell.Header>

      <AppShell.Navbar>
        <NavItem
          route={PAGE_DEFS.EXPLORE_TESTS.route}
          pageName={PAGE_DEFS.EXPLORE_TESTS.key}
          label={PAGE_DEFS.EXPLORE_TESTS.name}
          icon={PAGE_DEFS.EXPLORE_TESTS.icon}
        />
        <NavItem
          route={PAGE_DEFS.EXPLORE_SYSTEMS.route}
          pageName={PAGE_DEFS.EXPLORE_SYSTEMS.key}
          label={PAGE_DEFS.EXPLORE_SYSTEMS.name}
          icon={PAGE_DEFS.EXPLORE_SYSTEMS.icon}
        />
        <NavItem
          route={PAGE_DEFS.EXPLORE_SYSTEMS.route}
          pageName={PAGE_DEFS.EXPLORE_SYSTEMS.key}
          label={PAGE_DEFS.EXPLORE_SYSTEMS.name}
          icon={<IconHelp />}
        />
        {isSignedIn ? (
          <>
            <Divider orientation="horizontal" />
            <NavItem
              route={PAGE_DEFS.OPERATOR_DASHBOARD.route}
              pageName={PAGE_DEFS.OPERATOR_DASHBOARD.key}
              label={PAGE_DEFS.OPERATOR_DASHBOARD.name}
              icon={<IconLayoutDashboard />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}
              pageName={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.key}
              label={PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.name}
              icon={<IconHomeCog />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.route}
              pageName={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.key}
              label={PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.name}
              icon={<IconDeviceIpadHorizontal />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_METHODS.route}
              pageName={PAGE_DEFS.OPERATOR_METHODS.key}
              label={PAGE_DEFS.OPERATOR_METHODS.name}
              icon={<IconTransferIn />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_USE_CASES.route}
              pageName={PAGE_DEFS.OPERATOR_USE_CASES.key}
              label={PAGE_DEFS.OPERATOR_USE_CASES.name}
              icon={<IconBriefcase />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_SAFETY_CASES.route}
              pageName={PAGE_DEFS.OPERATOR_SAFETY_CASES.key}
              label={PAGE_DEFS.OPERATOR_SAFETY_CASES.name}
              icon={<IconHomeShield />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_MEASUREMENTS.route}
              pageName={PAGE_DEFS.OPERATOR_MEASUREMENTS.key}
              label={PAGE_DEFS.OPERATOR_MEASUREMENTS.name}
              icon={<IconRuler3 />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_DATASETS.route}
              pageName={PAGE_DEFS.OPERATOR_DATASETS.key}
              label={PAGE_DEFS.OPERATOR_DATASETS.name}
              icon={<IconDatabase />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_MODULES.route}
              pageName={PAGE_DEFS.OPERATOR_MODULES.key}
              label={PAGE_DEFS.OPERATOR_MODULES.name}
              icon={<IconBox />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_MODELS.route}
              pageName={PAGE_DEFS.OPERATOR_MODELS.key}
              label={PAGE_DEFS.OPERATOR_MODELS.name}
              icon={<IconBoxModel2 />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_REPORTS.route}
              pageName={PAGE_DEFS.OPERATOR_REPORTS.key}
              label={PAGE_DEFS.OPERATOR_REPORTS.name}
              icon={<IconFileDescription />}
            />
            <NavItem
              route={PAGE_DEFS.OPERATOR_EVALUATIONS.route}
              pageName={PAGE_DEFS.OPERATOR_EVALUATIONS.key}
              label={PAGE_DEFS.OPERATOR_EVALUATIONS.name}
              icon={<IconZoomCheck />}
            />
            <Divider orientation="horizontal" />
            <NavItem
              route="/user/profile"
              pageName="userprofile"
              label="Account"
              icon={<IconUserCircle />}
            />
            <NavItem
              route="/auth/signout"
              pageName="signout"
              label="Sign Out"
              icon={<IconLogout />}
            />
          </>
        ) : (
          <>
            <NavItem
              route={`/auth/signin?redirect=${encodeURIComponent(fullPathname)}`}
              pageName="signin"
              label="Sign In"
              icon={<IconLogin />}
            />
          </>
        )}
      </AppShell.Navbar>

      <AppShell.Main h="100vh">
        {loading ? (
          <PageLoading />
        ) : (
          <Stack w="100%" mih="100%" gap={0} style={{ overflow: 'hidden' }}>
            {children}
          </Stack>
        )}
        <Footer />
      </AppShell.Main>
    </AppShell>
  )
}
