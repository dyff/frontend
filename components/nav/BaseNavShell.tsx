/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import DyffLogo from '@/components/utility/DyffLogo'
import {
  OPERATOR_APP_SHELL_HEADER_HEIGHT,
  OPERATOR_APP_SHELL_MAIN_HEIGHT
} from '@/lib/constants/constants'
import classes from '@/styles/ExploreNavShell.module.css'
import { AppShell, Box, Burger, Center, Divider, Group, Stack, Title } from '@mantine/core'
import { useDisclosure } from '@mantine/hooks'
import Image from 'next/image'
import Link from 'next/link'
import PageBreadCrumb from './PageBreadCrumb'
import UserDropdown from './UserDropdown'

export default function BaseNavShell({
  children,
  navContent
}: {
  children: JSX.Element
  navContent: JSX.Element
}) {
  const [opened, { toggle }] = useDisclosure()

  return (
    <AppShell
      layout="alt"
      header={{ height: OPERATOR_APP_SHELL_HEADER_HEIGHT }}
      navbar={{
        width: 250,
        breakpoint: 'sm',
        collapsed: { mobile: !opened }
      }}
      // footer={{ height: 80 }}
    >
      <AppShell.Header>
        <Group align="center" h="100%" px={10}>
          <Group hiddenFrom="sm">
            <Burger opened={opened} onClick={toggle} size="sm" />
            <DyffLogo size={30} />
            <Title order={3}>Dyff</Title>
          </Group>
          <PageBreadCrumb />
        </Group>
      </AppShell.Header>

      <AppShell.Navbar>
        <AppShell.Section h={OPERATOR_APP_SHELL_HEADER_HEIGHT - 1}>
          <Center p="sm" h="100%">
            <Burger hiddenFrom="sm" opened={opened} onClick={toggle} size="sm" mr="sm" />
            <Link className={classes.title} href="/dashboard">
              <Group visibleFrom="sm">
                <DyffLogo size={30} />
                <Title order={3}>Dyff</Title>
              </Group>
            </Link>
            <Group ml="auto">
              <Box visibleFrom="sm">
                <UserDropdown />
              </Box>
            </Group>
          </Center>
        </AppShell.Section>
        <Divider />
        <AppShell.Section grow>{navContent}</AppShell.Section>
        <AppShell.Section bg="ul-dark-blue" h={75}>
          <Center>
            <Link href="https://dsri.org">
              <Image
                src="/static/dsri-white.svg"
                height="75"
                width="200"
                alt="UL Research Institutes Logo"
              />
            </Link>
          </Center>
        </AppShell.Section>
      </AppShell.Navbar>

      <AppShell.Main pos="relative" h={OPERATOR_APP_SHELL_MAIN_HEIGHT}>
        {children}
      </AppShell.Main>
    </AppShell>
  )
}
