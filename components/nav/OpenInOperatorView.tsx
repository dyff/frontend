/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import classes from '@/styles/OpenInOperatorView.module.css'
import { ActionIcon, Tooltip } from '@mantine/core'
import { IconEdit } from '@tabler/icons-react'
import { CSSProperties } from 'react'
import { useAuth } from '../auth/AuthProvider'

interface OpenInOperatorViewProps {
  href: string
  position?: any
  style?: CSSProperties
  label?: string
}

export default function OpenInOperatorView({
  href,
  style,
  label = 'Open in Operator View',
  position = 'relative'
}: OpenInOperatorViewProps) {
  const { isSignedIn, showOperatorControls } = useAuth()
  if (!isSignedIn || !showOperatorControls) return <></>
  return (
    <Tooltip label={label}>
      <ActionIcon
        style={{
          position: position,
          ...style
        }}
        className={classes.button}
        component="a"
        href={href}
      >
        <IconEdit />
      </ActionIcon>
    </Tooltip>
  )
}
