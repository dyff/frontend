/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { useAuth } from '@/components/auth/AuthProvider'
import { isValidPagePerms } from '@/lib/auth'
import { NavLink } from '@mantine/core'
import { usePathname } from 'next/navigation'

interface NavItemProps {
  route: string
  pageName: string
  label: string
  icon: JSX.Element
}

export default function NavItem({ route, pageName, label, icon }: NavItemProps) {
  const { sessionType, loading } = useAuth()
  const pathname = usePathname()
  const validPerms = isValidPagePerms(pathname, sessionType)
  const noDashboardSplit = pathname.replace('/dashboard', '').split('/')
  const currPage = pathname === '/dashboard' ? 'dashboard' : noDashboardSplit.at(1)

  if (loading) {
    return (
      <NavLink
        href={route}
        label={label}
        active={currPage === pageName}
        leftSection={icon}
        variant="light"
        color="ul-bright-blue"
        disabled={loading}
      />
    )
  }

  if (!validPerms) {
    return <></>
  }

  return (
    <NavLink
      href={route}
      label={label}
      active={currPage === pageName}
      leftSection={icon}
      variant="light"
      color="ul-bright-blue"
      disabled={loading}
    />
  )
}
