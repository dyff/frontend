/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Documentation, DocumentationEditRequest, Entity, EntityWithDocs } from '@/types/api'
import { UpdateDocsFn } from '@/types/entity'
import {
  Button,
  Center,
  Fieldset,
  Group,
  LoadingOverlay,
  Stack,
  Text,
  Textarea,
  Title,
  useMantineColorScheme
} from '@mantine/core'
import { useSetState } from '@mantine/hooks'
import { ChangeEvent, useState } from 'react'
import SafeMDEditor from '../utility/SafeMDEditor'
import SafeMDPreview from '../utility/SafeMDPreview'

interface DocumentationSectionProps<T extends EntityWithDocs<Entity>> {
  data: T | null
  updateDocs: UpdateDocsFn
}

const BLANK_DOCS = {
  title: '',
  summary: '',
  fullPage: ''
}

const toEditDocsSchema = (docs: Documentation) => {
  return {
    title: docs.title,
    summary: docs.summary,
    fullPage: docs.fullPage
  }
}

export default function DocumentationSection<T extends EntityWithDocs<Entity>>({
  data,
  updateDocs
}: DocumentationSectionProps<T>) {
  const { colorScheme } = useMantineColorScheme()
  const [editEnabled, setEditEnabled] = useState<boolean>(false)
  const [stagedDocs, setStagedDocs] = useSetState<DocumentationEditRequest>(
    data && data.docs ? toEditDocsSchema(data.docs) : BLANK_DOCS
  )
  const [updating, setUpdating] = useState<boolean>(false)

  /** Handler for beginning / cancelling editing */
  const handleToggleEdit = () => {
    // If cancelling, reset staged to original
    if (editEnabled) {
      if (data && data.docs) {
        setStagedDocs(toEditDocsSchema(data.docs))
      } else {
        setStagedDocs(BLANK_DOCS)
      }
    }
    setEditEnabled(!editEnabled)
  }

  const handleTitleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setStagedDocs({ title: e.target.value })
  }

  const handleSummaryChange = (value: string | undefined) => {
    setStagedDocs({ summary: value })
  }

  const handleFullPageChange = (value: string | undefined) => {
    setStagedDocs({ fullPage: value })
  }

  const handleSaveChanges = () => {
    if (!data) return
    setUpdating(true)
    updateDocs(data.id, stagedDocs, {
      onDone: () => {
        setUpdating(false)
      }
    })
  }

  return (
    <Center data-color-mode={colorScheme} pos="relative">
      <LoadingOverlay visible={updating} />
      <Stack w="100%">
        <Group gap="md">
          {!!editEnabled && (
            <Button
              variant="filled"
              color="ul-dark-blue"
              w="fit-content"
              onClick={handleSaveChanges}
            >
              Save
            </Button>
          )}

          <Button
            color="ul-bright-blue"
            variant="outline"
            w="fit-content"
            onClick={handleToggleEdit}
          >
            {editEnabled ? 'Cancel' : 'Edit'}
          </Button>
        </Group>
        {editEnabled ? (
          <>
            <Textarea
              label="Title"
              placeholder="Some title here!"
              value={stagedDocs.title || ''}
              onChange={handleTitleChange}
            />
            <Stack gap="xs">
              <Text size="sm">Summary</Text>
              <SafeMDEditor
                style={{ width: '100%' }}
                value={stagedDocs.summary || ''}
                onChange={(value) => {
                  handleSummaryChange(value)
                }}
                height="200px"
              />
            </Stack>
            <Stack gap="xs">
              <Text size="sm">Full Page</Text>
              <SafeMDEditor
                style={{ width: '100%' }}
                value={stagedDocs.fullPage || ''}
                onChange={(value) => {
                  handleFullPageChange(value)
                }}
                height="500px"
              />
            </Stack>
          </>
        ) : (
          <>
            <Fieldset legend="Title">
              {!data?.docs?.title ? (
                <Text c="dimmed">No title set</Text>
              ) : (
                <Title order={3}>{data.docs.title}</Title>
              )}
            </Fieldset>
            <Fieldset legend="Summary">
              {!data?.docs?.summary ? (
                <Text c="dimmed">No summary set</Text>
              ) : (
                <SafeMDPreview source={data?.docs?.summary} />
              )}
            </Fieldset>
            <Fieldset legend="Full Page">
              {!data?.docs?.fullPage ? (
                <Text c="dimmed">No full page set</Text>
              ) : (
                <SafeMDPreview source={data?.docs?.fullPage} />
              )}
            </Fieldset>
          </>
        )}
      </Stack>
    </Center>
  )
}
