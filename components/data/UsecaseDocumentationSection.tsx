/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  Documentation,
  DocumentationEditRequest,
  Entity,
  EntityWithDocs,
  UseCase
} from '@/types/api'
import { UpdateDocsFn } from '@/types/entity'
import {
  Button,
  Center,
  Fieldset,
  Group,
  LoadingOverlay,
  Stack,
  Text,
  Textarea,
  Title,
  useMantineColorScheme
} from '@mantine/core'
import { useSetState } from '@mantine/hooks'
import { ChangeEvent, useState } from 'react'
import SafeMDEditor from '../utility/SafeMDEditor'
import SafeMDPreview from '../utility/SafeMDPreview'

interface UseCaseDocumentationSectionProps {
  data: UseCase
  updateDocs: UpdateDocsFn
}

const BLANK_DOCS = {
  title: '',
  summary: '',
  fullPage: ''
}

const toEditDocsSchema = (docs: Documentation) => {
  return {
    title: docs.title,
    summary: docs.summary,
    fullPage: docs.fullPage
  }
}

// UseCase is temporarily the only entity that natively has .documentation as a prop from API
export default function UseCaseDocumentationSection({
  data,
  updateDocs
}: UseCaseDocumentationSectionProps) {
  const { colorScheme } = useMantineColorScheme()
  const [editEnabled, setEditEnabled] = useState<boolean>(false)
  const [stagedDocs, setStagedDocs] = useSetState<DocumentationEditRequest>(
    data && data.documentation ? toEditDocsSchema(data.documentation) : BLANK_DOCS
  )
  const [updating, setUpdating] = useState<boolean>(false)

  /** Handler for beginning / cancelling editing */
  const handleToggleEdit = () => {
    // If cancelling, reset staged to original
    if (editEnabled) {
      if (data && data.documentation) {
        setStagedDocs(toEditDocsSchema(data.documentation))
      } else {
        setStagedDocs(BLANK_DOCS)
      }
    }
    setEditEnabled(!editEnabled)
  }

  const handleTitleChange = (e: ChangeEvent<HTMLTextAreaElement>) => {
    setStagedDocs({ title: e.target.value })
  }

  const handleSummaryChange = (value: string | undefined) => {
    setStagedDocs({ summary: value })
  }

  const handleFullPageChange = (value: string | undefined) => {
    setStagedDocs({ fullPage: value })
  }

  const handleSaveChanges = () => {
    if (!data) return
    setUpdating(true)
    updateDocs(data.id, stagedDocs, {
      onDone: () => {
        setUpdating(false)
      }
    })
  }

  return (
    <Center data-color-mode={colorScheme} pos="relative">
      <LoadingOverlay visible={updating} />
      <Stack w="100%">
        <Group gap="md">
          {!!editEnabled && (
            <Button
              variant="filled"
              color="ul-dark-blue"
              w="fit-content"
              onClick={handleSaveChanges}
            >
              Save
            </Button>
          )}

          <Button
            color="ul-bright-blue"
            variant="outline"
            w="fit-content"
            onClick={handleToggleEdit}
          >
            {editEnabled ? 'Cancel' : 'Edit'}
          </Button>
        </Group>
        {editEnabled ? (
          <>
            <Textarea
              label="Title"
              placeholder="Some title here!"
              value={stagedDocs.title || ''}
              onChange={handleTitleChange}
            />
            <Stack gap="xs">
              <Text size="sm">Summary</Text>
              <SafeMDEditor
                style={{ width: '100%' }}
                value={stagedDocs.summary || ''}
                onChange={(value) => {
                  handleSummaryChange(value)
                }}
                height="200px"
              />
            </Stack>
            <Stack gap="xs">
              <Text size="sm">Full Page</Text>
              <SafeMDEditor
                style={{ width: '100%' }}
                value={stagedDocs.fullPage || ''}
                onChange={(value) => {
                  handleFullPageChange(value)
                }}
                height="500px"
              />
            </Stack>
          </>
        ) : (
          <>
            <Fieldset legend="Title">
              {!data?.documentation?.title ? (
                <Text c="dimmed">No title set</Text>
              ) : (
                <Title order={3}>{data.documentation.title}</Title>
              )}
            </Fieldset>
            <Fieldset legend="Summary">
              {!data?.documentation?.summary ? (
                <Text c="dimmed">No summary set</Text>
              ) : (
                <SafeMDPreview source={data?.documentation?.summary} />
              )}
            </Fieldset>
            <Fieldset legend="Full Page">
              {!data?.documentation?.fullPage ? (
                <Text c="dimmed">No full page set</Text>
              ) : (
                <SafeMDPreview source={data?.documentation?.fullPage} />
              )}
            </Fieldset>
          </>
        )}
      </Stack>
    </Center>
  )
}
