/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  DYFF_ACCESS_STR,
  DYFF_USECASE_INDEX_KEY,
  DYFF_USECASE_INDEX_VALUE,
  TOPIC_LABEL_KEY,
  VERSION_LABEL_KEY
} from '@/lib/constants/labels'
import { DYFF_RANGEMAX_SCORE_LABEL, DYFF_RANGEMIN_SCORE_LABEL } from '@/lib/constants/labels'
import { useEntityData } from '@/lib/hooks/entity'
import { Entity, UseCase } from '@/types/api'
import { StagedLabels, UpdateLabelsFn } from '@/types/entity'
import {
  ActionIcon,
  Button,
  Fieldset,
  Group,
  LoadingOverlay,
  Menu,
  Modal,
  Stack,
  Table,
  Text,
  TextInput,
  Tooltip
} from '@mantine/core'
import { useDisclosure, useSetState } from '@mantine/hooks'
import { IconPlus, IconSelector, IconX } from '@tabler/icons-react'
import { useEffect, useState } from 'react'

interface LabelsSectionProps {
  data: Entity
  updateLabels: UpdateLabelsFn
}

/** Label Modal Component
 * Open state declared outside of component
 */
export default function LabelsSection({ data, updateLabels }: LabelsSectionProps) {
  const [isOpen, { open: openModal, close: closeModal }] = useDisclosure()
  const [stagedLabels, _setStagedLabels] = useState<StagedLabels>({})
  const [newStagedLabel, setNewStagedLabel] = useSetState<Record<string, string>>({
    key: '',
    value: ''
  })
  const [loading, setLoading] = useState(false)

  const setStagedLabels = (statePartial: Partial<StagedLabels>) => {
    const newState = { ...stagedLabels, ...statePartial }
    _setStagedLabels(newState)
  }

  const handleOpenModal = (): void => {
    if (data.labels) {
      setStagedLabels(data.labels)
    }
    openModal()
  }

  const handleCloseModal = (): void => {
    setStagedLabels({})
    setNewStagedLabel({
      key: '',
      value: ''
    })
    closeModal()
  }

  const handleAddLabel = (): void => {
    setStagedLabels({ [newStagedLabel.key]: newStagedLabel.value })
    setNewStagedLabel({ key: '', value: '' })
  }

  const handleDeleteLabel = (key: string): void => {
    // Set to null if in old labels, otherwise remove key
    if (data.labels && Object.keys(data.labels).includes(key)) {
      setStagedLabels({ [key]: null })
    } else {
      const newStagedLabels = { ...stagedLabels }
      delete newStagedLabels[key]
      _setStagedLabels(newStagedLabels)
    }
  }

  const handleEditLabelValue = (key: string, value: string) => {
    setStagedLabels({ [key]: value })
  }

  const handleSaveStagedLabels = async (): Promise<void> => {
    setLoading(true)
    if (!data) return
    updateLabels(data.id, stagedLabels, {
      onSuccess: () => {
        closeModal()
      },
      onDone: () => {
        setLoading(false)
      }
    })
  }

  // Fetch use cases if method entity for use case add
  const {
    data: usecases,
    refresh: refreshUsecases,
    loading: usecasesLoading
  } = useEntityData<UseCase>('usecases', {
    doInitalFetch: false
  })

  // Set staged labels upon open
  useEffect(() => {
    if (isOpen) {
      if (data.labels) {
        setStagedLabels(data.labels)
      }

      if (data.kind === 'Method') {
        refreshUsecases()
      }
    }
  }, [isOpen])

  return (
    <>
      <Fieldset
        legend={
          <Group gap="xs">
            Labels
            <Button variant="light" size="xs" px="xs" onClick={handleOpenModal}>
              Edit
            </Button>
          </Group>
        }
        h="100%"
      >
        {!data.labels || (!!data.labels && Object.keys(data.labels).length === 0) ? (
          <Text c="dimmed">No data</Text>
        ) : (
          <Stack>
            {Object.entries(data.labels).map(([key, value]) => (
              <Group gap="xs" key={key}>
                <Text fw={700} style={{ wordBreak: 'break-all' }}>
                  {key}
                </Text>
                <Text>{value}</Text>
              </Group>
            ))}
          </Stack>
        )}
      </Fieldset>
      <Modal opened={isOpen} onClose={handleCloseModal} title="Edit Labels" centered size="xl">
        <Modal.Body pos="relative">
          <LoadingOverlay visible={loading} />
          <Table highlightOnHover striped>
            <Table.Thead>
              <Table.Tr>
                <Table.Th>Key</Table.Th>
                <Table.Th>Value</Table.Th>
                <Table.Th w="25px"></Table.Th>
              </Table.Tr>
            </Table.Thead>
            <Table.Tbody>
              {Object.entries(stagedLabels).map(([key, value]) => {
                const nulled = value === null
                let style = 'none'
                if (nulled) {
                  style = 'line-through'
                  return null
                }
                return (
                  <Table.Tr key={key}>
                    <Table.Td>
                      <Text>{key}</Text>
                    </Table.Td>
                    <Table.Td>
                      <TextInput
                        value={value}
                        onChange={(e) => {
                          handleEditLabelValue(key, e.currentTarget.value)
                        }}
                      />
                    </Table.Td>
                    <Table.Td>
                      <Tooltip label="Remove label" position="right" withArrow>
                        <ActionIcon
                          color="ul-bright-blue"
                          onClick={() => {
                            handleDeleteLabel(key)
                          }}
                        >
                          <IconX />
                        </ActionIcon>
                      </Tooltip>
                    </Table.Td>
                  </Table.Tr>
                )
              })}
              <Table.Tr bg="ul-bright-blue.1">
                <Table.Td>
                  <TextInput
                    placeholder="New Label Key"
                    value={newStagedLabel.key}
                    onChange={(e) => {
                      setNewStagedLabel({ key: e.currentTarget.value })
                    }}
                  />
                </Table.Td>
                <Table.Td>
                  <TextInput
                    placeholder="New Label Value"
                    value={newStagedLabel.value}
                    onChange={(e) => {
                      setNewStagedLabel({ value: e.currentTarget.value })
                    }}
                  />
                </Table.Td>
                <Table.Td>
                  <Tooltip label="Add label" position="right" withArrow>
                    <ActionIcon
                      color="ul-bright-blue"
                      onClick={handleAddLabel}
                      disabled={newStagedLabel.key === ''}
                    >
                      <IconPlus />
                    </ActionIcon>
                  </Tooltip>
                </Table.Td>
              </Table.Tr>
            </Table.Tbody>
          </Table>
          <Group mt="md" wrap="nowrap">
            <Group wrap="wrap" gap="xs">
              <Menu position="bottom-start">
                <Menu.Target>
                  <Button size="xs" variant="outline">
                    Publish <IconSelector size={15} style={{ marginLeft: '5px' }} />
                  </Button>
                </Menu.Target>
                <Menu.Dropdown>
                  {!(
                    Object.keys(stagedLabels).includes(DYFF_ACCESS_STR) &&
                    stagedLabels[DYFF_ACCESS_STR] !== null &&
                    stagedLabels[DYFF_ACCESS_STR] === 'public'
                  ) && (
                    <Menu.Item
                      variant="outline"
                      onClick={() => {
                        setStagedLabels({ [DYFF_ACCESS_STR]: 'public' })
                      }}
                    >
                      Public
                    </Menu.Item>
                  )}
                  {!(
                    Object.keys(stagedLabels).includes(DYFF_ACCESS_STR) &&
                    stagedLabels[DYFF_ACCESS_STR] !== null &&
                    stagedLabels[DYFF_ACCESS_STR] === 'internal'
                  ) && (
                    <Menu.Item
                      variant="outline"
                      onClick={() => {
                        setStagedLabels({ [DYFF_ACCESS_STR]: 'internal' })
                      }}
                    >
                      Internal
                    </Menu.Item>
                  )}
                  {!Object.keys(stagedLabels).includes(TOPIC_LABEL_KEY) &&
                    stagedLabels[TOPIC_LABEL_KEY] !== null && (
                      <Menu.Item
                        variant="outline"
                        onClick={() => {
                          setStagedLabels({ [TOPIC_LABEL_KEY]: '' })
                        }}
                      >
                        Topic
                      </Menu.Item>
                    )}
                  {!Object.keys(stagedLabels).includes(VERSION_LABEL_KEY) &&
                    stagedLabels[VERSION_LABEL_KEY] !== null && (
                      <Menu.Item
                        variant="outline"
                        onClick={() => {
                          setStagedLabels({ [VERSION_LABEL_KEY]: '1.0.0' })
                        }}
                      >
                        Version
                      </Menu.Item>
                    )}
                </Menu.Dropdown>
              </Menu>
              {data.kind === 'Method' && (
                <>
                  <Menu position="bottom-start">
                    <Menu.Target>
                      <Button size="xs" variant="outline">
                        Scores <IconSelector size={15} style={{ marginLeft: '5px' }} />
                      </Button>
                    </Menu.Target>
                    <Menu.Dropdown>
                      <Menu.Item
                        variant="outline"
                        onClick={() => {
                          setNewStagedLabel({ key: `${DYFF_RANGEMIN_SCORE_LABEL}` })
                        }}
                      >
                        Min
                      </Menu.Item>
                      <Menu.Item
                        variant="outline"
                        onClick={() => {
                          setNewStagedLabel({ key: `${DYFF_RANGEMAX_SCORE_LABEL}` })
                        }}
                      >
                        Max
                      </Menu.Item>
                    </Menu.Dropdown>
                  </Menu>
                  <Menu position="bottom-start">
                    <Menu.Target>
                      <Button size="xs" variant="outline">
                        Use Case <IconSelector size={15} style={{ marginLeft: '5px' }} />
                      </Button>
                    </Menu.Target>
                    <Menu.Dropdown>
                      {usecases?.map((u, idx) => (
                        <Menu.Item
                          key={idx}
                          variant="outline"
                          onClick={() => {
                            setStagedLabels({
                              [`${DYFF_USECASE_INDEX_KEY}${u.id}`]: DYFF_USECASE_INDEX_VALUE
                            })
                          }}
                        >
                          {u.documentation?.title ?? u.id}
                        </Menu.Item>
                      ))}
                    </Menu.Dropdown>
                  </Menu>
                </>
              )}
            </Group>
            <Group ml="auto" align="end" gap="xs">
              <Button variant="outline" size="xs" onClick={handleCloseModal}>
                Cancel
              </Button>
              <Button size="xs" onClick={handleSaveStagedLabels}>
                Save
              </Button>
            </Group>
          </Group>
        </Modal.Body>
      </Modal>
    </>
  )
}
