/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import PageLoading from '@/components/utility/PageLoading'
import { isValidPagePerms } from '@/lib/auth'
import { Center } from '@mantine/core'
import { usePathname, useRouter } from 'next/navigation'
import { useEffect, useState } from 'react'
import { useAuth } from './AuthProvider'

export default function ProtectedRoute({ children }: { children: any }): JSX.Element {
  const router = useRouter()
  const { sessionType, loading } = useAuth()
  const [permitted, setPermitted] = useState(false)
  const pathname = usePathname()

  useEffect(() => {
    if (loading) {
      setPermitted(false)
      return
    }

    const validPerms = isValidPagePerms(pathname, sessionType)
    setPermitted(validPerms)
    if (!validPerms) {
      // notifications.show({ message: `You aren't allowed to view this page`, color: 'red' })
      router.push(`/auth/error?redirect=${encodeURIComponent(pathname)}`)
    }
  }, [loading, sessionType, pathname])

  if (!permitted || loading) {
    return (
      <Center h="100%" w="100%">
        <PageLoading />
      </Center>
    )
  }

  return <>{children}</>
}
