/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { verifyAPIKey } from '@/lib/auth'
import { AuthContextType, DyffApiJWT, SessionType, SetTokenType } from '@/types/auth'
import { useComputedColorScheme, useMantineColorScheme } from '@mantine/core'
import { useLocalStorage } from '@mantine/hooks'
import { showNotification } from '@mantine/notifications'
import { useRouter } from 'next/navigation'
import { createContext, useContext, useEffect, useMemo, useState } from 'react'
import { decodeToken, isExpired } from 'react-jwt'

const AuthContext = createContext({} as AuthContextType)

const AuthProvider = ({ children }: { children: any }) => {
  // State to hold the authentication token
  const router = useRouter()
  const [token, setToken_] = useLocalStorage({
    key: 'token',
    defaultValue: '',
    getInitialValueInEffect: false
  })
  const [_showOperatorControls, _setShowOperatorControls] = useLocalStorage({
    key: 'showOperatorControls',
    defaultValue: 'false',
    getInitialValueInEffect: false
  })
  const [loading, setLoading] = useState<boolean>(true)
  const [tokenExpired, setTokenExpired] = useState<boolean>(true)
  const [tokenExpiresAt, setTokenExpiresAt] = useState<Date | null>(null)
  const [tokenIssuedAt, setTokenIssuedAt] = useState<Date | null>(null)
  const [tokenValid, setTokenValid] = useState<boolean>(false)
  const [decodedToken, setDecodedToken] = useState<DyffApiJWT>(null)
  const [sessionType, setSessionType] = useState<SessionType>('guest')
  const [loginRedirect, setLoginRedirect] = useState<string | undefined>()
  const isSignedIn = sessionType === 'user'

  /**
   * Sets token
   * @param newToken New token
   */
  const setToken: SetTokenType = (newToken, redirect) => {
    setLoginRedirect(redirect)
    setToken_(newToken)
  }

  /**
   * Signs out (releases token)
   */
  const releaseToken = () => {
    setToken_('')
    setLoginRedirect(undefined)
  }

  /**
   * Evaluate token on change
   * Token is null if not loaded from local storage
   */
  useEffect(() => {
    if (token !== null) {
      evaluateToken(token)
    }
  }, [token])

  /**
   * Helper to set invalid state and push message
   * @param msg
   */
  const handleInvalidToken = (msg = 'Invalid API Key') => {
    setTokenValid(false)
    setSessionType('guest')
    setLoading(false)
    showNotification({ message: msg })
    router.push(
      `/auth/signin${loginRedirect ? `?redirect=${encodeURIComponent(loginRedirect)}` : ''}`
    )
  }

  /**
   * Evaluates and validates additional state regarding JWT token
   * @param newToken New JWT Token
   */
  const evaluateToken = async (newToken: string): Promise<void> => {
    setLoading(true)
    // If blank new token (but still loaded), reset state
    if (newToken === '') {
      setDecodedToken(null)
      setTokenValid(false)
      setSessionType('guest')
      setLoading(false)
      return
    }

    // Verify signature if login redirect set (login flow active)
    if (loginRedirect) {
      try {
        const res = await verifyAPIKey(newToken)
        if (res.status !== 200) {
          handleInvalidToken()
          return
        }
      } catch (e) {
        handleInvalidToken()
        return
      }
    }

    // If invalid parse, kick back to login
    const decoded = decodeToken<DyffApiJWT>(newToken) // Returns null if invalid
    if (decoded === null) {
      handleInvalidToken('Invalid API Key format!')
      return
    }

    // If not null, valid decoded token
    setDecodedToken(decoded)
    setTokenValid(true)
    setSessionType('user')

    const expired = isExpired(newToken)
    setTokenExpired(expired)
    const expiresAt = new Date(decoded.exp * 1000)
    setTokenExpiresAt(expiresAt)
    const issuedAt = new Date(decoded.iat * 1000)
    setTokenIssuedAt(issuedAt)

    if (expired) {
      handleInvalidToken('Your API Key expired, please re-log in')
      return
    }

    // Success
    setLoading(false)
    if (loginRedirect) {
      router.push(decodeURIComponent(loginRedirect))
      setLoginRedirect(undefined)
    }
  }

  /** Handle parsing of showOperatorControls */
  const showOperatorControls = useMemo(() => {
    try {
      return JSON.parse(_showOperatorControls) === true
    } catch (_) {
      return false
    }
  }, [_showOperatorControls])

  const setShowOperatorControls = (newValue: boolean) => {
    const newBool = JSON.stringify(newValue)
    _setShowOperatorControls(newBool)
  }

  /**
   * Memoized value of the authentication context
   */
  const contextValue = useMemo(
    () => ({
      token,
      decodedToken,
      tokenValid,
      tokenExpired,
      sessionType,
      loading,
      setToken,
      releaseToken,
      isSignedIn: isSignedIn,
      tokenExpiresAt,
      tokenIssuedAt,
      showOperatorControls,
      setShowOperatorControls
    }),
    [token, decodedToken, tokenExpired, tokenValid, sessionType, loading, showOperatorControls]
  )

  const { setColorScheme } = useMantineColorScheme()
  const computedColorScheme = useComputedColorScheme('light', {
    getInitialValueInEffect: true
  })

  // Temp override color scheme to white for ULRI Symposium
  useEffect(() => {
    if (computedColorScheme === 'dark') {
      setColorScheme('light')
    }
  }, [])

  return <AuthContext.Provider value={contextValue}>{children}</AuthContext.Provider>
}

export const useAuth = () => {
  return useContext(AuthContext)
}

export default AuthProvider
