/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Button } from '@mantine/core'
import { useRouter } from 'next/navigation'

export default function AuthButton() {
  const router = useRouter()
  return (
    <>
      <Button
        onClick={() => {
          router.push('/auth/signin')
        }}
      >
        Sign in
      </Button>
    </>
  )
}
