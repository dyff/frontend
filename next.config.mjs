/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import bundleAnalyzer from '@next/bundle-analyzer'

const withBundleAnalyzer = bundleAnalyzer({
  enabled: process.env.ANALYZE === 'true'
})

export default withBundleAnalyzer({
  reactStrictMode: false,
  eslint: {
    ignoreDuringBuilds: true
  },
  experimental: {
    optimizePackageImports: ['@mantine/core', '@mantine/hooks']
  },
  output: 'standalone'
  // webpack: (config, { isServer }) => {
  //   // Custom webpack configuration
  //   config.resolve.alias['@dsnp/parquetjs'] =
  //     'node_modules/@dsnp/parquetjs/dist/browser/parquetjs.esm'
  //   return config
  // }
})
