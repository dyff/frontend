/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import userEvent from '@testing-library/user-event'

export * from '@testing-library/react'
export { render } from './render'
export { userEvent }
