/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { test, expect } from '@playwright/test'
import 'dotenv/config'

test('homepage redirects to login', async ({ page }) => {
  await page.goto('/')
  await expect(page).toHaveURL('/auth/signin', { timeout: 10000 })
})

test('simulated token to allow dashboard view', async ({ page }) => {
  // Simulate login with env var
  const token = process.env.DYFF_API_KEY
  if (!token) {
    throw new Error('No Env Var token API Key detected')
  }
  await page.goto(`/auth/callback?token=${token}`)
  // Should load dashboard if set
  await page.goto('/dashboard')
  await expect(page).toHaveURL('/dashboard')
})
