/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { NextResponse } from 'next/server'

type ContextType = {
  params: {
    slug: string[]
  }
}

export const dynamic = 'force-dynamic'

export async function GET(request: Request, context: ContextType) {
  const forwardedURL = assembleRewriteURL(request, context)
  const headers = new Headers(request.headers)
  headers.set('accept-encoding', 'gzip, deflate, br')
  try {
    const res = await fetch(forwardedURL, {
      method: 'GET',
      headers: headers,
      cache: 'no-store'
    })

    if (res.ok && res.body) {
      const jsonRes = await res.json() // In case large payload, await data parse
      return NextResponse.json(jsonRes, { ...res })
    }

    return res
  } catch (e) {
    const err = e as Error
    return NextResponse.json({ error: err.message, forwardedURL: forwardedURL }, { status: 500 })
  }
}

/**
 * Rewrites an API request URL to the rewritten URL using the runtime env var API URL.
 * In expected fetch format
 * @param request The Route Request object
 * @param context Context containing the slug params
 */
const assembleRewriteURL = (request: Request, context: ContextType): string => {
  // const slug = context.params.slug
  const pathnames = context.params.slug.join('/')
  const VIEWS_URL = process.env.DYFF_API_VIEWS_URL
  const searchParams = new URL(request.url).searchParams
  const pathedURL = `${VIEWS_URL}/${pathnames}`
  const fullURL = searchParams.size ? `${pathedURL}?${searchParams.toString()}` : pathedURL
  return fullURL
}
