/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import InfoChip from '@/components/display/InfoChip'
import MethodScoreCard from '@/components/display/MethodScoreCard'
import NoDataDisplay from '@/components/display/NoData'
import { ALL_USECASES_KEY, UseCaseFilter } from '@/components/display/UseCaseFilter'
import UseCaseScoreGroupCard from '@/components/display/UseCaseScoreGroupCard'
import StackMotion from '@/components/layout/StackMotion'
import OpenInOperatorView from '@/components/nav/OpenInOperatorView'
import SafeMDPreview from '@/components/utility/SafeMDPreview'
import SectionLoading from '@/components/utility/SectionLoading'
import { MIDDLE_LAYOUT_MAX_W, PAGE_DEFS } from '@/lib/constants/constants'
import { DYFF_USECASE_INDEX_KEY } from '@/lib/constants/labels'
import { HOME_SYSTEM_SINGLE_NO_RESULTS_TEXT } from '@/lib/constants/copy'
import {
  findDocs,
  formatDateStr,
  getUseCaseLabelKeyById,
  makeEntityDocs,
  useIsMobile
} from '@/lib/helpers'
import { useView } from '@/lib/hooks/view'
import {
  InferenceService,
  Method,
  MethodWithDocs,
  MethodWithSafetyCases,
  Model,
  UseCaseWithMethodsWithSafetyCases
} from '@/types/api'
import { SystemSingleView } from '@/types/views'
import {
  Card,
  Flex,
  Grid,
  Group,
  Stack,
  Text,
  Title,
  Tooltip,
  useMantineTheme
} from '@mantine/core'
import { useDebouncedState } from '@mantine/hooks'
import { showNotification } from '@mantine/notifications'
import { AnimatePresence, motion } from 'motion/react'
import Link from 'next/link'
import { useSearchParams } from 'next/navigation'
import { useEffect, useMemo, useState } from 'react'

export default function Page({ params }: { params: { systemID: string } }) {
  const theme = useMantineTheme()
  const isMobile = useIsMobile()
  const searchParams = useSearchParams()

  const [useCaseSearchText, setUseCaseSearchText] = useDebouncedState('', 100)
  const [selectedUseCase, setSelectedUseCase] = useState(
    searchParams.get('usecase') || ALL_USECASES_KEY
  )

  const { data, loading } = useView<SystemSingleView>({
    route: `/views/systems/${params.systemID}`,
    onError: (msg) => {
      showNotification({ message: `Error: ${msg}` })
    }
  })

  const model = data?.model
  const services = data?.services
  const methodsArr = data?.methods
  const usecases = data?.usecases
  const safetycases = data?.safetycases
  const docs = data?.documentation
  const scores = data?.scores || []

  // Handle inf svc or model polymorphism selection
  const systemType = !model ? 'service' : 'model'
  const system: Model | InferenceService | undefined = !!model
    ? model
    : !!services && services.length
      ? services[0]
      : undefined

  const systemDocs = findDocs(system, docs)

  useEffect(() => {
    document.title = systemDocs?.title
      ? `${systemDocs.title} | Dyff`
      : PAGE_DEFS.EXPLORE_SYSTEMS.title
  }, [systemDocs])

  const methods: MethodWithDocs[] = useMemo(() => {
    return methodsArr?.map((m) => makeEntityDocs<Method>(m, docs) as MethodWithDocs) ?? []
  }, [methodsArr])

  // If all selected, return all, else return filtered id
  const filteredUsecases = useMemo(() => {
    return usecases?.filter((u) => selectedUseCase === 'all' || u.id === selectedUseCase) ?? []
  }, [usecases, selectedUseCase])

  const { usecasesWithMethods, orphanMethods } = useMemo(() => {
    // Aggregate usecase -> method(s)
    const orphanMethods = new Array<MethodWithSafetyCases>()
    const newUsecases: UseCaseWithMethodsWithSafetyCases[] =
      filteredUsecases?.map((u) => {
        return {
          ...u,
          methods: []
        }
      }) ?? []

    methods?.forEach((m) => {
      // Aggregate many safety cases -> one method
      const thisMethod: MethodWithSafetyCases = {
        ...m,
        safetyCases: safetycases?.filter((sc) => sc.method.id === m.id) ?? []
      }
      if (!m.labels || !Object.keys(m.labels).length) {
        orphanMethods.push(thisMethod)
        return
      }

      // If some labels but no use case labels
      if (!Object.keys(m.labels).find((key) => key.includes(DYFF_USECASE_INDEX_KEY))) {
        orphanMethods.push(thisMethod)
      }

      // Aggregate many methods -> one use case
      if (newUsecases) {
        newUsecases.forEach((u, idx, arr) => {
          const thisUseCase = arr[idx]
          // If usecase label exists, add to bucket
          if (Object.keys(m.labels!).find((key) => key === getUseCaseLabelKeyById(u.id))) {
            thisUseCase.methods.push(thisMethod)
          }
        })
      }
    })

    return {
      usecasesWithMethods: newUsecases,
      orphanMethods: orphanMethods
    }
  }, [methods, filteredUsecases])

  return (
    <ProtectedRoute>
      <Stack w="100%" p={0} m={0} gap={0} justify="start" align="center">
        <Card m={0} p={0} w="100%" bg={theme.colors['ul-light-blue'][0]} radius={0}>
          <Stack
            justify="start"
            gap="sm"
            pos="relative"
            w="100%"
            maw={MIDDLE_LAYOUT_MAX_W}
            mx="auto"
            p="md"
          >
            <Grid>
              <Grid.Col
                span={{
                  base: 12,
                  sm: 8
                }}
              >
                <Stack justify="start" align="start" gap={0} pos="relative">
                  {loading ? (
                    <SectionLoading rows={3} h="30px" />
                  ) : !system ? (
                    <NoDataDisplay />
                  ) : (
                    <>
                      <Group gap="sm" align="center">
                        <Title order={2}>{systemDocs?.title || system.name}</Title>
                        <OpenInOperatorView
                          label="Open System in Operator View"
                          position="relative"
                          href={`${systemType === 'service' ? PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route : PAGE_DEFS.OPERATOR_MODELS.route}/${params.systemID}`}
                        />
                      </Group>
                      <Tooltip
                        openDelay={500}
                        label={system.kind || 'System'}
                        position="bottom-start"
                      >
                        <Text c="dimmed" mb="xs">
                          System
                        </Text>
                      </Tooltip>
                      {!systemDocs?.summary ? (
                        <Text c="dimmed">Description Unavailable</Text>
                      ) : (
                        <SafeMDPreview source={systemDocs.summary} />
                      )}
                    </>
                  )}
                </Stack>
              </Grid.Col>
              <Grid.Col
                span={{
                  base: 12,
                  sm: 4
                }}
              >
                <Stack pos="relative">
                  {loading ? (
                    <SectionLoading h="56px" rows={2} />
                  ) : !system ? (
                    <NoDataDisplay />
                  ) : (
                    <>
                      {systemType === 'model' && model && model.source && (
                        <>
                          {!!model.source.huggingFaceHub && (
                            <InfoChip
                              label="Hugging Face Repo"
                              content={
                                <Link
                                  style={{ color: 'ul-dark-blue' }}
                                  target="_blank"
                                  href={`https://huggingface.co/${model.source.huggingFaceHub.repoID}`}
                                >
                                  {model.source.huggingFaceHub.repoID}
                                </Link>
                              }
                            />
                          )}
                          {!!model.source.gitLFS && (
                            <InfoChip
                              label="Git LFS"
                              content={
                                <Link
                                  style={{ color: 'ul-dark-blue' }}
                                  target="_blank"
                                  href={model.source.gitLFS.url}
                                >
                                  Git LFS Resource
                                </Link>
                              }
                            />
                          )}
                        </>
                      )}
                      <InfoChip
                        label="Date Added"
                        content={formatDateStr(system.creationTime, true)}
                      />
                    </>
                  )}
                </Stack>
              </Grid.Col>
            </Grid>
          </Stack>
        </Card>
        <Flex
          gap="sm"
          justify="center"
          align="center"
          direction={isMobile ? 'column' : 'row'}
          mx="auto"
        >
          <Title c="ul-bright-blue" style={{ textAlign: 'center' }}>
            Test Results
          </Title>
        </Flex>
        <StackMotion
          w="100%"
          maw={MIDDLE_LAYOUT_MAX_W}
          mx="auto"
          p="sm"
          gap="sm"
          justify="flex-start"
          align="start"
          layout="position"
        >
          {loading ? (
            <SectionLoading rows={5} h="150px" />
          ) : !usecasesWithMethods || !usecasesWithMethods.length ? (
            <Text mx="auto" c="dimmed">
              {HOME_SYSTEM_SINGLE_NO_RESULTS_TEXT}
            </Text>
          ) : (
            <>
              <UseCaseFilter
                useCaseSearchText={useCaseSearchText}
                setUseCaseSearchText={setUseCaseSearchText}
                selectedUseCase={selectedUseCase}
                setSelectedUseCase={setSelectedUseCase}
                loading={loading}
                usecases={usecases}
              />
              <AnimatePresence mode="popLayout">
                {usecasesWithMethods.map((uc, idx) => (
                  <motion.div
                    style={{ width: '100%' }}
                    key={uc.id}
                    layout
                    initial={{ opacity: 0, height: 0 }}
                    animate={{ opacity: 1, height: 'auto' }}
                    exit={{ opacity: 0, height: 0 }}
                  >
                    <UseCaseScoreGroupCard key={idx} scores={scores} usecase={uc} />
                  </motion.div>
                ))}
                {selectedUseCase === 'all' &&
                  orphanMethods.map((m, idx) => (
                    <motion.div
                      style={{ width: '100%' }}
                      key={m.id}
                      layout
                      initial={{ opacity: 0, height: 0 }}
                      animate={{ opacity: 1, height: 'auto' }}
                      exit={{ opacity: 0, height: 0 }}
                    >
                      <MethodScoreCard key={idx} scores={scores} method={m} />
                    </motion.div>
                  ))}
              </AnimatePresence>
            </>
          )}
        </StackMotion>
      </Stack>
    </ProtectedRoute>
  )
}
