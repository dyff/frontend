/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import SystemListCard from '@/components/display/SystemListCard'
import StackMotion from '@/components/layout/StackMotion'
import OpenInOperatorView from '@/components/nav/OpenInOperatorView'
import SectionLoading from '@/components/utility/SectionLoading'
import { MIDDLE_LAYOUT_MAX_W, PAGE_DEFS } from '@/lib/constants/constants'
import {
  HOME_SYSTEMS_NO_SYSTEMS_TEXT,
  MAKE_SYSTEMS_SUB_TITLE,
  SYSTEMS_TITLE
} from '@/lib/constants/copy'
import { useIsMobile } from '@/lib/helpers'
import { useView } from '@/lib/hooks/view'
import { ModelWithDocsSvcs } from '@/types/api'
import { SystemListView } from '@/types/views'
import { Card, Group, Stack, Text, TextInput, Title, useMantineTheme } from '@mantine/core'
import { useDebouncedState, useDocumentTitle } from '@mantine/hooks'
import { showNotification } from '@mantine/notifications'
import { IconSearch } from '@tabler/icons-react'
import { AnimatePresence, motion } from 'motion/react'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.EXPLORE_SYSTEMS.title)
  const theme = useMantineTheme()
  const isMobile = useIsMobile()
  const [searchText, setSearchText] = useDebouncedState('', 100)

  const { data, loading } = useView<SystemListView>({
    route: `/views/systems`,
    onError: (msg) => {
      showNotification({ message: `Error: ${msg}` })
    },
    onSuccess: (_) => {
      setDelayedLoad(true)
    }
  })

  const services = data?.services
  const models = data?.models
  const docs = data?.documentation

  const [delayedLoading, setDelayedLoad] = useDebouncedState(true, 500)

  const modelWithDocsSvcs: ModelWithDocsSvcs[] = useMemo(() => {
    if (!models) return []
    return models?.map((model) => {
      const theseDocs = docs?.find((doc) => doc.entity && doc.entity === model.id)
      const theseServices = services?.filter((svc) => svc.model?.id === model.id) || []
      return {
        ...model,
        docs: theseDocs,
        services: theseServices
      }
    })
  }, [data])

  const filteredModels = useMemo(() => {
    if (!searchText) return modelWithDocsSvcs
    return modelWithDocsSvcs.filter(
      (model) =>
        model.name.toLowerCase().includes(searchText.toLowerCase()) ||
        model.docs?.title?.toLowerCase().includes(searchText.toLowerCase())
    )
  }, [modelWithDocsSvcs, searchText])

  return (
    <ProtectedRoute>
      <Stack w="100%" p={0} m={0} gap={0} justify="start" align="center">
        <Card m={0} p={0} w="100%" bg={theme.colors['ul-light-blue'][0]} radius={0}>
          <Stack
            justify="start"
            gap="sm"
            pos="relative"
            w="100%"
            maw={MIDDLE_LAYOUT_MAX_W}
            mx="auto"
            p="md"
          >
            <Group>
              <Title order={2}>{SYSTEMS_TITLE}</Title>
              <OpenInOperatorView
                label="Open Systems in Operator View"
                position="relative"
                href={PAGE_DEFS.OPERATOR_MODELS.route}
              />
            </Group>
            {MAKE_SYSTEMS_SUB_TITLE(`/tests`)}
            <TextInput
              leftSectionPointerEvents="none"
              leftSection={<IconSearch />}
              placeholder="Search"
              w={isMobile ? '100%' : '300px'}
              onChange={(e) => setSearchText(e.target.value)}
            />
          </Stack>
        </Card>
        <StackMotion w="100%" maw={MIDDLE_LAYOUT_MAX_W} mx="auto" p="md" gap="md">
          {loading ? (
            <SectionLoading rows={5} h="150px" />
          ) : !filteredModels || filteredModels.length === 0 ? (
            <Text mx="auto" c="dimmed">
              {HOME_SYSTEMS_NO_SYSTEMS_TEXT}
            </Text>
          ) : (
            <AnimatePresence mode="popLayout" initial={false}>
              {filteredModels.map((model) => (
                <motion.div
                  key={model.id}
                  layout="position"
                  initial={{ opacity: 0, height: 0 }}
                  animate={{ opacity: 1, height: 'auto' }}
                  exit={{ opacity: 0, height: 0 }}
                >
                  <SystemListCard model={model} />
                </motion.div>
              ))}
            </AnimatePresence>
          )}
        </StackMotion>
      </Stack>
    </ProtectedRoute>
  )
}
