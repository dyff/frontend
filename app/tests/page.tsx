/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import MethodListCard from '@/components/display/MethodListCard'
import { ALL_USECASES_KEY, UseCaseFilter } from '@/components/display/UseCaseFilter'
import UseCaseMethodGroupCard from '@/components/display/UseCaseMethodGroupCard'
import StackMotion from '@/components/layout/StackMotion'
import OpenInOperatorView from '@/components/nav/OpenInOperatorView'
import SectionLoading from '@/components/utility/SectionLoading'
import { MIDDLE_LAYOUT_MAX_W, PAGE_DEFS } from '@/lib/constants/constants'
import { DYFF_USECASE_INDEX_KEY } from '@/lib/constants/labels'
import {
  METHODS_CARD_SCORES_UNAVAILABLE,
  METHODS_SUB_TITLE,
  METHODS_TITLE
} from '@/lib/constants/copy'
import { findDocs, getUseCaseLabelKeyById, useIsMobile } from '@/lib/helpers'
import { useView } from '@/lib/hooks/view'
import { MethodWithDocs, UseCase, UseCaseWithMethods } from '@/types/api'
import { MethodListView } from '@/types/views'
import { Card, Group, Stack, Text, TextInput, Title, useMantineTheme } from '@mantine/core'
import { useDebouncedState, useDocumentTitle } from '@mantine/hooks'
import { showNotification } from '@mantine/notifications'
import { IconSearch } from '@tabler/icons-react'
import { AnimatePresence, motion } from 'motion/react'
import { useSearchParams } from 'next/navigation'
import { useMemo, useState } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.EXPLORE_TESTS.title)
  const theme = useMantineTheme()
  const isMobile = useIsMobile()
  const searchParams = useSearchParams()

  const [methodSearchText, setMethodSearchText] = useDebouncedState('', 100)
  const [useCaseSearchText, setUseCaseSearchText] = useDebouncedState('', 100)
  const [selectedUseCase, setSelectedUseCase] = useState(
    searchParams.get('usecase') || ALL_USECASES_KEY
  )

  const { data, loading } = useView<MethodListView>({
    route: `/views/methods`,
    onError: (msg) => {
      showNotification({ message: `Error: ${msg}` })
    }
  })

  const methods = data?.methods
  const usecases = data?.usecases
  const docs = data?.documentation

  const filteredMethods: MethodWithDocs[] = useMemo(() => {
    // Append docs property
    if (!methods) return []
    const combMethods = methods.map((m) => {
      return {
        ...m,
        docs: findDocs(m, docs)
      }
    })

    // Filter based on search text
    if (!methodSearchText) return combMethods
    return combMethods.filter(
      (method) =>
        method.name.toLowerCase().includes(methodSearchText.toLowerCase()) ||
        method.docs?.title?.toLowerCase().includes(methodSearchText.toLowerCase())
    )
  }, [data, methodSearchText])

  const filteredUsecases: UseCase[] = useMemo(() => {
    if (!usecases) return []
    const filtered = usecases.filter(
      (u) => selectedUseCase === ALL_USECASES_KEY || u.id === selectedUseCase
    )
    // If no use cases available, reset selectedUseCase
    // Per DYFF-599, leave empty
    // if (!filtered || !filtered.length) {
    //   setSelectedUseCase(ALL_USECASES_KEY)
    // }
    return filtered
  }, [data, selectedUseCase])

  const { usecasesWithMethods, orphanMethods } = useMemo(() => {
    // Aggregate usecase -> method(s)
    const orphanMethods = new Array<MethodWithDocs>()
    const newUsecases: UseCaseWithMethods[] =
      filteredUsecases?.map((u) => {
        return {
          ...u,
          methods: []
        }
      }) ?? []

    filteredMethods.forEach((m) => {
      // If no labels at all,
      if (!m.labels || !Object.keys(m.labels).length) {
        orphanMethods.push(m)
        return
      }

      // If some labels but no use case labels
      if (!Object.keys(m.labels).find((key) => key.includes(DYFF_USECASE_INDEX_KEY))) {
        orphanMethods.push(m)
        return
      }

      // If not orphan, Aggregate use case methods
      newUsecases.forEach((u, idx, arr) => {
        const thisUseCase = arr[idx]
        // If usecase label exists, add to use case
        if (Object.keys(m.labels!).find((key) => key === getUseCaseLabelKeyById(u.id))) {
          thisUseCase.methods.push(m)
        }
      })
    })

    // Dont show use cases without any methods
    const useCasesWithMethods = newUsecases.filter((uc) => !!uc.methods.length)

    // Don't show orphan methods if filtering use case and use cases available
    const ignoreOrphanArr =
      selectedUseCase !== ALL_USECASES_KEY || !newUsecases.length ? [] : orphanMethods

    return {
      usecasesWithMethods: useCasesWithMethods,
      orphanMethods: [...ignoreOrphanArr, ...ignoreOrphanArr, ...ignoreOrphanArr]
    }
  }, [filteredUsecases, filteredMethods])

  return (
    <ProtectedRoute>
      <Stack p={0} m={0} w="100%" gap={0} justify="start" align="center">
        <Card p={0} m={0} w="100%" bg={theme.colors['ul-light-blue'][0]} radius={0}>
          <Stack
            justify="start"
            align="start"
            gap="sm"
            pos="relative"
            w="100%"
            maw={MIDDLE_LAYOUT_MAX_W}
            mx="auto"
            p="md"
          >
            <Group>
              <Title order={2}>{METHODS_TITLE}</Title>
              <OpenInOperatorView
                label="Open Methods in Operator View"
                href={PAGE_DEFS.OPERATOR_METHODS.route}
              />
            </Group>
            <Text fz="lg">{METHODS_SUB_TITLE}</Text>
            <Group w="100%" justify="space-between">
              <TextInput
                leftSectionPointerEvents="none"
                leftSection={<IconSearch />}
                placeholder="Search tests"
                w={isMobile ? '100%' : '300px'}
                onChange={(e) => setMethodSearchText(e.target.value)}
              />
            </Group>
          </Stack>
        </Card>
        <StackMotion
          w="100%"
          justify="start"
          maw={MIDDLE_LAYOUT_MAX_W}
          mx="auto"
          p="md"
          gap="md"
          mb="md"
          layout="position"
        >
          <UseCaseFilter
            useCaseSearchText={useCaseSearchText}
            setUseCaseSearchText={setUseCaseSearchText}
            selectedUseCase={selectedUseCase}
            setSelectedUseCase={setSelectedUseCase}
            loading={loading}
            usecases={usecases}
          />
          {loading ? (
            <SectionLoading rows={5} h="150px" />
          ) : (!usecasesWithMethods || !usecasesWithMethods.length) &&
            (!orphanMethods || !orphanMethods.length) ? (
            <motion.div
              initial={{ opacity: 0, height: 0 }}
              animate={{ opacity: 1, height: 'auto' }}
              exit={{ opacity: 0, height: 0 }}
              style={{ width: '100%' }}
            >
              <Text ta="center" c="dimmed">
                {METHODS_CARD_SCORES_UNAVAILABLE}
              </Text>
            </motion.div>
          ) : (
            <AnimatePresence mode="popLayout">
              {usecasesWithMethods.map((u, idx) => (
                <motion.div
                  key={u.id}
                  layout="position"
                  initial={{ opacity: 0, height: 0 }}
                  animate={{ opacity: 1, height: 'auto' }}
                  exit={{ opacity: 0, height: 0 }}
                  style={{ width: '100%' }}
                >
                  <UseCaseMethodGroupCard key={idx} usecase={u} />
                </motion.div>
              ))}
              {orphanMethods.map((m, idx) => (
                <motion.div
                  key={m.id}
                  layout="position"
                  initial={{ opacity: 0, height: 0 }}
                  animate={{ opacity: 1, height: 'auto' }}
                  exit={{ opacity: 0, height: 0 }}
                  style={{ width: '100%' }}
                >
                  <MethodListCard key={idx} method={m} />
                </motion.div>
              ))}
            </AnimatePresence>
          )}
        </StackMotion>
      </Stack>
    </ProtectedRoute>
  )
}
