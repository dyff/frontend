/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'
import ProtectedRoute from '@/components/auth/ProtectedRoute'
import PillSegmentedControl from '@/components/display/inputs/PillSegmentedControl'
import NoDataDisplay from '@/components/display/NoData'
import ScoreBarChart from '@/components/display/ScoreBarChart'
import SystemScoreCard from '@/components/display/SystemScoreCard'
import OpenInOperatorView from '@/components/nav/OpenInOperatorView'
import SafeMDPreview from '@/components/utility/SafeMDPreview'
import SectionLoading from '@/components/utility/SectionLoading'
import { MIDDLE_LAYOUT_MAX_W, PAGE_DEFS } from '@/lib/constants/constants'
import { SCORE_INDEX_KEY } from '@/lib/constants/labels'
import {
  METHODS_CARD_TITLE,
  METHODS_TAB_DOCS_TITLE,
  METHODS_TAB_SCORES_TITLE
} from '@/lib/constants/copy'
import { findDocs } from '@/lib/helpers'
import { useView } from '@/lib/hooks/view'
import { CombinedScore, InferenceServiceWithDocs, ModelWithDocs } from '@/types/api'
import { MethodSingleView, ScoreSpec } from '@/types/views'
import {
  Card,
  Center,
  Group,
  SegmentedControl,
  Select,
  Stack,
  Text,
  Title,
  useMantineTheme
} from '@mantine/core'
import { AnimatePresence, motion } from 'motion/react'
import { useEffect, useMemo, useState } from 'react'

type SortOption = 'ascending' | 'descending'

export default function Page({ params }: { params: { id: string } }) {
  const theme = useMantineTheme()
  const { data, loading } = useView<MethodSingleView>({
    route: `/views/methods/${params.id}`
  })

  const method = data?.method
  const docs = data?.documentation
  const services = data?.services
  const models = data?.models
  const safetycases = data?.safetycases
  const scores = data?.scores
  const scoreSpecs = data?.method.scores

  const methodDocs = findDocs(method, docs)

  const [activeTab, setActiveTab] = useState('evaluations')
  const [sortOption, setSortOption] = useState<SortOption>('ascending')
  const [scoreType, setScoreType] = useState('')

  const primaryScoreSpec = scoreSpecs?.find((s) => s.priority === 'primary')
  const selectedScoreSpec: ScoreSpec | undefined =
    scoreSpecs?.find((spec) => scoreType === spec[SCORE_INDEX_KEY]) ?? undefined

  // On initial data load
  useEffect(() => {
    document.title = methodDocs?.title
      ? `${methodDocs.title} | Dyff`
      : PAGE_DEFS.EXPLORE_TESTS.title

    if (scores && scores.length > 0) {
      const primaryScore = scores.find((s) => s.priority === 'primary')
      if (primaryScore) {
        setScoreType(primaryScore[SCORE_INDEX_KEY])
      } else {
        setScoreType(scores[0][SCORE_INDEX_KEY])
      }
    }
  }, [data])

  const availableScoreTypes = useMemo(() => {
    // Verify no duplicates - Mantine select dislikes this
    const unique = new Set<string>()
    return (
      scoreSpecs?.filter((spec) => {
        if (unique.has(spec[SCORE_INDEX_KEY])) return false
        unique.add(spec[SCORE_INDEX_KEY])
        return true
      }) || []
    )
  }, [scoreSpecs])

  // Apply filtering and ordering of scores
  const [chartScores, cardScores] = useMemo(() => {
    if (!scores || !services || !models || !safetycases || !scoreType || !docs) {
      return [[], []]
    }

    const servicesMap = new Map(services.map((service) => [service.id, service]))
    const modelsMap = new Map(models.map((model) => [model.id, model])) // Lookup via inf svc id

    const filteredScores = scores
      .filter((score) => score[SCORE_INDEX_KEY] === scoreType)
      .map((score) => {
        const {
          inferenceService: inferenceServiceId,
          method: methodId,
          model: modelId // Rely on found service model ref instead
        } = score.metadata.refs

        const service = inferenceServiceId ? servicesMap.get(inferenceServiceId) : undefined
        const serviceDocs = service ? findDocs(service, docs) : undefined
        const serviceWithDocs: InferenceServiceWithDocs | undefined = service
          ? { ...service, docs: serviceDocs }
          : undefined

        const model = service && service.model ? modelsMap.get(service.model.id) : undefined
        const modelDocs = model ? findDocs(model, docs) : undefined
        const modelWithDocs: ModelWithDocs | undefined = model
          ? { ...model, docs: modelDocs }
          : undefined

        const matchingSafetyCase = safetycases.find(
          (safetyCase) =>
            safetyCase.scope?.inferenceService === inferenceServiceId &&
            safetyCase.method.id === methodId
        )

        return {
          ...score,
          service: serviceWithDocs,
          model: modelWithDocs,
          method: method,
          safetyCase: matchingSafetyCase
        } as CombinedScore
      })

    const ascSorted = filteredScores.sort((a, b) =>
      sortOption === 'ascending' ? a.quantity - b.quantity : b.quantity - a.quantity
    )

    const valenceSorted =
      selectedScoreSpec && selectedScoreSpec.valence === 'negative'
        ? ascSorted.toReversed()
        : ascSorted

    // Chart scores display in reverse order
    const cardScores: CombinedScore[] = valenceSorted.toReversed()
    return [valenceSorted, cardScores]
  }, [data, scoreType, sortOption])

  const noReportsCondition =
    !chartScores ||
    !chartScores.length ||
    !scoreSpecs ||
    !scoreSpecs.length ||
    !primaryScoreSpec ||
    !selectedScoreSpec

  return (
    <ProtectedRoute>
      <Stack w="100%" p={0} m={0} gap={0}>
        <Card p={0} m={0} w="100%" bg={theme.colors['ul-light-blue'][0]} radius={0}>
          <Stack
            justify="start"
            align="start"
            gap={0}
            pos="relative"
            w="100%"
            maw={MIDDLE_LAYOUT_MAX_W}
            mx="auto"
            p="md"
          >
            {loading ? (
              <>
                <SectionLoading rows={3} h="30px" />
              </>
            ) : !method ? (
              <NoDataDisplay />
            ) : (
              <>
                <Group gap="sm" align="end">
                  <Title order={2}>{methodDocs?.title || method.name}</Title>
                  <OpenInOperatorView
                    label="Open Method in Operator View"
                    href={`${PAGE_DEFS.OPERATOR_METHODS.route}/${params.id}`}
                  />
                </Group>
                <Text c="dimmed" w="auto" mb="xs">
                  {METHODS_CARD_TITLE}
                </Text>
                {!methodDocs?.summary ? (
                  <Text c="dimmed">Description Unavailable</Text>
                ) : (
                  <SafeMDPreview source={methodDocs.summary} />
                )}
                <SegmentedControl
                  mt="xs"
                  fz="lg"
                  fw={700}
                  value={activeTab}
                  onChange={setActiveTab}
                  data={[
                    { label: METHODS_TAB_SCORES_TITLE, value: 'evaluations' },
                    { label: METHODS_TAB_DOCS_TITLE, value: 'documentation' }
                  ]}
                  styles={{
                    root: {
                      backgroundColor: 'transparent'
                    },
                    indicator: {
                      backgroundColor: 'rgba(111, 111, 111, 0.1)'
                    },
                    label: {
                      fontSize: theme.fontSizes.lg,
                      fontWeight: 700
                    }
                  }}
                />
              </>
            )}
          </Stack>
        </Card>
        <Stack
          w="100%"
          maw={MIDDLE_LAYOUT_MAX_W}
          mx="auto"
          gap="md"
          justify="flex-start"
          align="start"
          p="md"
          mb="md"
        >
          {activeTab === 'evaluations' ? (
            loading ? (
              <>
                <SectionLoading rows={1} h="400px" />
                <SectionLoading rows={5} h="150px" />
              </>
            ) : noReportsCondition ? (
              <Center w="100%">
                <Text c="dimmed">We're working on safety reports! Check back soon.</Text>
              </Center>
            ) : (
              <>
                <ScoreBarChart scores={chartScores} method={method} scoreSpec={selectedScoreSpec} />
                <Group justify="space-between" align="center" w="100%">
                  <Group
                    bg={theme.colors.gray[0]}
                    gap={0}
                    align="stretch"
                    style={{
                      borderColor: theme.colors.gray[3],
                      borderRadius: theme.radius.lg
                    }}
                    p="4px"
                  >
                    <Center
                      style={{
                        borderTopLeftRadius: theme.radius.lg,
                        borderBottomLeftRadius: theme.radius.lg,
                        borderTopRightRadius: 0,
                        borderBottomRightRadius: 0
                      }}
                    >
                      <Text fz="lg" fw={700} px="sm">
                        Score
                      </Text>
                    </Center>
                    <Select
                      variant="filled"
                      w="300px"
                      placeholder="Score"
                      data={availableScoreTypes.map((spec) => {
                        return {
                          label: spec.title,
                          value: spec[SCORE_INDEX_KEY]
                        }
                      })}
                      defaultValue={primaryScoreSpec[SCORE_INDEX_KEY]}
                      value={scoreType}
                      onChange={(_value, option) => {
                        if (!option) return
                        setScoreType(option.value)
                      }}
                      radius="lg"
                      styles={{
                        input: {
                          fontSize: theme.fontSizes.lg,
                          fontWeight: 700,
                          backgroundColor: theme.colors['ul-light-blue'][0]
                        }
                      }}
                      comboboxProps={{
                        offset: 0,
                        transitionProps: { transition: 'fade-down', duration: 200 }
                      }}
                    />
                  </Group>
                  <PillSegmentedControl
                    size="lg"
                    value={sortOption}
                    onChange={(value: string) => setSortOption(value as SortOption)}
                    data={[
                      { label: 'Best', value: 'ascending' },
                      { label: 'Worst', value: 'descending' }
                    ]}
                    label="Sort"
                  />
                </Group>
                <Stack gap="md" w="100%">
                  <AnimatePresence mode="popLayout">
                    {cardScores.map((score, idx) => {
                      return (
                        <motion.div
                          key={`${score.id}_${idx}`}
                          // key={score.id}
                          layout="position"
                          initial={{ opacity: 0 }}
                          animate={{ opacity: 1 }}
                          exit={{ opacity: 0 }}
                          transition={{ duration: 0.7 }}
                        >
                          <SystemScoreCard score={score} />
                        </motion.div>
                      )
                    })}
                  </AnimatePresence>
                </Stack>
              </>
            )
          ) : loading ? (
            <SectionLoading h="300px" rows={1} />
          ) : !methodDocs || !methodDocs.fullPage || !methodDocs.fullPage.length ? (
            <Center w="100%">
              <Text c="dimmed">Documentation is 10 story points away... stayed tuned!</Text>
            </Center>
          ) : (
            <Card withBorder p="md">
              <SafeMDPreview source={methodDocs.fullPage} />
            </Card>
          )}
        </Stack>
      </Stack>
    </ProtectedRoute>
  )
}
