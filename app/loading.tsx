/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import PageLoading from '@/components/utility/PageLoading'

export default PageLoading
