/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import HomeSystemCard from '@/components/display/HomeSystemCard'
import HomeUseCaseCard from '@/components/display/HomeUseCaseCard'
import SectionLoading from '@/components/utility/SectionLoading'
import {
  EXPLORE_APP_SHELL_HEADER_HEIGHT,
  MIDDLE_LAYOUT_MAX_W,
  PAGE_DEFS
} from '@/lib/constants/constants'
import {
  HOME_SPLASH_BUTTON_TEXT,
  HOME_SPLASH_SUB_TITLE,
  HOME_SPLASH_SUB_TITLE_SIZE,
  HOME_SPLASH_TITLE,
  HOME_SPLASH_TITLE_SIZE,
  HOME_SYSTEMS_SUB_TITLE,
  HOME_SYSTEMS_SUB_TITLE_SIZE,
  HOME_SYSTEMS_TITLE,
  HOME_SYSTEMS_TITLE_SIZE,
  HOME_USE_CASES_BUTTON_TEXT,
  HOME_USE_CASES_CARD_UNAVAILABLE,
  HOME_USE_CASES_SUB_TITLE,
  HOME_USE_CASES_SUB_TITLE_SIZE,
  HOME_USE_CASES_TITLE,
  HOME_USE_CASES_TITLE_SIZE,
  MAKE_HOME_SYSTEMS_BUTTON_TEXT
} from '@/lib/constants/copy'
import { useIsMobile } from '@/lib/helpers'
import { useView } from '@/lib/hooks/view'
import classes from '@/styles/HomePage.module.css'
import { ModelWithDocs, UseCase } from '@/types/api'
import { HomeView } from '@/types/views'
import {
  Box,
  Button,
  Card,
  Center,
  Grid,
  Group,
  Stack,
  Text,
  Title,
  useMantineTheme
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { IconArrowBadgeDown } from '@tabler/icons-react'
import { motion } from 'motion/react'
import Image from 'next/image'
import { useMemo, useRef } from 'react'

const WAVES_GRAPHIC_SIZE = 1750
const DYFF_GRAPHIC_SIZE = 1500
const NUM_USECASES_DISPLAY = 3
const NUM_SYSTEMS_DISPLAY = 3
const FIRST_SECTION_H = `calc(100vh - ${EXPLORE_APP_SHELL_HEADER_HEIGHT}px)`
const SECTION_H = '100vh'

export default function HomePage() {
  useDocumentTitle(PAGE_DEFS.HOME.title)
  const isMobile = useIsMobile()
  const theme = useMantineTheme()
  const usecaseRef = useRef<HTMLDivElement>(null)

  const { data, loading } = useView<HomeView>({
    route: '/views/home'
  })

  const models = data?.models
  const usecases = data?.usecases
  const docs = data?.documentation

  // Selection of use cases (TBD)
  const selectedUsecases: UseCase[] = useMemo(() => {
    if (loading || !usecases || !usecases.length) return []
    return usecases.slice(0, NUM_USECASES_DISPLAY)
  }, [loading])

  const selectedSystems: ModelWithDocs[] = useMemo(() => {
    if (!models) return []
    return models
      ?.map((model) => {
        const theseDocs = docs?.find((doc) => doc.entity && doc.entity === model.id)
        return {
          ...model,
          docs: theseDocs
        }
      })
      .splice(0, NUM_SYSTEMS_DISPLAY)
  }, [data])

  return (
    <Stack gap={0} justify="start" align="center">
      <Group
        h="auto"
        mih={FIRST_SECTION_H}
        className={`${classes['section']} ${classes['splash-section']}`}
      >
        <Box
          w="100%"
          maw={MIDDLE_LAYOUT_MAX_W}
          mx="auto"
          pt="xl"
          style={{ zIndex: 5 }}
          pos="relative"
        >
          <Stack
            justify="start"
            align={isMobile ? 'center' : 'start'}
            px="md"
            maw="700px"
            gap={0}
            pos="relative"
          >
            <motion.span
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ duration: 1 }}
            >
              <Title
                c={theme.colors['ul-dark-blue'][9]}
                fz={HOME_SPLASH_TITLE_SIZE}
                ta={isMobile ? 'center' : 'start'}
                my={0}
                textWrap="balance"
              >
                {HOME_SPLASH_TITLE}
              </Title>
            </motion.span>
            <motion.span
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ duration: 1, delay: 0.25 }}
            >
              <Title
                fz={HOME_SPLASH_SUB_TITLE_SIZE}
                ta={isMobile ? 'center' : 'start'}
                my={0}
                textWrap="balance"
              >
                {HOME_SPLASH_SUB_TITLE}
              </Title>
            </motion.span>
            <motion.span
              initial={{ opacity: 0 }}
              animate={{ opacity: 1 }}
              transition={{ duration: 1, delay: 0.5 }}
            >
              <Button
                mt="lg"
                className={classes['splash-btn']}
                size="compact-xl"
                onClick={() => {
                  usecaseRef.current?.scrollIntoView({ behavior: 'smooth' })
                }}
              >
                {HOME_SPLASH_BUTTON_TEXT}
                <IconArrowBadgeDown size={30} style={{ marginLeft: '3px' }} />
              </Button>
            </motion.span>
            {!isMobile && (
              <>
                <motion.div
                  initial={{ x: DYFF_GRAPHIC_SIZE, opacity: 0 }}
                  animate={{ x: 0, opacity: 1 }}
                  transition={{ duration: 0.25, type: 'spring', stiffness: 25 }}
                  style={{
                    bottom: -(DYFF_GRAPHIC_SIZE / 3),
                    right: -DYFF_GRAPHIC_SIZE,
                    position: 'absolute',
                    pointerEvents: 'none',
                    zIndex: 1
                  }}
                >
                  <Image
                    src="/static/logo.svg"
                    alt="Dyff Logo"
                    width={DYFF_GRAPHIC_SIZE}
                    height={DYFF_GRAPHIC_SIZE}
                    priority
                  />
                </motion.div>
                <motion.div
                  initial={{ x: DYFF_GRAPHIC_SIZE, opacity: 0 }}
                  animate={{ x: 0, opacity: 1 }}
                  transition={{ duration: 0.25, type: 'spring', stiffness: 25 }}
                  style={{
                    bottom: -(DYFF_GRAPHIC_SIZE / 3),
                    right: -DYFF_GRAPHIC_SIZE,
                    position: 'absolute',
                    pointerEvents: 'none',
                    zIndex: 1
                  }}
                >
                  <Image
                    src="/static/logo.svg"
                    alt="Dyff Logo"
                    width={DYFF_GRAPHIC_SIZE}
                    height={DYFF_GRAPHIC_SIZE}
                    style={{
                      filter: 'blur(40px)'
                    }}
                    priority
                  />
                </motion.div>
              </>
            )}
          </Stack>
        </Box>
      </Group>
      <Group
        mih={SECTION_H}
        className={`${classes['section']} ${classes['usecases-section']}`}
        ref={usecaseRef}
      >
        <Image
          src="/static/dsri-circles.svg"
          alt="DSRI Circles SVG"
          width={WAVES_GRAPHIC_SIZE}
          height={WAVES_GRAPHIC_SIZE}
          style={{
            bottom: -(WAVES_GRAPHIC_SIZE / 2),
            left: -(WAVES_GRAPHIC_SIZE / 2),
            filter: 'blur(25px)',
            position: 'absolute',
            opacity: 0.5
          }}
        />
        <Box w="100%" maw={MIDDLE_LAYOUT_MAX_W} mx="auto" py="md">
          <Grid p={0} w="100%">
            <Grid.Col span={{ sm: 6, xs: 12 }}>
              <Stack justify="start" align={isMobile ? 'center' : 'start'} gap="md" px="md" mt="md">
                {loading ? (
                  <SectionLoading rows={3} h="90px" w="400px" />
                ) : !selectedUsecases.length ? (
                  <Card withBorder shadow={undefined} w="100%" maw="400px">
                    <Center>
                      <Text c="dimmed">{HOME_USE_CASES_CARD_UNAVAILABLE}</Text>
                    </Center>
                  </Card>
                ) : (
                  selectedUsecases.map((uc, idx) => <HomeUseCaseCard key={idx} usecase={uc} />)
                )}
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ sm: 6, xs: 12 }} order={isMobile ? -1 : 2}>
              <Stack
                h="100%"
                justify="start"
                align={isMobile ? 'center' : 'start'}
                px="md"
                w="100%"
                maw={isMobile ? '100%' : '600px'}
              >
                <Title c="white" ta={isMobile ? 'center' : 'start'} fz={HOME_USE_CASES_TITLE_SIZE}>
                  {HOME_USE_CASES_TITLE}
                </Title>
                <Text
                  c={theme.colors.gray[2]}
                  ta={isMobile ? 'center' : 'start'}
                  fz={HOME_USE_CASES_SUB_TITLE_SIZE}
                >
                  {HOME_USE_CASES_SUB_TITLE}
                </Text>
                <Button
                  size="compact-xl"
                  className={classes['usecases-btn']}
                  component="a"
                  href={PAGE_DEFS.EXPLORE_TESTS.route}
                >
                  {HOME_USE_CASES_BUTTON_TEXT}
                </Button>
              </Stack>
            </Grid.Col>
          </Grid>
        </Box>
      </Group>
      <Group mih={SECTION_H} className={`${classes['section']} ${classes['systems-section']}`}>
        {Array.from(new Array(6).fill(0)).map((_, idx) => {
          const size = 2250 - 300 * (idx + 1)
          return (
            <Image
              key={idx}
              src="/static/dsri-dot-circle.svg"
              alt="DSRI Circles SVG"
              width={size}
              height={size}
              style={{
                top: -(size / 2),
                right: -(size / 2),
                filter: `blur(${size / 45}px)`,
                position: 'absolute',
                opacity: 1,
                pointerEvents: 'none'
              }}
            />
          )
        })}
        <Box w="100%" maw={MIDDLE_LAYOUT_MAX_W} mx="auto" py="md">
          <Grid p={0} w="100%">
            <Grid.Col span={{ sm: 6, xs: 12 }}>
              <Stack
                h="100%"
                justify="start"
                align={isMobile ? 'center' : 'start'}
                px="lg"
                w="100%"
                maw={isMobile ? '100%' : '600px'}
              >
                <Title ta={isMobile ? 'center' : 'start'} fz={HOME_SYSTEMS_TITLE_SIZE}>
                  {HOME_SYSTEMS_TITLE}
                </Title>
                <Text ta={isMobile ? 'center' : 'start'} fz={HOME_SYSTEMS_SUB_TITLE_SIZE}>
                  {HOME_SYSTEMS_SUB_TITLE}
                </Text>
                <Button
                  className={classes['splash-btn']}
                  size="compact-xl"
                  component="a"
                  href={PAGE_DEFS.EXPLORE_SYSTEMS.route}
                >
                  {MAKE_HOME_SYSTEMS_BUTTON_TEXT(undefined)}
                </Button>
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ sm: 6, xs: 12 }}>
              <Stack justify="start" align={isMobile ? 'center' : 'start'} gap="md" px="lg" mt="md">
                {loading ? (
                  <SectionLoading rows={3} h="90px" w="400px" />
                ) : (
                  selectedSystems.map((m, idx) => <HomeSystemCard key={idx} model={m} />)
                )}
              </Stack>
            </Grid.Col>
          </Grid>
        </Box>
      </Group>
    </Stack>
  )
}
