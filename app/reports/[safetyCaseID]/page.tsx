/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import ResponsiveIframe from '@/components/display/ResponsiveIframe'
import OpenInOperatorView from '@/components/nav/OpenInOperatorView'
import { EXPLORE_APP_SHELL_MAIN_HEIGHT, PAGE_DEFS } from '@/lib/constants/constants'
import { REPORTS_UNAVAILABLE } from '@/lib/constants/copy'
import { useEntityData } from '@/lib/hooks/entity'
import { SafetyCase } from '@/types/api'
import { Button, Group, Stack, Text } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useRouter } from 'next/navigation'
import PageLoading from './loading'

export default function Page({ params }: { params: { safetyCaseID: string } }) {
  const router = useRouter()
  useDocumentTitle(PAGE_DEFS.EXPLORE_REPORTS.title)
  const { artifacts: scWithArtifactsArr, loading: scLoading } = useEntityData<SafetyCase>(
    'safetycases',
    {
      initParams: { id: params.safetyCaseID },
      usePublic: true,
      useInternal: true,
      fetchArtifacts: true
    }
  )

  // Assume only one safety case was found
  const thisSafetyCase =
    scWithArtifactsArr && scWithArtifactsArr.length ? scWithArtifactsArr[0] : null

  // Assume only one notebook
  const thisNotebook = thisSafetyCase?.artifacts.data?.find((a) => a.kind === 'notebook')?.data

  return (
    <ProtectedRoute>
      {scLoading ? (
        <PageLoading />
      ) : !thisSafetyCase ||
        thisSafetyCase.artifacts.error ||
        !thisNotebook ||
        !thisNotebook.length ? (
        <Stack h={EXPLORE_APP_SHELL_MAIN_HEIGHT} w="100%" justify="center" align="center">
          <OpenInOperatorView
            href={`${PAGE_DEFS.OPERATOR_SAFETY_CASES.route}/${params.safetyCaseID}`}
            position="relative"
          />
          <Text c="dimmed">{REPORTS_UNAVAILABLE}</Text>
          <Group>
            <Button onClick={() => router.back()} variant="subtle">
              Back
            </Button>
            <Button component="a" href={PAGE_DEFS.HOME.route} variant="default">
              Go Home
            </Button>
          </Group>
        </Stack>
      ) : (
        <ResponsiveIframe srcDoc={thisNotebook} />
      )}
    </ProtectedRoute>
  )
}
