/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import ExploreNavShell from '@/components/nav/ExploreNavShell'

interface HomeLayoutProps {
  children: JSX.Element
}

export default function HomeLayout({ children }: HomeLayoutProps) {
  return <ExploreNavShell>{children}</ExploreNavShell>
}
