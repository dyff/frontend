/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { AxiosError } from 'axios'
import { NextRequest, NextResponse } from 'next/server'

type ContextType = {
  params: {
    slug: string[]
  }
}

export const dynamic = 'force-dynamic'

export async function GET(request: Request, context: ContextType) {
  const forwardedURL = assembleRewriteURL(request, context)
  try {
    const headers = new Headers(request.headers)
    headers.set('accept-encoding', 'gzip, deflate, br')
    const res = await fetch(forwardedURL, {
      method: 'GET',
      headers: headers,
      cache: 'no-store'
    })

    if (res.ok) {
      const contentType = res.headers.get('content-type')
      if (contentType && contentType.includes('application/json')) {
        const jsonRes = await res.json()
        return NextResponse.json(jsonRes, { status: res.status })
      }
    }

    return res
  } catch (e) {
    const err = e as Error
    return NextResponse.json({ error: err.message, forwardedURL: forwardedURL }, { status: 500 })
  }
}

export async function PATCH(request: NextRequest, context: ContextType) {
  const forwardedURL = assembleRewriteURL(request, context)
  const token = request.headers.get('Authorization')
  if (!token) {
    return NextResponse.json({}, { status: 401, statusText: 'No Authorization token passed!' })
  }

  try {
    const data = await request.json()

    const res = await fetch(forwardedURL, {
      method: 'PATCH',
      headers: new Headers({
        Authorization: token,
        'Content-Type': 'application/json'
      }),
      body: JSON.stringify(data),
      cache: 'no-store'
    })

    if (!res.ok) {
      // Res body is null and may fail to parse - expected when success
      if (res.status === 422) {
        return NextResponse.json(
          {},
          {
            status: 200,
            statusText: 'OK Expected 422 success'
          }
        )
      }
      // Extract dyff-api detail error JSON
      if (res.body) {
        const body = await res.json()
        return NextResponse.json(body, {
          status: res.status,
          statusText: res.statusText
        })
      }
    }

    if (res.body) {
      const body = await res.json()
      return NextResponse.json(body, { ...res })
    }

    return res
  } catch (e) {
    if (e instanceof AxiosError) {
      const status = e.response ? e.response.status : 400
      const msg = e.response ? e.response.statusText : e.message
      return NextResponse.json(
        {},
        {
          status: status,
          statusText: msg
        }
      )
    } else {
      const err = e as Error
      return NextResponse.json(
        {},
        {
          status: 400,
          statusText: err.message
        }
      )
    }
  }
}

/**
 * Rewrites an API request URL to the rewritten URL using the runtime env var API URL.
 * In expected fetch format
 * @param request The Route Request object
 * @param context Context containing the slug params
 */
const assembleRewriteURL = (request: Request, context: ContextType): string => {
  // const slug = context.params.slug
  const pathnames = context.params.slug.join('/')
  const DYFF_API_FULL_URL = process.env.DYFF_API_FULL_URL
  const searchParams = new URL(request.url).searchParams
  const pathedURL = `${DYFF_API_FULL_URL}/${pathnames}`
  const fullURL = searchParams.size ? `${pathedURL}?${searchParams.toString()}` : pathedURL
  return fullURL
}
