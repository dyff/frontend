/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import ProtectedRoute from '@/components/auth/ProtectedRoute'
import InfoChip from '@/components/display/InfoChip'
import { formatDateStr, truncateStr } from '@/lib/helpers'
import { ActionIcon, Card, Container, Group, ScrollArea, Stack, Text, Tooltip } from '@mantine/core'
import { notifications } from '@mantine/notifications'
import { IconClipboard } from '@tabler/icons-react'

export default function UserProfile() {
  const { token, decodedToken, tokenExpiresAt, tokenIssuedAt } = useAuth()

  const copyToken = (): void => {
    navigator.clipboard.writeText(token)
    notifications.show({ message: 'Copied token to clipboard!' })
  }

  return (
    <ProtectedRoute>
      <Container>
        <Stack gap="sm">
          <Card withBorder>
            <ScrollArea>
              <Stack>
                {!token ? (
                  <Text>No token loaded</Text>
                ) : (
                  <InfoChip
                    label="Token"
                    content={
                      <Group gap="xs">
                        <Tooltip label="Copy to clipboard" position="left">
                          <ActionIcon onClick={copyToken} size="sm">
                            <IconClipboard />
                          </ActionIcon>
                        </Tooltip>
                        <Text>{truncateStr(token, 30)}</Text>
                      </Group>
                    }
                  />
                )}

                {!decodedToken ? (
                  <Text>Invalid token!</Text>
                ) : (
                  <>
                    <InfoChip label="Account ID" content={decodedToken.sub} />
                    <InfoChip
                      label="Issued"
                      content={!tokenIssuedAt ? 'N/A' : formatDateStr(tokenIssuedAt)}
                    />
                    <InfoChip
                      label="Expires"
                      content={!tokenExpiresAt ? 'N/A' : formatDateStr(tokenExpiresAt)}
                    />
                  </>
                )}
              </Stack>
            </ScrollArea>
          </Card>
        </Stack>
      </Container>
    </ProtectedRoute>
  )
}
