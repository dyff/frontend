/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { redirect } from 'next/navigation'
import { useEffect } from 'react'

export default function User() {
  useEffect(() => {
    redirect('/user/profile')
  }, [])
  return <></>
}
