/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  Button,
  Card,
  Container,
  Group,
  Stack,
  Table,
  TableData,
  TextInput,
  Title,
  Transition
} from '@mantine/core'
import { useDisclosure, useSetState } from '@mantine/hooks'
import { notifications } from '@mantine/notifications'
import { useRouter } from 'next/navigation'

export default function UserKeys() {
  const router = useRouter()
  // const session = useSession()
  // if (!session || !session.data) {
  //   router.push('/api/auth/signin')
  //   return <></>
  // }

  const [showCreateKeySection, handlers] = useDisclosure(false)
  const [newKey, setNewKey] = useSetState({
    alias: ''
  })
  const tableData: TableData = {
    head: ['Alias', 'Key', 'Created'],
    body: [
      ['Key1', '3g4534gg243g24g24', new Date().toLocaleDateString()],
      ['Key1', '3g4534gg243g24g24', new Date().toLocaleDateString()],
      ['Key1', '3g4534gg243g24g24', new Date().toLocaleDateString()]
    ]
  }

  const handleCreateNewKey = () => {
    handlers.close()
    notifications.show({
      title: 'Key Created',
      message: 'Created key ' + newKey.alias
    })
  }

  const handleCancelNewKey = () => {
    handlers.close()
    setNewKey({ alias: '' })
  }

  return (
    <Container>
      <Stack gap="sm">
        <Title>Keys</Title>
        <Card withBorder shadow="sm">
          <Card.Section>
            <Group p="sm">
              <Title order={3}>API Keys</Title>
              {!showCreateKeySection && (
                <Button
                  size="sm"
                  hidden={showCreateKeySection}
                  ml="auto"
                  onClick={() => handlers.open()}
                >
                  Add Key
                </Button>
              )}
            </Group>
          </Card.Section>
          <Transition mounted={showCreateKeySection}>
            {(styles) => (
              <Card.Section p="sm" withBorder bg="gray.1" style={styles}>
                <Stack align="start">
                  <Title order={4}>Create API Key</Title>
                  <TextInput label="Alias" placeholder="My Key" w="50%" />
                  <Group gap="sm">
                    <Button onClick={handleCreateNewKey}>Create</Button>
                    <Button onClick={handleCancelNewKey}>Cancel</Button>
                  </Group>
                </Stack>
              </Card.Section>
            )}
          </Transition>

          <Card.Section>
            <Table highlightOnHover captionSide="top" data={tableData} />
          </Card.Section>
        </Card>
      </Stack>
    </Container>
  )
}
