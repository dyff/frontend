/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { LoadingOverlay } from '@mantine/core'
import { redirect } from 'next/navigation'
import { useEffect } from 'react'

export default function HomePage() {
  useEffect(() => {
    redirect('/home')
  }, [])
  return <LoadingOverlay zIndex={1000} overlayProps={{ radius: 'sm', blur: 2 }} />
}
