/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import LabelsSection from '@/components/data/LabelsSection'
import UseCaseDocumentationSection from '@/components/data/UsecaseDocumentationSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import SectionLoading from '@/components/utility/SectionLoading'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { DYFF_USECASE_INDEX_VALUE } from '@/lib/constants/labels'
import { formatDateStr, getUseCaseLabelKeyById } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Method, UseCase } from '@/types/api'
import {
  Button,
  Container,
  Fieldset,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Tabs,
  Text,
  Title
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { IconArrowBigRightLine } from '@tabler/icons-react'
import { useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_USE_CASES.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    refresh,
    updateLabels,
    updateDocs,
    error,
    loading
  } = useEntityData<UseCase>('usecases', {
    initParams: { id: params.id }
  })

  const { docs: methods, loading: methodsLoading } = useEntityData<Method>('methods', {
    fetchDocs: true,
    initParams: {
      labels: {
        [getUseCaseLabelKeyById(params.id)]: DYFF_USECASE_INDEX_VALUE
      }
    }
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const colSpan = { lg: 12, xl: 6 }

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={
              <>
                <Title>{data.documentation?.title ?? data.id}</Title>
                <Text c="dimmed">{data.documentation?.summary ?? 'No description'}</Text>
              </>
            }
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                  <Button
                    color="ul-dark-blue"
                    size="xs"
                    w="fit-content"
                    component="a"
                    href={`${PAGE_DEFS.EXPLORE_TESTS.route}?usecase=${params.id}`}
                  >
                    Open in Explore View
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Status" content={data.status} />
                <InfoChip label="Reason" content={data.reason} />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
                <InfoChip
                  label="Last Transition"
                  content={formatDateStr(data.lastTransitionTime)}
                />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="docs">Documentation</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Methods" h="100%">
                        {methodsLoading ? (
                          <SectionLoading rows={3} />
                        ) : !methods || !methods.length ? (
                          <Text c="dimmed" mx="auto">
                            No linked methods found
                          </Text>
                        ) : (
                          methods.map((m, idx) => {
                            return (
                              <Button
                                key={idx}
                                variant="outline"
                                component="a"
                                href={`${PAGE_DEFS.OPERATOR_METHODS.route}/${m.id}`}
                                w="100%"
                                p={0}
                              >
                                <Group w="100%" justify="start">
                                  <Text fw="bold">{m.docs?.title ?? m.name}</Text>
                                  <IconArrowBigRightLine size={15} style={{ marginLeft: 'auto' }} />
                                </Group>
                              </Button>
                            )
                          })
                        )}
                      </Fieldset>
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="docs">
                  <UseCaseDocumentationSection data={data} updateDocs={updateDocs} />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
