/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import DocumentationSection from '@/components/data/DocumentationSection'
import LabelsSection from '@/components/data/LabelsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import EntityView from '@/components/display/EntityView'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import TableStatusIcon from '@/components/utility/TableStatusIcon'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getUniqueStatuses } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Measurement, Method, SafetyCase } from '@/types/api'
import {
  Button,
  Code,
  Container,
  Fieldset,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Tabs,
  Text,
  Title
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo, useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_METHODS.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    docs: docsArr,
    refresh,
    updateLabels,
    updateDocs,
    error,
    loading
  } = useEntityData<Method>('methods', {
    initParams: { id: params.id },
    fetchDocs: true
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const docs = docsArr && docsArr.length ? docsArr[0] : null
  const colSpan = { lg: 12, xl: 6 }

  const {
    data: measData,
    refresh: measRefresh,
    error: measError,
    loading: measLoading,
    lastUpdated: measLastUpdated
  } = useEntityData<Measurement>('measurements', {
    initParams: { method: params.id }
  })

  const {
    data: scData,
    refresh: scRefresh,
    error: scError,
    loading: scLoading,
    lastUpdated: scLastUpdated
  } = useEntityData<SafetyCase>('safetycases', {
    initParams: { method: params.id }
  })

  const measColumns: Array<MRT_ColumnDef<Measurement>> = useMemo(
    () => [
      {
        id: 'status',
        header: 'Status',
        accessorFn: (d) => d.status,
        Cell: ({ row }) => <TableStatusIcon status={row.original.status} />,
        size: 150,
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(measData)
        },
        mantineTableBodyCellProps: {
          align: 'center'
        },
        enableSorting: true,
        grow: false
      },
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [measData]
  )

  const scColumns: Array<MRT_ColumnDef<SafetyCase>> = useMemo(
    () => [
      {
        id: 'status',
        header: 'Status',
        accessorFn: (d) => d.status,
        Cell: ({ row }) => <TableStatusIcon status={row.original.status} />,
        size: 150,
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(scData)
        },
        mantineTableBodyCellProps: {
          align: 'center'
        },
        enableSorting: true,
        grow: false
      },
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [measData]
  )

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={
              <>
                <Title>{data.name}</Title>
                <Text c="dimmed">{data.description}</Text>
              </>
            }
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                  <Button
                    color="ul-dark-blue"
                    size="xs"
                    w="fit-content"
                    component="a"
                    href={`${PAGE_DEFS.EXPLORE_TESTS.route}/${params.id}`}
                  >
                    Open in Explore View
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Status" content={data.status} />
                <InfoChip label="Scope" content={data.scope} />
                <InfoChip label="Output" content={data.output.kind} />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="docs">Documentation</Tabs.Tab>
                  {!!(data.output.kind === 'Measurement') && (
                    <Tabs.Tab value="measurements">Measurements</Tabs.Tab>
                  )}
                  {!!(data.output.kind === 'SafetyCase') && (
                    <Tabs.Tab value="safetycases">Safety Cases</Tabs.Tab>
                  )}
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Inputs" h="100%">
                        {data.inputs?.map((i) => (
                          <InfoChip
                            key={i.keyword}
                            label={`${i.kind} (${i.keyword})`}
                            content={i.description ? i.description : 'No description'}
                          />
                        ))}
                      </Fieldset>
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset
                        legend={
                          data.output.kind === 'Measurement'
                            ? 'Output - Measurement'
                            : 'Output - Safety Case'
                        }
                        h="100%"
                      >
                        {!!data.output.measurement ? (
                          <>
                            <Text fw={700}>{data.output.measurement.name}</Text>
                            <Text>{data.output.measurement.description}</Text>
                          </>
                        ) : !!data.output.safetyCase ? (
                          <>
                            <Text fw={700}>{data.output.safetyCase.name}</Text>
                            <Text>{data.output.safetyCase.description}</Text>
                          </>
                        ) : (
                          <Text>No data</Text>
                        )}
                      </Fieldset>
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Parameters" h="100%">
                        {data.parameters?.map((p) => (
                          <InfoChip
                            key={p.keyword}
                            label={p.keyword}
                            content={p.description ? p.description : 'No description'}
                          />
                        ))}
                      </Fieldset>
                    </Grid.Col>
                    {!!data.scores && !!data.scores.length && (
                      <Grid.Col span={colSpan}>
                        <Fieldset legend="ScoreSpecs" mah="500px" style={{ overflowY: 'auto' }}>
                          <Stack gap="xs">
                            {data.scores
                              ?.sort((s) => (s.priority === 'primary' ? -1 : 1))
                              .map((s, idx) => (
                                <Fieldset key={`${s.name}_${idx}`} legend={s.name}>
                                  <Stack gap="xs">
                                    <Text fw={700}>{s.title}</Text>
                                    <Text>{s.summary}</Text>
                                    <Group gap="xs">
                                      <b>Priority</b> <Code>{s.priority}</Code>
                                    </Group>
                                    <Group gap="xs">
                                      <b>Min</b> <Code>{s.minimum ?? 'N/A'}</Code> |<b>Max</b>{' '}
                                      <Code>{s.maximum ?? 'N/A'}</Code>| <b>Valence</b>{' '}
                                      <Code>{s.valence}</Code>
                                    </Group>
                                  </Stack>
                                </Fieldset>
                              ))}
                          </Stack>
                        </Fieldset>
                      </Grid.Col>
                    )}
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="docs">
                  <DocumentationSection data={docs} updateDocs={updateDocs} />
                </Tabs.Panel>
                <Tabs.Panel value="measurements">
                  <EntityView
                    title="Measurements"
                    page="measurements"
                    data={measData}
                    error={measError}
                    refresh={measRefresh}
                    columns={measColumns}
                    loading={measLoading}
                    lastUpdated={measLastUpdated}
                  />
                </Tabs.Panel>
                <Tabs.Panel value="safetycases">
                  <EntityView
                    title="Safety Cases"
                    page="safetycases"
                    data={scData}
                    error={scError}
                    refresh={scRefresh}
                    loading={scLoading}
                    lastUpdated={scLastUpdated}
                    columns={scColumns}
                  />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(docs, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
