/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import MainNavShell from '@/components/nav/MainNavShell'
export default MainNavShell
