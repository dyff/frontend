/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import {
  Card,
  Center,
  ComboboxItem,
  Container,
  Select,
  Stack,
  TextInput,
  Title
} from '@mantine/core'
import { useInputState } from '@mantine/hooks'
import { useState } from 'react'

export default function NewDataset() {
  const [name, setName] = useInputState('')
  const [arrowSchema, setArrowSchema] = useState<ComboboxItem | null>(null)

  // const createParams: DatasetCreateRequest = {}
  const arrowSchemaOpts = [
    { value: 'realvalue', label: 'Some Arrow Schema' },
    { value: 'differentrealvalue', label: 'Another Arrow Schema' }
  ]

  return (
    <Container>
      <Card shadow="md" withBorder>
        <Center>
          <Stack w="50%">
            <Title>New Dataset</Title>
            <TextInput
              label="Name"
              description="Name of dataset"
              placeholder="Dataset Name"
              value={name}
              onChange={setName}
            />
            <Select
              searchable
              label="Arrow Schema"
              value={arrowSchema ? arrowSchema.value : null}
              onChange={(_value, option) => setArrowSchema(option)}
              data={arrowSchemaOpts}
            />
          </Stack>
        </Center>
      </Card>
    </Container>
  )
}
