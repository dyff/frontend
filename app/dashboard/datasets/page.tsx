/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import EntityView from '@/components/display/EntityView'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { makeCreatedColumn, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Dataset } from '@/types/api'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.OPERATOR_DATASETS.title)
  const { data, error, loading, refresh, lastUpdated } = useEntityData<Dataset>('datasets', {})
  const columns: Array<MRT_ColumnDef<Dataset>> = useMemo(
    () => [
      makeStatusColumn(data),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorKey: 'account',
        header: 'Account',
        enableClickToCopy: true
      },
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      makeCreatedColumn(data)
    ],
    [data]
  )

  return (
    <ProtectedRoute>
      <EntityView
        title="Datasets"
        page="datasets"
        data={data}
        loading={loading}
        columns={columns}
        initialSorting={DEFAULT_SORT}
        lastUpdated={lastUpdated}
        error={error}
        refresh={refresh}
      />
    </ProtectedRoute>
  )
}
