/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import DocumentationSection from '@/components/data/DocumentationSection'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import EntityView from '@/components/display/EntityView'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import TableStatusIcon from '@/components/utility/TableStatusIcon'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getUniqueStatuses } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceService, Measurement, SafetyCase } from '@/types/api'
import {
  Button,
  Container,
  Divider,
  Fieldset,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Tabs,
  Text,
  Title
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useRouter } from 'next/navigation'
import { useMemo, useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: sDataArr,
    docs: sDocsArr,
    refresh: sRefresh,
    error: sError,
    loading: sLoading,
    updateDocs,
    updateLabels
  } = useEntityData<InferenceService>('inferenceservices', {
    initParams: { id: params.id },
    fetchDocs: true
  })

  const {
    data: mData,
    error: mError,
    loading: mLoading,
    refresh: mRefresh,
    lastUpdated: mLastUpdated,
    params: mParams,
    setParams: mSetParams
  } = useEntityData<Measurement>('measurements', {
    initParams: {
      inferenceService: params.id
    }
  })

  const {
    data: scData,
    error: scError,
    loading: scLoading,
    refresh: scRefresh,
    lastUpdated: scLastUpdated,
    params: scParams,
    setParams: scSetParams
  } = useEntityData<SafetyCase>('safetycases', {
    initParams: {
      inferenceService: params.id
    }
  })

  const sData = !sLoading && !!sDataArr && sDataArr.length ? sDataArr[0] : null
  const sDocs = !sLoading && !!sDocsArr && sDocsArr.length ? sDocsArr[0] : null

  const mColumns: Array<MRT_ColumnDef<Measurement>> = useMemo(
    () => [
      {
        id: 'status',
        header: 'Status',
        accessorFn: (d) => d.status,
        Cell: ({ row }) => <TableStatusIcon status={row.original.status} />,
        size: 150,
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(mData)
        },
        mantineTableBodyCellProps: {
          align: 'center'
        },
        enableSorting: true,
        grow: false
      },
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [mData]
  )

  const scColumns: Array<MRT_ColumnDef<SafetyCase>> = useMemo(
    () => [
      {
        id: 'status',
        header: 'Status',
        accessorFn: (d) => d.status,
        Cell: ({ row }) => <TableStatusIcon status={row.original.status} />,
        size: 150,
        filterVariant: 'multi-select',
        mantineFilterMultiSelectProps: {
          data: getUniqueStatuses(scData)
        },
        mantineTableBodyCellProps: {
          align: 'center'
        },
        enableSorting: true,
        grow: false
      },
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [scData]
  )

  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {sLoading ? (
          <EntityDetailLoading />
        ) : !sData ? (
          <NoDataDisplay />
        ) : sError ? (
          <EntityFetchErrorDisplay error={sError} />
        ) : (
          <EntityViewLayout
            top={<Title>{sData.name}</Title>}
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => sRefresh()}>
                    Refresh
                  </Button>
                  <Button
                    color="ul-dark-blue"
                    size="xs"
                    w="fit-content"
                    component="a"
                    href={`${PAGE_DEFS.EXPLORE_SYSTEMS.route}/${sData.model ? sData.model?.id : sData.id}`}
                  >
                    Open in Explore View
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={sData.id} clickToCopy />
                <InfoChip label="Account" content={sData.account} clickToCopy />
                <InfoChip label="Status" content={sData.status} />
                <InfoChip
                  label="Model"
                  content={!sData.model ? 'N/A' : sData.model.name}
                  redirect={
                    !sData.model
                      ? undefined
                      : `${PAGE_DEFS.OPERATOR_MODELS.route}/${sData.model.id}`
                  }
                  clickToCopy
                />
                <InfoChip label="Date Created" content={formatDateStr(sData?.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="docs">Documentation</Tabs.Tab>
                  <Tabs.Tab value="measurements">Measurements</Tabs.Tab>
                  <Tabs.Tab value="safetycases">Safety Cases</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={sData} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={sData.annotations} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Runner" h="100%">
                        {!sData.runner ? (
                          <NoDataDisplay />
                        ) : (
                          <Stack gap="xs">
                            <InfoChip label="Kind" content={sData.runner.kind} />
                            <Divider />
                            <InfoChip
                              label="Resources"
                              content={
                                <Stack gap="xs">
                                  <Text> {`Storage: ${sData.runner.resources.storage}`}</Text>
                                  <Text> {`Memory: ${sData.runner.resources.memory}`}</Text>
                                </Stack>
                              }
                            />
                            <Divider />
                            <InfoChip label="Kind" content={sData.runner.kind} />
                          </Stack>
                        )}
                      </Fieldset>
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="docs">
                  <DocumentationSection data={sDocs} updateDocs={updateDocs} />
                </Tabs.Panel>
                <Tabs.Panel value="measurements">
                  <EntityView
                    title="Measurements"
                    page="measurements"
                    data={mData}
                    error={mError}
                    refresh={mRefresh}
                    loading={mLoading}
                    lastUpdated={mLastUpdated}
                    columns={mColumns}
                    initialSorting={DEFAULT_SORT}
                  />
                </Tabs.Panel>
                <Tabs.Panel value="safetycases">
                  <EntityView
                    title="Safety Cases"
                    page="safetycases"
                    data={scData}
                    error={scError}
                    refresh={scRefresh}
                    loading={scLoading}
                    lastUpdated={scLastUpdated}
                    columns={scColumns}
                    initialSorting={DEFAULT_SORT}
                  />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(sData, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
