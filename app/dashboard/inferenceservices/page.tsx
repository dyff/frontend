/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import CuratedServices from '@/components/display/CuratedServices'
import EntityView from '@/components/display/EntityView'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { makeCreatedColumn, makeLabelsColumn, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceService } from '@/types/api'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.title)
  const { data, error, loading, refresh, lastUpdated } = useEntityData<InferenceService>(
    'inferenceservices',
    {}
  )

  const columns: Array<MRT_ColumnDef<InferenceService>> = useMemo(
    () => [
      makeStatusColumn(data),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      makeLabelsColumn(data),
      {
        accessorKey: 'account',
        header: 'Account',
        enableClickToCopy: true
      },
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      makeCreatedColumn(data)
    ],
    [data]
  )
  return (
    <ProtectedRoute>
      {/* <EntityView
        title="Inference Services"
        page="inferenceservices"
        data={data}
        loading={loading}
        columns={columns}
        initialSorting={DEFAULT_SORT}
        lastUpdated={lastUpdated}
        error={error}
        refresh={refresh}
      /> */}
      <CuratedServices />
    </ProtectedRoute>
  )
}
