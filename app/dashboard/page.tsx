/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import EntitySummary from '@/components/display/EntitySummary'
import { APP_GRID_CARD_SPAN, DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { makeCreatedColumn, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Dataset, Evaluation, InferenceService, InferenceSession, Model, Report } from '@/types/api'
import { Card, Grid, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.OPERATOR_DASHBOARD.title)
  const {
    data: evalData,
    error: evalError,
    loading: evalLoading,
    refresh: evalRefresh,
    lastUpdated: evalLastUpdated,
    params: evalParams
  } = useEntityData<Evaluation>('evaluations', {})

  const evalColumns: Array<MRT_ColumnDef<Evaluation>> = useMemo(
    () => [
      makeStatusColumn(evalData),
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true,
        grow: true
      },
      makeCreatedColumn(evalData)
    ],
    [evalData]
  )

  const {
    data: datasetData,
    error: datasetError,
    loading: datasetLoading,
    refresh: datasetRefresh,
    lastUpdated: datasetLastUpdated,
    params: datasetParams
  } = useEntityData<Dataset>('datasets', {
    initParams: { status: '' }
  })

  const datasetColumns: Array<MRT_ColumnDef<Dataset>> = useMemo(
    () => [
      makeStatusColumn(datasetData),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      makeCreatedColumn(datasetData)
    ],
    [datasetData]
  )

  const {
    data: reportData,
    error: reportError,
    loading: reportLoading,
    refresh: reportRefresh,
    lastUpdated: reportLastUpdated,
    params: reportParams
  } = useEntityData<Report>('reports', {
    initParams: { status: '' }
  })

  const reportColumns: Array<MRT_ColumnDef<Report>> = useMemo(
    () => [
      makeStatusColumn(reportData),
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      makeCreatedColumn(reportData)
    ],
    [reportData]
  )

  const {
    data: modelData,
    error: modelError,
    loading: modelLoading,
    refresh: modelRefresh,
    lastUpdated: modelLastUpdated,
    params: modelParams
  } = useEntityData<Model>('models', {
    initParams: { status: '' }
  })

  const modelColumns: Array<MRT_ColumnDef<Model>> = useMemo(
    () => [
      makeStatusColumn(modelData),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      makeCreatedColumn(modelData)
    ],
    [modelData]
  )

  const {
    data: inferenceServiceData,
    error: inferenceServiceError,
    loading: inferenceServiceLoading,
    refresh: inferenceServiceRefresh,
    lastUpdated: inferenceServiceLastUpdated,
    params: inferenceServiceParams
  } = useEntityData<InferenceService>('inferenceservices', {
    initParams: { status: '' }
  })

  const inferenceServiceColumns: Array<MRT_ColumnDef<InferenceService>> = useMemo(
    () => [
      makeStatusColumn(inferenceServiceData),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      makeCreatedColumn(inferenceServiceData)
    ],
    [inferenceServiceData]
  )

  const {
    data: inferenceSessionData,
    error: inferenceSessionError,
    loading: inferenceSessionLoading,
    refresh: inferenceSessionRefresh,
    lastUpdated: inferenceSessionLastUpdated,
    params: inferenceSessionParams
  } = useEntityData<InferenceSession>('inferencesessions', {
    initParams: { status: '' }
  })

  const inferenceSessionColumns: Array<MRT_ColumnDef<InferenceSession>> = useMemo(
    () => [
      makeStatusColumn(inferenceSessionData),
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      makeCreatedColumn(inferenceSessionData)
    ],
    [inferenceSessionData]
  )

  return (
    <ProtectedRoute>
      <Grid h="100%" w="100%">
        <Grid.Col span={12}>
          <Card withBorder shadow="sm">
            <Title>Dashboard</Title>
          </Card>
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Inference Services"
            page="inferenceservices"
            data={inferenceServiceData}
            error={inferenceServiceError}
            loading={inferenceServiceLoading}
            lastUpdated={inferenceServiceLastUpdated}
            refresh={inferenceServiceRefresh}
            params={inferenceServiceParams}
            columns={inferenceServiceColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Inference Sessions"
            page="inferencesessions"
            data={inferenceSessionData}
            error={inferenceSessionError}
            loading={inferenceSessionLoading}
            lastUpdated={inferenceSessionLastUpdated}
            refresh={inferenceSessionRefresh}
            params={inferenceSessionParams}
            columns={inferenceSessionColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Models"
            page="models"
            data={modelData}
            error={modelError}
            loading={modelLoading}
            lastUpdated={modelLastUpdated}
            refresh={modelRefresh}
            params={modelParams}
            columns={modelColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Datasets"
            page="datasets"
            data={datasetData}
            error={datasetError}
            loading={datasetLoading}
            lastUpdated={datasetLastUpdated}
            refresh={datasetRefresh}
            params={datasetParams}
            columns={datasetColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Evaluations"
            page="evaluations"
            data={evalData}
            error={evalError}
            loading={evalLoading}
            lastUpdated={evalLastUpdated}
            refresh={evalRefresh}
            params={evalParams}
            columns={evalColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
        <Grid.Col span={APP_GRID_CARD_SPAN}>
          <EntitySummary
            title="Reports"
            page="reports"
            data={reportData}
            error={reportError}
            loading={reportLoading}
            lastUpdated={reportLastUpdated}
            refresh={reportRefresh}
            params={reportParams}
            columns={reportColumns}
            initialSorting={DEFAULT_SORT}
          />
        </Grid.Col>
      </Grid>
    </ProtectedRoute>
  )
}
