/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Center, Title } from '@mantine/core'

export default function Error() {
  return (
    <Center h="100%">
      <Title order={3}>Failed to load measurement</Title>
    </Center>
  )
}
