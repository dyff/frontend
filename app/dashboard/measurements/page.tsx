/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import EntityView from '@/components/display/EntityView'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Measurement } from '@/types/api'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.OPERATOR_MEASUREMENTS.title)
  const { data, error, loading, refresh, lastUpdated } = useEntityData<Measurement>(
    'measurements',
    {}
  )

  const columns: Array<MRT_ColumnDef<Measurement>> = useMemo(
    () => [
      makeStatusColumn(data),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      {
        accessorKey: 'account',
        header: 'Account',
        enableClickToCopy: true
      },
      {
        accessorFn: (m) => m.level,
        header: 'Level',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [data]
  )

  return (
    <ProtectedRoute>
      <EntityView
        title="Measurements"
        page="measurements"
        data={data}
        loading={loading}
        columns={columns}
        initialSorting={DEFAULT_SORT}
        lastUpdated={lastUpdated}
        error={error}
        refresh={refresh}
      />
    </ProtectedRoute>
  )
}
