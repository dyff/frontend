/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import ProtectedRoute from '@/components/auth/ProtectedRoute'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getStatusIcon } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Evaluation } from '@/types/api'
import { Button, Container, Grid, Group, ScrollArea, Stack, Tabs, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_EVALUATIONS.title)
  const { token, loading: tokenLoading } = useAuth()
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    refresh,
    updateLabels,
    error,
    loading,
    updateDocs
  } = useEntityData<Evaluation>('evaluations', {
    initParams: { id: params.id }
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading || tokenLoading || !token ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <Grid>
            <Grid.Col span={12}>
              <Title>{data.id}</Title>
            </Grid.Col>
            <Grid.Col
              span={{ lg: 12, xl: sideBarSpan }}
              style={{ position: 'sticky', alignSelf: 'start', top: 0, left: 0 }}
            >
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip
                  label="Status"
                  content={
                    <Group gap="xs">
                      {data.status}
                      {getStatusIcon(data.status, true)}
                    </Group>
                  }
                />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            </Grid.Col>
            <Grid.Col span={{ lg: 12, xl: 12 - sideBarSpan }}>
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={data.annotations} />
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            </Grid.Col>
          </Grid>
        )}
      </Container>
    </ProtectedRoute>
  )
}
