/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import DocumentationSection from '@/components/data/DocumentationSection'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import EntityView from '@/components/display/EntityView'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getStatusIcon, makeCreatedColumn, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceService, Model } from '@/types/api'
import {
  Button,
  Container,
  Fieldset,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Tabs,
  Text,
  Title
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo, useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_MODELS.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    docs: docsArr,
    refresh,
    updateLabels,
    error,
    loading,
    updateDocs
  } = useEntityData<Model>('models', {
    initParams: { id: params.id },
    fetchDocs: true
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const docs = docsArr && docsArr.length ? docsArr[0] : null

  const {
    data: infSvcDataArr,
    refresh: infSvcRefresh,
    error: infSvcError,
    loading: infSvcLoading,
    lastUpdated: infSvcLastUpdated
  } = useEntityData<InferenceService>('inferenceservices', {
    initParams: { model: params.id },
    fetchDocs: true
  })

  const infSvcColumns: Array<MRT_ColumnDef<InferenceService>> = useMemo(
    () => [
      makeStatusColumn(infSvcDataArr),
      {
        accessorKey: 'name',
        header: 'Name',
        enableClickToCopy: true
      },
      makeCreatedColumn(infSvcDataArr)
    ],
    [infSvcDataArr]
  )

  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={<Title>{data.name}</Title>}
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                  <Button
                    color="ul-dark-blue"
                    size="xs"
                    w="fit-content"
                    component="a"
                    href={`${PAGE_DEFS.EXPLORE_SYSTEMS.route}/${params.id}`}
                  >
                    Open in Explore View
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip
                  label="Status"
                  content={
                    <Group gap="xs">
                      {data.status}
                      {getStatusIcon(data.status, true)}
                    </Group>
                  }
                />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="docs">Documentation</Tabs.Tab>
                  <Tabs.Tab value="infsvcs">Inference Services</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Source" h="100%">
                        <Text fw={700}>{data.source.kind}</Text>
                        {!!data.source.huggingFaceHub && (
                          <Group gap="xs">
                            <Text fw={700}>Repo ID</Text>
                            <Text>{data.source.huggingFaceHub.repoID}</Text>
                          </Group>
                        )}
                      </Fieldset>
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={data.annotations} />
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="docs">
                  <DocumentationSection data={docs} updateDocs={updateDocs} />
                </Tabs.Panel>
                <Tabs.Panel value="infsvcs">
                  <EntityView
                    title="Inference Services"
                    page="inferenceservices"
                    data={infSvcDataArr}
                    error={infSvcError}
                    refresh={infSvcRefresh}
                    loading={infSvcLoading}
                    lastUpdated={infSvcLastUpdated}
                    columns={infSvcColumns}
                    initialSorting={DEFAULT_SORT}
                    enableBottomToolbar={true}
                  />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
