/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import InfoChip from '@/components/display/InfoChip'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getStatusIcon } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceSession } from '@/types/api'
import { Button, Container, Grid, Group, ScrollArea, Stack, Tabs, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    refresh,
    updateLabels,
    error,
    loading,
    updateDocs
  } = useEntityData<InferenceSession>('inferencesessions', {
    initParams: { id: params.id }
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={<Title>{data.inferenceService.name}</Title>}
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip
                  label="Inference Service"
                  content={data.inferenceService.name}
                  redirect={`${PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}/${data.inferenceService.id}`}
                  clickToCopy
                />
                <InfoChip
                  label="Inference Service ID"
                  content={data.inferenceService.id}
                  clickToCopy
                />
                <InfoChip label="Expires" content={data.expires} />
                <InfoChip
                  label="Status"
                  content={
                    <Group gap="xs">
                      {data.status}
                      {getStatusIcon(data.status, true)}
                    </Group>
                  }
                />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={data.annotations} />
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
