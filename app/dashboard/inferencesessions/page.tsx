/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import EntityView from '@/components/display/EntityView'
import { DEFAULT_SORT, PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, makeStatusColumn } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { InferenceSession } from '@/types/api'
import { useDocumentTitle } from '@mantine/hooks'
import { MRT_ColumnDef } from 'mantine-react-table'
import { useMemo } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.OPERATOR_INFERENCE_SESSIONS.title)
  const { data, error, loading, refresh, lastUpdated } = useEntityData<InferenceSession>(
    'inferencesessions',
    {}
  )

  const columns: Array<MRT_ColumnDef<InferenceSession>> = useMemo(
    () => [
      makeStatusColumn(data),
      {
        accessorKey: 'id',
        header: 'ID',
        enableClickToCopy: true
      },
      {
        accessorKey: 'account',
        header: 'Account',
        enableClickToCopy: true
      },
      {
        accessorFn: (d) => new Date(d.creationTime!),
        Cell: ({ row }) => formatDateStr(row.original.creationTime),
        id: 'created',
        header: 'Created',
        enableSorting: true,
        filterVariant: 'date-range'
      },
      {
        accessorFn: (d) => new Date(d.expires!),
        Cell: ({ row }) => formatDateStr(row.original.expires),
        id: 'expires',
        header: 'Expires',
        enableSorting: true,
        filterVariant: 'date-range'
      }
    ],
    [data]
  )
  return (
    <ProtectedRoute>
      <EntityView
        title="Inference Sessions"
        page="inferencesessions"
        data={data}
        loading={loading}
        columns={columns}
        initialSorting={DEFAULT_SORT}
        lastUpdated={lastUpdated}
        error={error}
        refresh={refresh}
      />
    </ProtectedRoute>
  )
}
