/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import InfoChip from '@/components/display/InfoChip'
import LogsDisplay from '@/components/display/LogsDisplay'
import NoDataDisplay from '@/components/display/NoData'
import NotebooksDisplay from '@/components/display/NotebooksDisplay'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { SafetyCase } from '@/types/api'
import {
  Button,
  Container,
  Fieldset,
  Grid,
  Group,
  ScrollArea,
  Stack,
  Tabs,
  Text,
  Title
} from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_SAFETY_CASES.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    artifacts: artifactsArr,
    logs: logsArr,
    refresh,
    updateLabels,
    error,
    loading
  } = useEntityData<SafetyCase>('safetycases', {
    initParams: { id: params.id },
    fetchArtifacts: true,
    fetchLogs: true
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const dataWithArtifacts = artifactsArr && artifactsArr.length ? artifactsArr[0] : null
  const logs = logsArr && logsArr.length ? logsArr[0] : null
  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={
              <>
                <Title>{data.name}</Title>
                <Text c="dimmed">{data.description}</Text>
              </>
            }
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                  <Button
                    color="ul-dark-blue"
                    size="xs"
                    w="fit-content"
                    component="a"
                    href={`${PAGE_DEFS.EXPLORE_REPORTS.route}/${params.id}`}
                  >
                    Open in Explore View
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data?.id} clickToCopy />
                <InfoChip
                  label="Method"
                  redirect={`${PAGE_DEFS.OPERATOR_METHODS.route}${data.method.id}`}
                  content={data.method.name}
                  clickToCopy
                />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip label="Status" content={data.status} />
                <InfoChip
                  label="Dataset"
                  redirect={
                    data.scope?.dataset
                      ? `${PAGE_DEFS.OPERATOR_DATASETS.route}/${data.scope?.dataset}`
                      : undefined
                  }
                  content={data.scope?.dataset || 'N/A'}
                  clickToCopy
                />
                <InfoChip
                  label="Evaluation"
                  redirect={
                    data.scope?.evaluation
                      ? `${PAGE_DEFS.OPERATOR_EVALUATIONS.route}/${data.scope?.evaluation}`
                      : undefined
                  }
                  content={data.scope?.evaluation || 'N/A'}
                  clickToCopy
                />
                <InfoChip
                  label="Inference Service"
                  redirect={
                    data.scope?.inferenceService
                      ? `${PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}/${data.scope?.inferenceService}`
                      : undefined
                  }
                  content={data.scope?.inferenceService || 'N/A'}
                  clickToCopy
                />
                <InfoChip
                  label="Model"
                  redirect={
                    data.scope?.model
                      ? `${PAGE_DEFS.OPERATOR_MODELS.route}/${data.scope?.model}`
                      : undefined
                  }
                  content={data.scope?.model || 'N/A'}
                  clickToCopy
                />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="notebooks">Notebooks</Tabs.Tab>
                  <Tabs.Tab value="logs">Logs</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Inputs" h="100%">
                        {data.inputs?.map((i) => (
                          <InfoChip key={i.keyword} label={`${i.keyword}`} content={i.entity} />
                        ))}
                      </Fieldset>
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={data.annotations} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <Fieldset legend="Parameters" h="100%">
                        {data.arguments?.map((a) => (
                          <InfoChip key={a.keyword} label={a.keyword} content={a.value} />
                        ))}
                      </Fieldset>
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="notebooks">
                  <NotebooksDisplay data={dataWithArtifacts} />
                </Tabs.Panel>
                <Tabs.Panel value="logs">
                  <LogsDisplay logs={logs} />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
