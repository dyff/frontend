/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Center, Image, Stack, Title } from '@mantine/core'

export default function Error() {
  return (
    <Center h="100%" w="100%">
      <Stack>
        <Title order={5} c="ul-coral">
          Dashboard failed to load {`:(`}
        </Title>
      </Stack>
    </Center>
  )
}
