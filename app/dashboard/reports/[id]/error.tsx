/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

export default function Error() {
  return <h1>Invalid Report ID</h1>
}
