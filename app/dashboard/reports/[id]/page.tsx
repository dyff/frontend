/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import ProtectedRoute from '@/components/auth/ProtectedRoute'
import LabelsSection from '@/components/data/LabelsSection'
import AnnotationsSection from '@/components/display/AnnotationsSection'
import EntityFetchErrorDisplay from '@/components/display/EntityFetchErrorDisplay'
import InfoChip from '@/components/display/InfoChip'
import LogsDisplay from '@/components/display/LogsDisplay'
import NoDataDisplay from '@/components/display/NoData'
import EntityViewLayout from '@/components/layout/EntityViewLayout'
import EntityDetailLoading from '@/components/utility/EntityDetailLoading'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { formatDateStr, getStatusIcon } from '@/lib/helpers'
import { useEntityData } from '@/lib/hooks/entity'
import { Report } from '@/types/api'
import { Button, Container, Grid, Group, ScrollArea, Stack, Tabs, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useState } from 'react'

export default function Page({ params }: { params: { id: string } }) {
  useDocumentTitle(PAGE_DEFS.OPERATOR_REPORTS.title)
  const [activeTab, setActiveTab] = useState<string | null>('details')

  const {
    data: dataArr,
    logs: logsArr,
    refresh,
    updateLabels,
    error,
    loading,
    updateDocs
  } = useEntityData<Report>('reports', {
    initParams: { id: params.id },
    fetchLogs: true
  })

  const data = dataArr && dataArr.length ? dataArr[0] : null
  const logs = logsArr && logsArr.length ? logsArr[0] : null
  const colSpan = { lg: 12, xl: 6 }
  const sideBarSpan = 3

  return (
    <ProtectedRoute>
      <Container display="relative" size="70%">
        {loading ? (
          <EntityDetailLoading />
        ) : !data ? (
          <NoDataDisplay />
        ) : error ? (
          <EntityFetchErrorDisplay error={error} />
        ) : (
          <EntityViewLayout
            top={<Title>{data.id}</Title>}
            left={
              <Stack gap="md">
                <Group gap="sm">
                  <Button color="ul-dark-blue" size="xs" w="fit-content" onClick={() => refresh()}>
                    Refresh
                  </Button>
                </Group>
                <InfoChip label="Identifier" content={data.id} clickToCopy />
                <InfoChip
                  label="Status"
                  content={
                    <Group gap="xs">
                      {data.status}
                      {getStatusIcon(data.status, true)}
                    </Group>
                  }
                />
                <InfoChip label="Account" content={data.account} clickToCopy />
                <InfoChip
                  label="Dataset"
                  redirect={
                    data.dataset
                      ? `${PAGE_DEFS.OPERATOR_DATASETS.route}/${data.dataset}`
                      : undefined
                  }
                  content={data.dataset || 'N/A'}
                />
                <InfoChip
                  label="Evaluation"
                  redirect={
                    data.evaluation
                      ? `${PAGE_DEFS.OPERATOR_EVALUATIONS.route}/${data.evaluation}`
                      : undefined
                  }
                  content={data.evaluation || 'N/A'}
                />
                <InfoChip
                  label="Inference Service"
                  redirect={
                    data.inferenceService
                      ? `${PAGE_DEFS.OPERATOR_INFERENCE_SERVICES.route}/${data.inferenceService}`
                      : undefined
                  }
                  content={data.inferenceService || 'N/A'}
                />
                <InfoChip
                  label="Model"
                  redirect={
                    data.model ? `${PAGE_DEFS.OPERATOR_MODELS.route}/${data.model}` : undefined
                  }
                  content={data.model || 'N/A'}
                />
                <InfoChip label="Date Created" content={formatDateStr(data.creationTime)} />
              </Stack>
            }
            right={
              <Tabs defaultValue="details" w="99%" value={activeTab} onChange={setActiveTab}>
                <Tabs.List mb="md">
                  <Tabs.Tab value="details">Details</Tabs.Tab>
                  <Tabs.Tab value="logs">Logs</Tabs.Tab>
                  <Tabs.Tab value="json">Raw JSON</Tabs.Tab>
                </Tabs.List>
                <Tabs.Panel value="details">
                  <Grid>
                    <Grid.Col span={colSpan}>
                      <LabelsSection data={data} updateLabels={updateLabels} />
                    </Grid.Col>
                    <Grid.Col span={colSpan}>
                      <AnnotationsSection annotations={data.annotations} />
                    </Grid.Col>
                  </Grid>
                </Tabs.Panel>
                <Tabs.Panel value="logs">
                  <LogsDisplay logs={logs} />
                </Tabs.Panel>
                <Tabs.Panel value="json">
                  <ScrollArea>
                    <pre>{JSON.stringify(data, null, 2)}</pre>
                  </ScrollArea>
                </Tabs.Panel>
              </Tabs>
            }
          />
        )}
      </Container>
    </ProtectedRoute>
  )
}
