/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import DyffLogo from '@/components/utility/DyffLogo'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { Button, Center, Stack, Title } from '@mantine/core'
import { useRouter } from 'next/navigation'

export default function NotFound() {
  return (
    <Center h="100vh" w="100vw">
      <Stack>
        <DyffLogo size={200} />
        <Title>{`This page doesn't exist :(`}</Title>
        <Button variant="outline" component="a" href={PAGE_DEFS.HOME.route}>
          Return to a safe home
        </Button>
      </Stack>
    </Center>
  )
}
