/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import { DEFAULT_AUTH_REDIRECT } from '@/lib/constants/constants'
import { notifications } from '@mantine/notifications'
import { useRouter, useSearchParams } from 'next/navigation'
import { useEffect } from 'react'

export default function CallbackPage() {
  const { setToken } = useAuth()
  const router = useRouter()
  const params = useSearchParams()
  const paramToken = params.get('token')
  const redirect = decodeURIComponent(params.get('redirect') || DEFAULT_AUTH_REDIRECT)

  useEffect(() => {
    if (!paramToken) {
      notifications.show({ message: 'No token passed to callback!' })
      router.push(`/auth/signin?redirect=${encodeURIComponent(redirect)}`)
      return
    }
    setToken(paramToken, redirect)
  }, [params])

  return <></>
}
