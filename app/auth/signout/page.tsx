/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import DyffLoader from '@/components/utility/DyffLoader'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { getRedirectParameter } from '@/lib/helpers'
import { Center, Stack, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useRouter, useSearchParams } from 'next/navigation'
import { useEffect } from 'react'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.AUTH_SIGNOUT.title)
  const { releaseToken } = useAuth()
  const router = useRouter()
  const params = useSearchParams()
  const redirectPath = getRedirectParameter(params)

  // Release token and redirect to sign in
  useEffect(() => {
    releaseToken()
    router.push(`${PAGE_DEFS.AUTH_SIGNIN.route}?redirect=${redirectPath}`)
  }, [])

  return (
    <Center h="100vh">
      <Stack align="center">
        <DyffLoader />
        <Title order={2}>Signing out...</Title>
      </Stack>
    </Center>
  )
}
