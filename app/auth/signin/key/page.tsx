/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import DyffLogo from '@/components/utility/DyffLogo'
import { verifyAPIKey } from '@/lib/auth'
import { getRedirectParameter } from '@/lib/helpers'
import { Button, Center, Stack, TextInput, Title } from '@mantine/core'
import { useInputState } from '@mantine/hooks'
import { useRouter, useSearchParams } from 'next/navigation'
import { ChangeEvent, useState } from 'react'

export default function SignIn() {
  const router = useRouter()
  const params = useSearchParams()

  const [key, setKey] = useInputState('')
  const [keyError, setKeyError] = useState('')
  const [keyVerifyLoading, setKeyVerifyLoading] = useState<boolean>(false)

  const redirectParam = `redirect=${getRedirectParameter(params)}`
  const keyParam = `token=${key}`

  const handleKeyInput = (e: ChangeEvent<HTMLInputElement>) => {
    setKeyError('')
    setKey(e)
  }

  const handleVerifyKey = async (): Promise<void> => {
    setKeyVerifyLoading(true)
    try {
      const res = await verifyAPIKey(key)
      if (res.status === 200) {
        router.push(`/auth/callback?${keyParam}&${redirectParam}`)
        return
      }
    } catch (e) {}
    setKeyVerifyLoading(false)
    setKeyError('Invalid key')
  }

  return (
    <Center h="100vh">
      <Stack align="center">
        <DyffLogo size={200} />
        <Title>Dyff</Title>
        <TextInput
          disabled={keyVerifyLoading}
          label="API Key"
          placeholder=""
          value={key}
          onChange={handleKeyInput}
          error={keyError}
        />
        <Button color="ul-dark-blue" fullWidth onClick={handleVerifyKey} loading={keyVerifyLoading}>
          Verify
        </Button>
        <Button
          color="ul-dark-blue"
          variant="subtle"
          fullWidth
          onClick={() => {
            router.push(`/auth/signin?${redirectParam}`)
          }}
          disabled={keyVerifyLoading}
        >
          Try another way
        </Button>
      </Stack>
    </Center>
  )
}
