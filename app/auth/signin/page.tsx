/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import DyffLogo from '@/components/utility/DyffLogo'
import { redirectGoogleLogin } from '@/lib/auth'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { getRedirectParameter } from '@/lib/helpers'
import { Button, Center, Stack, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { notifications } from '@mantine/notifications'
import { IconBrandGoogle, IconKey, IconUserQuestion } from '@tabler/icons-react'
import { useRouter, useSearchParams } from 'next/navigation'
import { useEffect } from 'react'

export default function SignIn() {
  useDocumentTitle(PAGE_DEFS.AUTH_SIGNIN.title)
  const router = useRouter()
  const params = useSearchParams()
  const redirectPath = getRedirectParameter(params)
  const msg = params.get('msg') // Dyff JWT Token (Account's api key in base64)
  const error = params.get('error') // Dyff JWT Token (Account's api key in base64)

  // On msg, display it and navigate
  useEffect(() => {
    if (msg) {
      notifications.show({ message: msg })
      router.push(`/auth/signin?redirect=${redirectPath}`)
    } else if (error) {
      notifications.show({ message: error, color: 'red' })
      router.push(`/auth/signin?redirect=${redirectPath}`)
    }
  }, [msg, error])

  return (
    <Center h="100vh">
      <Stack align="center">
        <DyffLogo size={200} />
        <Title>Dyff</Title>
        <Button
          color="ul-dark-blue"
          fullWidth
          leftSection={<IconUserQuestion />}
          rightSection={<span />}
          justify="space-between"
          onClick={(): void => {
            router.push('/home')
          }}
        >
          Explore as guest
        </Button>
        <Button
          color="ul-dark-blue"
          fullWidth
          leftSection={<IconBrandGoogle />}
          rightSection={<span />}
          justify="space-between"
          onClick={(): void => {
            redirectGoogleLogin(router)
          }}
        >
          Sign in with Google
        </Button>
        <Button
          color="ul-dark-blue"
          fullWidth
          leftSection={<IconKey />}
          rightSection={<span />}
          justify="space-between"
          onClick={(): void => {
            router.push(`/auth/signin/key?redirect=${encodeURIComponent(redirectPath)}`)
          }}
        >
          Sign in with Key
        </Button>
      </Stack>
    </Center>
  )
}
