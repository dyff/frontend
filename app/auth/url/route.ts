/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import { NextResponse } from 'next/server'

export const dynamic = 'force-dynamic'
export async function GET(request: Request) {
  return NextResponse.json({ url: process.env.DYFF_API_AUTH_URL })
}
