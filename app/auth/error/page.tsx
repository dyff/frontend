/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { useAuth } from '@/components/auth/AuthProvider'
import DyffLogo from '@/components/utility/DyffLogo'
import { PAGE_DEFS } from '@/lib/constants/constants'
import { getRedirectParameter } from '@/lib/helpers'
import { Button, Center, Stack, Text, Title } from '@mantine/core'
import { useDocumentTitle } from '@mantine/hooks'
import { useSearchParams } from 'next/navigation'

export default function Page() {
  useDocumentTitle(PAGE_DEFS.AUTH_ERROR.title)
  const params = useSearchParams()
  const redirectPath = getRedirectParameter(params)
  const { isSignedIn } = useAuth()

  return (
    <Center h="100vh" w="100vw">
      <Stack align="center">
        <DyffLogo size={200} />
        <Title order={4} ta="center">
          Uh oh!
        </Title>
        <Text c="dimmed">{`You're not allowed to view that page`}</Text>

        {isSignedIn ? (
          <Button
            w="200px"
            variant="filled"
            component="a"
            href={`${PAGE_DEFS.AUTH_SIGNOUT.route}?redirect=${redirectPath}`}
          >
            Try a different account
          </Button>
        ) : (
          <Button
            w="200px"
            variant="filled"
            component="a"
            href={`${PAGE_DEFS.AUTH_SIGNIN.route}?redirect=${redirectPath}`}
          >
            Sign in
          </Button>
        )}
        <Button w="200px" variant="outline" component="a" href={PAGE_DEFS.HOME.route}>
          Go home
        </Button>
      </Stack>
    </Center>
  )
}
