/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

declare global {
  namespace NodeJS {
    interface ProcessEnv {
      DYFF_API_BASE_URL: string
      DYFF_API_FULL_URL: string
      DYFF_API_AUTH_URL: string
      DYFF_API_KEY: string
    }
  }
}

export {}
