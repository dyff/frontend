# SPDX-FileCopyrightText: 2024 UL Research Institutes
# SPDX-License-Identifier: Apache-2.0

-include .env
check-env:
	@if [ ! -f .env ]; then \
		echo "Warning: .env file not found. Please create one per the README"; \
		exit 1; \
	fi

IMAGE = dyff-frontend
DOCKER_RUN_OPTS = --rm -it -p 3000:3000 \
	-e DYFF_API_FULL_URL=$(DYFF_API_FULL_URL) \
	-e DYFF_API_AUTH_URL=$(DYFF_API_AUTH_URL) \
	-e DYFF_API_VIEWS_URL=$(DYFF_API_VIEWS_URL) \
	$(IMAGE)

build: check-env
	docker build -t $(IMAGE) .

run: check-env
	docker run $(DOCKER_RUN_OPTS)

all: check-env
	docker build -t ${IMAGE} .
	docker run $(DOCKER_RUN_OPTS)

shell:
	docker run --rm -it $(IMAGE) /bin/sh --login

types: check-env
	npx openapi-typescript $(DYFF_API_FULL_URL)/openapi.json -o ./types/dyff-api.d.ts
	npx openapi-typescript $(DYFF_API_VIEWS_URL)/openapi.json -o ./types/dyff-views.d.ts
	npx prettier -cw ./types/
