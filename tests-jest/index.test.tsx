/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

import HomePage from '@/app/page'
import '@testing-library/jest-dom'
import { render } from '@/tests-jest-utils'

jest.mock('next/navigation', () => ({
  useRouter() {
    return {
      prefetch: () => jest.fn(),
      push: () => jest.fn()
    }
  }
}))

const fakeAuth = {
  token: 'asd',
  decodedToken: {},
  isExpired: false,
  loading: false
}

// Return fake auth object
jest.mock('@/components/auth/AuthProvider', () => ({
  useAuth() {
    console.log('mock use auth')
    return fakeAuth
  }
}))

describe('Homepage', () => {
  it('renders correctly', () => {
    const { container } = render(<HomePage />)

    expect(container).toBeInTheDocument()
  })
})
