/*
 * SPDX-FileCopyrightText: 2024 UL Research Institutes
 * SPDX-License-Identifier: Apache-2.0
 */

'use client'

import { Button, Card, createTheme, TextInput } from '@mantine/core'
import '@mantine/core/styles.css'
import '@mantine/dates/styles.css'
import 'mantine-react-table/styles.css'

export const theme = createTheme({
  primaryColor: 'ul-dark-blue',
  primaryShade: 9,
  colors: {
    'ul-bright-blue': [
      '#e9eeff',
      '#cfd7ff',
      '#9bacff',
      '#647eff',
      '#3856fe',
      '#1b3efe',
      '#0931ff',
      '#0025e4',
      '#0020cc',
      '#001ab4'
    ],
    'ul-dark-blue': [
      '#ebebff',
      '#d1d0fa',
      '#9e9df8',
      '#6865f7',
      '#3f39f6',
      '#291df7',
      '#2011f8',
      '#1707dd',
      '#0f04c5',
      '#0000ad'
    ],
    'ul-midnight-blue': [
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49',
      '#122C49'
    ],
    'ul-bright-green': [
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451',
      '#00A451'
    ],
    'ul-coral': [
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255',
      '#FF6255'
    ],
    'ul-light-blue': [
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc',
      '#eaf3fc'
    ]
  },
  components: {
    Card: Card.extend({
      defaultProps: {
        radius: 'md'
      }
    }),
    TextInput: TextInput.extend({
      defaultProps: {
        radius: 'md'
      }
    }),
    Button: Button.extend({
      defaultProps: {
        style: {
          transition: 'all 0.1s linear'
        },
        radius: 'md'
      }
    })
  }
})
